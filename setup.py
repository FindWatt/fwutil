import os, sys

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwUtil/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:][:-2]

def download_nltk_stopwords():
    print("About to download nltk stopwords...")
    import nltk
    nltk.download("stopwords")
    nltk.download("punkt")

def download_spacy_model():
    # try to check if spacy model is already downloaded and installed
    import spacy
    try:
        o_nlp = spacy.load('en')
        if o_nlp.__dict__.get('path'): return None
    except:
        pass
        
    #print("About to download spacy model...")
    os.system("python -m spacy download en")
    #os.system("pip install https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-1.2.0/en_core_web_sm-1.2.0.tar.gz")
    #this will always download even if model is already present
    #spacy.cli.download('en')
            
class custom_install(install):
    def run(self):
        print("This is a custom installation")
        install.run(self)
        download_nltk_stopwords()
        download_spacy_model()

setuptools.setup(
    name="FwUtil",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwutil",

    author="FindWatt",

    description="Utilities packaged in different categories",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt"]},
    py_modules=['FwUtil'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "numpy",
        "pandas",
        "scikit-learn",
        "scipy",
        "nltk",
        "requests",
        "pymssql",
        "spacy",
        "boto",
        "openpyxl",
        "XlsxWriter"
    ],
    cmdclass={'install': custom_install}
)


