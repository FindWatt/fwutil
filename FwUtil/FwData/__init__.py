try:
    from FwCsvInput import *
except ImportError:
    from .FwCsvInput import *

try:
    from FwSpreadsheetIput import *
except ImportError:
    from .FwSpreadsheetIput import *

try:
    from FwSpreadsheetOutput import *
except ImportError:
    from .FwSpreadsheetOutput import *

try:
    from FwSequelInput import *
except ImportError:
    from .FwSequelInput import *

try:
    from _FwData import *
except ImportError:
    from ._FwData import *
    
try:
    from encodings import *
except ImportError:
    from .encodings import *