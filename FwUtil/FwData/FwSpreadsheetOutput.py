import os, json, re
import pandas as pd
import xlsxwriter as xl

__all__ = ["SpreadsheetOutput", "MultiSheetExporter"]
__version__ = "0.0.5"

class SpreadsheetOutput:
    """The SpreadsheetOutput class is meant to take a pandas dataframe as input,
    And store it in a file, according to the parameters passed in the json_config
    argument
    """

    def __init__(self, pd_dataframe, json_config):
        """ Construct the spreadsheet output object """

        self.df = pd_dataframe

        if type(json_config) == str:
            self.config = self.loadConfigFile( json_config ) #Load configurations from json file, if it's a path
        elif type(json_config) == dict:
            self.config = json_config #Just use the config dictionary, if it's a dictionary

        if not "file_name" in self.config:
            print("The config file is missing the file name")
            return False

        if "file_name" in self.config: #If an output path was provided
            self.filename = self.config["file_name"]

            if ".csv" in str(self.filename).lower(): #And it contains .csv
                self.df.to_csv(self.filename, header=True ) #Output a csv file
            else:
                if not "sheet_name" in self.config:
                    self.config["sheet_name"] = "ggl_tax"
                self.df.to_excel(self.filename, sheet_name=self.config["sheet_name"], engine='xlsxwriter') #Otherwise output a spreadsheet
        else:
            self.df.to_csv("output.csv", header=True )#If no output path was provided, use a default filename



def MultiSheetExporter(e_dataframes={}, e_config={}, s_file="python_export.xlsx"):
    '''
    This function write exel file from several input dataframes with desired formatting for columns : width,
    number type and switching autofilter, text wrapping and freezing panes on/off

    :param dataframes: dictionary of data frames to write to excel file with their names
    :param e_config: a dictionary of column wise settings for columns width (as 'col_width')  and number formating ('col_format')
           where dataframes have names and columns are given as 0-based integers/header names, 'freeze_column_names' identifies if first row with
           names should be frozen and 'add_autofilter' identifies using autofilter
           like {'Sheet1':{'col_width':{1:20, 2:35}, 'col_format':{0:'#.00%', 1:'#.##$', 2:'#.00'}}}
    :return: returns nothing
    '''
    
    if not s_file.lower().endswith(".xlsx"):
        s_file += ".xlsx"

    o_writer = pd.ExcelWriter(s_file, engine='xlsxwriter')
    wb_workbook = o_writer.book

    # set any defaults
    for s_sht, e_sht_config in e_config.items():
        if not 'sht_name' in e_sht_config:
            e_sht_config = str(s_sht)

    e_sht_name_map = {}
    e_shts = {}
    for s_sht, pd_frame in e_dataframes.items():
        pd_frame.to_excel(o_writer, sheet_name=validate_sheet_name(s_sht, pd_frame, e_shts, e_sht_name_map), index=False)

    for s_sht in o_writer.sheets:
        ws = o_writer.sheets[s_sht]
        pd_sht = e_shts[s_sht]
        if e_sht_name_map[s_sht] in e_config:
        
            e_sht_config = e_config[e_sht_name_map[s_sht]]
    
            if 'col_format' in e_sht_config:
                e_col_form={get_col_index(a, list(pd_sht.columns)):b
                            for a,b in e_config[e_sht_name_map[s_sht]]['col_format'].items()
                            if get_col_index(a, list(pd_sht.columns)) != -1}
            else:
                e_col_form={}
    
            if 'col_width' in e_sht_config:
                e_col_width={get_col_index(a, list(pd_sht.columns)):b
                             for a,b in e_config[e_sht_name_map[s_sht]]['col_width'].items()
                             if get_col_index(a, list(pd_sht.columns)) != -1}
            else:
                e_col_width={}
    
            if 'font_color' in e_sht_config:
                e_col_fontcol={get_col_index(a, list(pd_sht.columns)):b
                               for a,b in e_config[e_sht_name_map[s_sht]].get('font_color',{}).items()
                               if get_col_index(a, list(pd_sht.columns)) != -1}
            else:
                e_col_fontcol={}
    
            if 'fill_color' in e_sht_config:
                e_col_bgcol={get_col_index(a, list(pd_sht.columns)):b
                             for a,b in e_config[e_sht_name_map[s_sht]].get('fill_color',{}).items()
                             if get_col_index(a, list(pd_sht.columns)) != -1}
            else:
                e_col_bgcol={}
    
            for i in range(len(pd_sht.columns)):
                col_format = wb_workbook.add_format()
                try: col_format.set_num_format(e_col_form[i])
                except KeyError:  pass
                ws.set_column(i, i, e_col_width.get(i, 8), col_format)
    
            if e_sht_config.get('freeze_column_names', False):
                ws.freeze_panes(1, 0)
    
            if e_sht_config.get('add_autofilter', False):
                ws.autofilter(0, 0, 0, len(pd_sht.columns) - 1)
    
            for i in range(0, len(e_dataframes[s_sht].columns)):
               header_format=wb_workbook.add_format()
               header_format.set_color( e_col_fontcol.get(i, 'black'))
               header_format.set_bg_color(e_col_bgcol.get(i, 'white'))
               ws.write(0, i, e_dataframes[s_sht].columns[i], header_format)

    o_writer.save()
    o_writer.close()
    return True
    

def get_col_index(v_col, a_columns):
    if isinstance(v_col, int) and v_col < len(a_columns): #change to <= if non-zero indexed
        return v_col
    else:
        if v_col in a_columns:
            return a_columns.index(v_col)
        else:
            return -1
    

def validate_sheet_name(s_suggested_name, pd_dataframe, e_existing_sheet_names={}, e_sht_name_map=None):
    ''' Make sure sheetname is valid and unique for excel workbooks. '''
    s_cleaned = re.sub("[^A-Za-z0-9+@$^&(),.! _|-]", "", s_suggested_name)[:31]
    if s_cleaned in e_existing_sheet_names:
        i_count = 1
        while "{} ({})".format(s_cleaned[:27], i_count) in e_existing_sheet_names:
            i_count += 1
        s_cleaned = "{} ({})".format(s_cleaned[:27], i_count)

    e_existing_sheet_names[s_cleaned] = pd_dataframe
    if not e_sht_name_map is None:
        e_sht_name_map[s_suggested_name] = s_cleaned

    return s_cleaned
    
    
s_gray_light = '#F2F2F2'
s_green_light = '#B8FFB8'
s_red_light = '#F3DDDC'
s_blue_light = '#CEDFE6'
s_cyan = '#00FFFF'
s_yellow_medium = '#FFFF99'
s_green_medium = '#99FF99'
def header_color(s_col):
    #return the hex color for standard system columns, else light gray
    s_col = s_col.lower().strip()
    if s_col.startswith('rd :'): return s_yellow_medium
    if s_col.startswith('d :'): return s_green_medium
    if s_col in ['preparedpn','remainder','match']: return s_red_light
    if s_col in ['productgroup','referenceclass','pn']: return s_blue_light
    return s_gray_light