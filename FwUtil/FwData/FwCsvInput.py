import json
import codecs
import sys
import pandas as pd
import encodings

#from dataframe_input import DataframeInput
__all__ = ["CsvInput"]
__version__ = "0.0.3"

class CsvInput:
    """The CSVInput class is meant to take a csv as input, and turn it into a pandas dataframe. """

    def __init__(self, json_config ):
        """Construct the csv input object"""

        if type(json_config) == str:
            self.config = self.loadConfigFile( json_config ) #Load configurations, if it's a path
        elif type(json_config) == dict:
            self.config = json_config #Just use the config dictionary, if it's a dictionary

        if not self.config["file_name"] or not ".csv" in str(self.config["file_name"]).lower():
            #If the input path wasn't provided, or the input wasn't a csv file, quit.
            print("The config file is missing the input path. Or the input isn't a CSV file")
            return False
        else:#Otherwise use the input file
            self.input = self.config["file_name"]
            
        if 'file_delimiter' in self.config:
            s_delimit = self.config['file_delimiter']
            if s_delimit.lower() in ['detect','auto','automatic','unknown']: s_delimit = None
        
        try:
            self.df = pd.read_csv(self.input, sep=s_delimit,
                                  encoding=encodings.detect_bom(s_path=self.input, default="ISO-8859-2")) #Transform the input into a pandas dataframe
        except:
            self.df = pd.read_csv(self.input, sep=s_delimit, encoding=sys.getdefaultencoding()) #specify encoding if csv is from Excel

    def loadConfigFile( self, config_file_path ):
        """Returns the content of the json config file """
        with open( config_file_path, "r" ) as config_file:
            return json.loads( config_file.read() )
    
