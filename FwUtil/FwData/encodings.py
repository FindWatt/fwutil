import codecs,sys

__all__ = ["encoding_tester", "detect_bom"]
__version__ = "0.0.1"

def detect_bom(s_path, default=None):
    with open(s_path, 'rb') as f:
        raw = f.read(8) #will read less if the file is smaller
    for enc,boms in [
            ('utf-32',(codecs.BOM_UTF32_LE,codecs.BOM_UTF32_BE)),
            ('utf-16',(codecs.BOM_UTF16_LE,codecs.BOM_UTF16_BE)),
            ('utf-8-sig',(codecs.BOM_UTF8,)),
        ]:
        if any(raw.startswith(bom) for bom in boms): return enc
    return default

def encoding_tester(s_file, s_format='',
                    a_default_formats=['utf-8', 'ISO-8859-2', 'Windows-1252', 'cp1252', 'latin_1']):
    a_formats = [s_format]
    
    s_bom_encoding = detect_bom(s_file)
    if not s_bom_encoding in a_formats: a_formats.append(s_bom_encoding)
        
    a_formats += [s_default for s_default in a_default_formats if not s_default == s_format]
    
    s_sys_encoding = sys.getdefaultencoding()
    if not s_sys_encoding in a_formats: a_formats.append(s_sys_encoding)
        
    for s_encoding in a_formats:
        if not s_encoding: continue
        try:
            with open(s_file, "r", encoding=s_encoding) as file:
                for line in file: pass
            return s_encoding
        except:
            print("Couldn't read file with encoding: {}".format(s_encoding))
            
    print("No successful encoding found.")
    return False