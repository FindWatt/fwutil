import os, json
import sys
import pandas as pd
import warnings
try:
    import CsvInput
except:
    from . import CsvInput
try:
    import pymssql
except ImportError:
    pass

__all__ = ["SqlInput", "db_columns_from_list", "get_table_columns", "get_present_columns"]

if not "FW_SQL_PASSWORD" in os.environ or not os.environ["FW_SQL_PASSWORD"]:
    warnings.warn("The FW_SQL_PASSWORD environment variable is not set. SqlInput will not work as expected")

class SqlInput:
    """Turns a SQL table into a pandas df"""

    def __init__(self, json_config):
        """ Construct the spreadsheet input object """

        if type(json_config) == str:
            self.config = self.loadConfigFile( json_config ) #Load configurations from json file, if it's a path
        elif type(json_config) == dict:
            self.config = json_config #Just use the config dictionary, if it's a dictionary

        if not "ngram_cols" in self.config:
            print("No columns to build n-grams from where chosen")

        if not "table_id" in self.config:
            print("The config file is missing the table id")

        self.input = self.config["table_id"]
        if "input_type" in self.config:
            self.input_type = str(self.config["input_type"]).lower()
        else:
            self.input_type = 'sql'

        #Turn the sql table into a pandas dataframe
        if self.input_type ==  'sql':
            if type(self.input) == str:
                self.df = self.get_dataframe_from_sql(s_table_id=self.input )
            else:
                self.df = self.get_dataframe_from_sql(i_table_id=self.input )

        elif self.input_type ==  'historical':

            if not "unique_id_col" in self.config:
                self.config["unique_id_col"] = "SKU"
            if not "classification_col" in self.config:
                self.config["classification_col"] = "CH_Product Type"
            if not "ngram_cols" in self.config:
                self.config["ngram_cols"] = ["CH_Title","CH_Description","CH_Brand"]

            if type(self.input) == str:
                self.df = self.get_train_dataframe_from_sql(s_table_id=self.input )
            else:
                self.df = self.get_train_dataframe_from_sql(i_table_id=self.input, skuName=self.config["unique_id_col"],
                                                            ngVector=self.config["ngram_cols"], classificationName=self.config["classification_col"] )


    def get_dataframe_from_sql(self, columns=None, i_table_id=0, s_table_id=''):
        ''' [i_table_id] should be received from args as a number or [s_table_id] should be received from args as a string
        DataProfilingResults database contains result of all analyses '''
        if columns: columns = db_columns_from_list(columns) #stringify the optional list of columns for SQL
        if i_table_id > 0:
            # format of table of result : 'dpw_'+i_table_id+'_7'
            # 7 is the table of result of original data of a dataset, translated by rules
            s_table_id = ('dpw_%s_7') % (i_table_id)
        elif len(s_table_id) == 0:
            FwUtil.printerror("No SQL table specified.")
            sys.exit("No SQL table specified.")

        print("table_id: %s" % s_table_id)

        rows_list=[]
        try:
            with pymssql.connect("localhost", "sa", os.environ["FW_SQL_PASSWORD"], "tempdb") as conn :
                with conn.cursor(as_dict=True) as curs :
                    curs.execute(
                        'SELECT {} FROM DataProfilingResults.dbo.{}'.format(
                            columns or "*", #If a list of columns wasn't provided, select *
                            s_table_id.format
                        )
                    )
                    rows_list=curs.fetchall()
        except:
            FwUtil.printerror("Couldn't read from SQL table: DataProfilingResults.dbo.%s" % s_table_id)
            sys.exit("Couldn't read from SQL table: DataProfilingResults.dbo.%s" % s_table_id)

        # you can work with it as pd_train[0]['Product']
        return pd.DataFrame.from_dict(rows_list)

    def get_train_dataframe_from_sql(self, i_table_id=0, s_table_id='', skuName='SKU' ,ngVector='CH_Title,CH_Description,CH_Brand', classificationName='CH_Product Type'):
        ''' [i_table_id] should be received from args as a number or [s_table_id] should be received from args as a string
        SQL Script should return table of all seen skus for dataset: FWEHistory.dbo.sckt_IDProductType_getTrainData '''
        if i_table_id <= 0 and len(s_table_id) == 0:
            FwUtil.printerror("No SQL table specified.")
            sys.exit("No SQL table specified.")
        if i_table_id > 0:
            table_id = i_table_id
        else:
            table_id = s_table_id

        print("table_id: %s"%table_id)

        if type(ngVector) == list:
            ngVector = ",".join(ngVector)

        rows_list=[]
        try:
            with pymssql.connect("localhost", "sa", os.environ["FW_SQL_PASSWORD"], "tempdb") as conn :
                with conn.cursor(as_dict=True) as curs :
                    curs.execute("exec FWEHistory.dbo.sckt_IDProductType_getTrainData @table_id=%s,@class_id=0,@skuName=%s,@ngVector=%s,@classificationName=%s",(table_id,skuName,ngVector,classificationName))
                    rows_list=curs.fetchall()
        except:
            FwUtil.printerror("Couldn't read from SQL SKU Database: FWEHistory.dbo.sckt_IDProductType_getTrainData @table_id=%s,@class_id=0,@skuName=%s,@ngVector=%s,@classificationName=%s" % (table_id,skuName,ngVector,classificationName))
            sys.exit("Couldn't read from SQL SKU Database: FWEHistory.dbo.sckt_IDProductType_getTrainData @table_id=%s,@class_id=0,@skuName=%s,@ngVector=%s,@classificationName=%s" % (table_id,skuName,ngVector,classificationName))

        # you can work with it as pd_train[0]['Product']
        return pd.DataFrame.from_dict(rows_list)

    def loadConfigFile( self, config_file_path ):
        """Returns the content of the json config file """
        with open( config_file_path, "r" ) as config_file:
            return json.loads( config_file.read() )


def db_columns_from_list(list_of_columns):
    """ Stringifies a list of columns, to be used in a SQL statement."""
    return ", ".join(list_of_columns)

def get_table_columns(db_conn, table):
    """ Returns a list of the columns of a given table """
    with pymssql.connect("localhost", "sa", os.environ["FW_SQL_PASSWORD"], "tempdb") as conn :
        with db_conn.cursor(as_dict=True) as curs :
            curs.execute("SHOW columns FROM {}".format(table) )
            return [column_info[0] for column_info in curs.fetchall()]

def get_present_columns(predict_dict, columns_list):
    """ Gets a list of a column names present in predict_dict """
    matching_cols = []
    for col in FwUtil.flatten(FwUtil.flatten_and_get_dict_values(predict_dict)):
        if col in columns_list:
            matching_cols.append(col)
    return matching_cols
