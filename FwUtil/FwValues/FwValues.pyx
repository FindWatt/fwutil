import re
import numpy as np
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

__version__ = "0.0.22"
__all__ = ["is_empty", "num_to_string",
           "list_values", "fraction_to_float", "list_numbers",
           "value_list_sum", "value_list_avg", "value_list_stddev",
           "value_list_similarity", "value_list_variation",
           "group_contiguous_indexes",
           "vector_similarity", "average_vectors", "filter_vectors", "plottable_vectors",
           "delimited_strings_to_dicts"]


cpdef bint is_empty(v_val):
    ''' Pass in a value, and check to see if it is empty, nan or blank. '''
    cdef str s_val
    if isinstance(v_val, int):
        return False
    elif isinstance(v_val, (float, np.float32)):
        return np.isnan(v_val)
    elif isinstance(v_val, str):
        s_val = v_val.strip()
        if len(s_val) > 0:
            return False
        else:
            return True
    

cpdef str num_to_string(v_number):
    ''' Check if number should be whole or decimal and return appropriate string. '''
    if isinstance(v_number, str):
        return v_number
    elif isinstance(v_number, int):
        return str(v_number)
    elif isinstance(v_number, (float, np.float32)):
        if np.isnan(v_number):
            return ''
        elif v_number.is_integer():
            return str(int(v_number))
        else:
            return str(v_number)
    

cpdef list list_values(list a_values):
    ''' Pass in flat mixed list and get back non-empty values (excluding missing values, nans, empty strings). '''
    cdef a_filled = []
    cdef str s_val
    
    for v_val in a_values:
        if isinstance(v_val, int):
            a_filled.append(v_val)
        elif isinstance(v_val, (float, np.float32)):
            if np.isnan(v_val):
                pass
            else:
                a_filled.append(v_val)
        elif isinstance(v_val, str):
            s_val = v_val.strip()
            if len(s_val) > 0:
                a_filled.append(s_val)
    return a_filled
    
    
re_fraction = re.compile(r'(?P<sign>[+-]?)((?P<whole>\d+)[ +-])?(?P<num>\d+)/(?P<denom>[1-9]\d*)')
re_decimal = re.compile(r'(?P<sign>[+-]?)(?P<whole>\d+)(?P<dec>\.\d+)?')
cpdef double fraction_to_float(v_fraction):
    ''' Take a fraction and convert it into a float.
        Can accept formats like "-1/2", "11-1/2", "-11-1/2", "11 1/2", "11 1/2"
    '''
    cdef dict e_fract
    cdef double d_whole, d_num, d_denom, d_dec, d_value
    cdef int i_sign
    
    if isinstance(v_fraction, str):
        o_match = re_fraction.search(v_fraction.strip())
        if o_match:
            e_fract = o_match.groupdict()

            if 'num' in e_fract and 'denom' in e_fract:
                if e_fract.get('whole', 0.0):
                    d_value = float(e_fract['whole'])
                else:
                    d_value = 0.0

                d_num = float(e_fract.get('num', 0.0))
                d_denom = float(e_fract.get('denom', 0.0))
                if d_denom > 0:
                    d_value += (d_num / d_denom)

                if d_value > 0:
                    if e_fract.get('sign') == '-':
                        return -d_value
                    else:
                        return d_value
                else:
                    return np.nan
            else:
                return np.nan
            
        else:
            o_match = re_decimal.search(v_fraction.strip())
            if o_match:
                e_fract = o_match.groupdict()
                if 'whole' not in e_fract and 'dec' not in e_fract:
                    return np.nan
                d_value = 0
                
                if e_fract.get('whole', 0.0):
                    d_value += float(e_fract['whole'])
                    
                if e_fract.get('dec', 0.0):
                    d_value += float(e_fract['dec'])
                
                if e_fract.get('sign') == '-':
                    return -d_value
                else:
                    return d_value
    
    elif isinstance(v_fraction, (float, np.float32)):
        return v_fraction
    elif isinstance(v_fraction, int):
        return float(v_fraction)
    else:
        return np.nan
        
        
re_digits = re.compile('[^\d.]')
re_digits_signs = re.compile('[^\d./ +-]')
cpdef list list_numbers(list a_values):
    ''' Pass in flat mixed list and get back numbers (integers and non-nan floats)'''
    cdef ad_numbers = []
    cdef str s_val
    
    for v_val in a_values:
        if isinstance(v_val, int):
            ad_numbers.append(v_val)
        elif isinstance(v_val, (float, np.float32)):
            #if isnan(v_val):
            #    pass
            if np.isnan(v_val):
                pass
            else:
                ad_numbers.append(v_val)
        elif isinstance(v_val, str):
            try:
                ad_numbers.append(float(v_val.strip()))
            except:
                if '/' in v_val:
                    s_val = re_digits_signs.sub('', v_val)
                    v_val = fraction_to_float(s_val)
                    if not np.isnan(v_val):
                        ad_numbers.append(v_val)
                else:
                    s_val = re_digits.sub('', v_val)
                    try:
                        ad_numbers.append(float(s_val.strip()))
                    except:
                        pass
    return ad_numbers


cpdef double value_list_sum(list a_values):
    ''' Return sum of non np.nan numbers. '''
    cdef list ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        return sum(ad_vals)
    else:
        return np.nan


cpdef double value_list_avg(list a_values):
    ''' Return average of non np.nan numbers. '''
    cdef list ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        return np.mean(ad_vals)
    else:
        return np.nan

cpdef double value_list_stddev(list a_values):
    ''' Return StdDev of non np.nan numbers. '''
    cdef list ad_vals = list_numbers(a_values)
    try:
        return np.std(ad_vals)
    except:
        return np.nan


cpdef double value_list_similarity(list a_values):
    ''' Return similarity of non np.nan numbers as factor. '''
    cdef list ad_vals = list_numbers(a_values)
    cdef double d_mean
    if len(ad_vals) > 0:
        d_mean = np.mean(ad_vals)
        if d_mean != 0:
            return 1 - (np.std(ad_vals) / np.mean(ad_vals))
        else:
            # for now return 0.0.
            # The ideal solution would be to adjust the numbers to all possitive then run again
            return 0.0
    else:
        return np.nan


cpdef double value_list_variation(list a_values, bint b_sort=False):
    ''' Return variation of non np.nan numbers as % of largest number. '''
    cdef list ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        d_max, d_min = max(ad_vals), min(ad_vals)
        if d_max == 0: return -d_min
        if d_max < 0: d_max, d_min = -d_min, -d_max
        return ((d_max - d_min) / d_max)
    else:
        return np.nan
    

cpdef list group_contiguous_indexes(list a_indexes, b_sort=False):
    ''' join contiguous integer indexes into subgroups in the list '''
    cdef int before, current
    if not a_indexes: return []
    
    if b_sort:
        a_indexes = sorted(a_indexes)
    
    cdef list a_phrases = [[a_indexes[0]]]
    for before, current in zip(a_indexes, a_indexes[1:]):
        if before + 1 == current or before == current:
            a_phrases[-1].append(current)
        else:
            a_phrases.append([current])

    return a_phrases


cpdef float vector_similarity(vec1, vec2):
    ''' Calculate cosine similarity/distance between two large-dimensional arrays. '''
    if vec1 is None or vec2 is None: return 0.0
    return np.dot(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))
    
    
def average_vectors(*args, int i_dimensions=0, list a_weights=None):
    ''' Calculate the average centerpoint of a group of vectors.
        Optionally use weights. '''
    cdef list a_vectors
    if isinstance(args[0], list): a_vectors = args[0]
    elif isinstance(args[0], tuple): a_vectors = list(args[0])
    else: a_vectors = list(args)
    if i_dimensions == 0: i_dimensions = len(a_vectors[0])
    
    new_vec = np.zeros((i_dimensions,), dtype="float32")
    if not a_weights:
        for vec in a_vectors:
            new_vec = np.add(new_vec, vec)
    else:
        for vec, weight in zip(a_vectors, a_weights):
            new_vec = np.add(new_vec, np.multiply(vec, weight))
    return np.divide(new_vec, len(a_vectors))
    

def filter_vectors(a_vectors, a_labels=None):
    ''' Pass in list of vector arrays and string labels.
        Return filtered lists with no None or NaN values. '''
    a_new_vectors = []
    if a_labels:
        a_new_labels = []
        for vector, label in zip(a_vectors, a_labels):
            if vector is not None and not np.isnan(np.sum(vector)):
                a_new_vectors.append(vector)
                a_new_labels.append(label)
        return a_new_vectors, a_new_labels
    else:
        for vector in a_vectors:
            if vector is not None and not np.isnan(np.sum(vector)):
                a_new_vectors.append(vector)
        return a_new_vectors


def plottable_vectors(a_vectors, a_labels=None, dimension_transformer=None, i_dimensions=3):
    ''' Reduce the dimensions using a passed in fitted transformer with a transform function,
        or by a creating a PCA transformer and using that. '''
    np_vector_table = np.vstack([vec for vec in a_vectors if vec is not None])
    # the array of vectors is the X element
    # the labels are the Y element

    if i_dimensions >= np_vector_table.shape[0]:
        i_dimensions = np_vector_table.shape[0] - 1
    
    if np_vector_table.shape[0] == i_dimensions:
        np_reduced_vectors = np_vector_table
    else:
        if dimension_transformer and dimension_transformer not in ['PCA', 'LDA', 'pca', 'lda']:
            # print("Using passed in transformer")
            o_transformer = dimension_transformer
            np_reduced_vectors = o_transformer.transform(np_vector_table)
        elif dimension_transformer in ['LDA', 'lda']:
            # print("Using LDA transformer")
            o_transformer = LDA(n_components=i_dimensions)
            np_reduced_vectors = o_transformer.fit_transform(np_vector_table, a_labels)
            dimension_transformer = o_transformer
        else:
            # print("Using PCA transformer")
            o_transformer = PCA(i_dimensions)
            np_reduced_vectors = o_transformer.fit_transform(np_vector_table)
            dimension_transformer = o_transformer
    dimension_transformer = o_transformer
    
    return np_reduced_vectors.transpose()
    

# this is deliberately non-pythonic to focus completely on speed.
cpdef list delimited_strings_to_dicts(list a_dict_strings,
                                      str s_pairs_delimiter=",",
                                      str s_key_val_delimiter="="):
    ''' Take in a list of strings containing delimited key/values.
        Return list of dictionaries. '''
    cdef str s_row, s_key, s_val
    cdef int i_char, i_key_start, i_val_start, i_len
    cdef dict e_row
    cdef list ae_rows = []
    
    for s_row in a_dict_strings:
        i_len = len(s_row)
        if i_len == 0:
            ae_rows.append({})
            continue
            
        e_row, s_key, s_val, i_key_start, i_val_start, i_old_key = ({}, '', '', 0, 0, 0)
        for i_char in range(i_len):
            if s_row[i_char] == s_pairs_delimiter:
                if i_char <= i_key_start:  # handle repeated pair delimiters
                    i_key_start = i_char + 1
                elif i_val_start > i_key_start:
                    s_key = s_row[i_key_start:i_val_start - 1].strip()
                    s_val = s_row[i_val_start:i_char].strip()
                    
                    e_row[s_key] = s_val
                    i_old_key = i_key_start
                    i_key_start = i_char + 1
                else:
                    ''' There wasn't a key-val delimiter since this is the last key
                        so make the last key include this value also.
                        This handles things like extra commas inside the value
                        when commas are also the attribute delimiter '''
                    i_key_start = i_old_key
                    
            elif s_row[i_char] == s_key_val_delimiter:
                i_val_start = i_char + 1
        
        # finish up string
        if i_key_start < i_char:
            s_key = s_row[i_key_start:i_val_start - 1].strip()
            s_val = s_row[i_val_start:i_len].strip()
            e_row[s_key] = s_val
                    
        ae_rows.append(e_row)
    return ae_rows
