'''
This module is for generic functions
'''
import os
from os import path, walk
import zipfile


__version__ = "0.0.5"
__all__ = [
    "path_exists", "file_exists", "folder_from_path", "filename_from_path",
    "file_from_path", "ext_from_path", "add_suffix_to_file", "get_new_write_filename", "create_new_zip",
    "unzip_to_folder", "get_file_list", "get_pickle_files"
]


def path_exists(s_path):
    ''' check if folder or file exists '''
    return os.path.exists(s_path)


def file_exists(s_path):
    ''' check if file exists '''
    return os.path.isfile(s_path)


def folder_from_path(s_path):
    ''' Return folder from path. '''
    s_folder = os.path.dirname(s_path)
    if len(s_folder) > 0:
        s_folder += os.path.sep
    return s_folder

def filename_from_path(s_path):
    ''' Return filename from path. '''
    return os.path.basename(s_path)


def file_from_path(s_path):
    ''' Return file without extension from path. '''
    s_file = os.path.basename(s_path)
    return os.path.splitext(s_file)[0]


def ext_from_path(s_path):
    ''' Return file extension from path. '''
    s_file = os.path.basename(s_path)
    return os.path.splitext(s_file)[1]


def add_suffix_to_file(s_path, s_suffix):
    ''' Return file path with suffix added after filename and before extension. '''
    s_file = "{}{}{}{}".format(
        folder_from_path(s_path), file_from_path(s_path), s_suffix, ext_from_path(s_path)
    )
    return s_file


def get_new_write_filename(s_path):
    ''' Get a new, unoccupied filename. If a file already exists, add a number
        to the end of the filename and increment it upwards until no existing file is found.
        Then return new filename to write to.'''
    if not isinstance(s_path, str):
        return ""
    s_folder = folder_from_path(s_path)
    s_file = os.path.basename(s_path)

    if not path_exists(s_folder): #if the folder doesn't exists
        return ""
    if not file_exists(s_folder + s_file): #if a file doesn't exist with that name
        #Filepaths were coming out doubled, when only a filename was passed to this function
        return s_folder + s_file if s_folder != s_file else s_file

    s_base, s_ext = os.path.splitext(s_file)
    i_count = 1
    while file_exists(s_folder + s_base + "-" + str(i_count) + s_ext):
        i_count += 1
    return s_folder + s_base + "-" + str(i_count) + s_ext


def create_new_zip(s_zip_path, a_files):
    ''' Pass in the new zip file path and a list of file paths to insert in it. '''
    #We expect a_files to be a list, but we're not validating for it
    if not isinstance(a_files, list):
        a_files = [a_files]
    with zipfile.ZipFile(s_zip_path, mode='w') as o_zip: #could add check for append
        for s_file in a_files:
            o_zip.write(s_file, filename_from_path(s_file))
        o_zip.close()
    return s_zip_path #useful if the zip file gets renamed to not replace existing file.


def unzip_to_folder(s_zip_path, s_folder):
    ''' Pass in the zip file path and a folder path to extract its files to. '''
    zipfile.ZipFile(s_zip_path).extractall(s_folder)


def get_file_list(s_folder, e_exts=None):
    ''' Pass in a folder path and a set of extensions.
        Return a list of files matching any extension as absolute paths. '''
    a_files = []
    if e_exts:
        e_valid_exts = set([
            s_ext if s_ext.startswith('.') else '.{}'.format(s_ext) for s_ext in e_exts
        ])

        for (dirpath, dirnames, filenames) in walk(s_folder):
            a_files.extend([
                path.join(dirpath, f) for f in filenames if os.path.splitext(f)[1] in e_valid_exts
            ])
            break
    else:
        for (dirpath, dirnames, filenames) in walk(s_folder):
            a_files.extend([path.join(dirpath, f) for f in filenames])
            break
    return a_files


def get_pickle_files(s_main_pickle_file):
    ''' Get a list of pickle files associated with a main pkl file. '''
    s_folder = folder_from_path(s_main_pickle_file)
    s_main_file = filename_from_path(s_main_pickle_file)
    a_files = get_file_list(s_folder, ['.pkl', '.npz', '.npy', '.z'])
    a_pkl_files = [s_file for s_file in a_files if s_main_file in s_file]
    return a_pkl_files
