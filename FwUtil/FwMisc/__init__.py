#import pyximport; pyximport.install()
try:
    from FwPath import *
except:
    from .FwPath import *

try:
    from FwOther import *
except:
    from .FwOther import *

try:
    from parallel_processing import *
except ImportError:
    from .parallel_processing import *

try:
    import amazonS3
except:
    from . import amazonS3
