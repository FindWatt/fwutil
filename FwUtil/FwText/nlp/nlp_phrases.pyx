import nltk.data
from nltk.tokenize import sent_tokenize
#import spacy
from spacy.tokens.doc import Doc as spDoc
from spacy.tokens.span import Span as spSpan
from spacy.tokens.token import Token as spTok

#NLP = spacy.en.English()

__version__ = "0.0.3"
__all__ = ["get_text", "merge_hyphenated", "noun_phrases"]

cdef set e_merge_types = set(['NOUN', 'PROPN', 'NUM', 'ADJ', 'VERB'])


def get_text(nlp_doc):
    return [word.text for word in nlp_doc]


def merge_hyphenated(nlp_doc):
    ''' Merge hyphenated tokens in a sentence together if either
    is a noun, and there is no space between hyphen and words. '''
    nlp_merged = nlp_doc
    cdef int i_index = 1
    cdef int i_start, i_end
    cdef str s_tag, s_lemma
    
    while i_index < len(nlp_doc) - 1:
        nlp_hyphen = nlp_doc[i_index]
        if (
            nlp_hyphen.text == '-' and
            nlp_merged.text[nlp_hyphen.idx - 1] != ' ' and
            nlp_merged.text[nlp_hyphen.idx + 1] != ' '
        ):
            nlp_before = nlp_doc[i_index - 1]
            nlp_after = nlp_doc[i_index + 1]
            if nlp_before.pos_ in e_merge_types or nlp_after.pos_ in e_merge_types:
                i_start = nlp_before.idx
                i_end = nlp_after.idx + len(nlp_after.text)
                
                if nlp_before.pos_ == 'NOUN' or nlp_after.pos_ == 'NOUN':
                    s_tag = 'NN'
                elif nlp_before.pos_ == 'ADJ' or nlp_after.pos_ == 'ADJ':
                    s_tag = 'JJ'
                elif nlp_before.pos_ == 'VERB' or nlp_after.pos_ == 'VERB':
                    s_tag = 'JJ'
                else:
                    s_tag = 'NN'
                
                s_lemma = '-'.join([nlp_before.lemma_, nlp_after.lemma_])
                i_ent_type = max([nlp_before.ent_type, nlp_after.ent_type])
                nlp_doc.merge(i_start, i_end, tag=s_tag, lemma=s_lemma, ent_type=i_ent_type)
                i_index = max(i_index - 2, 1)
                
            else:
                i_index += 1
        else:
            i_index += 1
    return nlp_merged


cdef set e_np_root_pos = set(["NOUN", "PROPN"])
cdef set e_np_pos = set(["NOUN", "PROPN", "ADJ", "VBG"])
cpdef noun_phrases(nlp_sentence, bint b_merge_hyphenated=True):
    ''' Get the noun phrases from a sentence that has been parsed and
        tagged by spacy.
        Returns noun phrases that noun_chunks doesn't. '''
    if b_merge_hyphenated: nlp_sentence = merge_hyphenated(nlp_sentence)
        
    cdef list a_phrases = []
    cdef int i_end = len(nlp_sentence) - 1
    cdef int i_start
    
    while i_end >= 0:
        if nlp_sentence[i_end].pos_ in e_np_root_pos:
            i_start = i_end
            while i_start > 0 and nlp_sentence[i_start - 1].pos_ in e_np_pos:
                i_start -= 1
                
            a_phrases.insert(0, nlp_sentence[i_start:i_end + 1])
            i_end = i_start - 1
        else:
            i_end -= 1

    return a_phrases
