from __future__ import absolute_import
import os, re, string, sys
import unicodedata
from unicodedata import category, normalize
import itertools
from operator import itemgetter
import numpy as np
import nltk
from nltk import stem
from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from .dimension_patterns import (
    DimensionsTokenizer, e_dimension_names, e_count_words,
    dimensions_tokenizer, dimensionizer)
from .html_characters import get_html_char_codes

__version__ = "0.0.75"
__all__ = ["title_tokenize", "description_tokenize", "word_tokenize",
           "word_indexes",
           "trim", "remove_hyphens", "remove_apostrophes", "remove_possessives",
           "remove_periods", "fix_sentence_periods",
           "replace_ampersands", "replace_with", "remove_trademarks", "remove_slashes",
           "remove_html_characters", "replace_html_characters",
           "sort_tokens_by_length", "pos_contained_in_spans", "unique_subset_strings", "unique_regex_results",
           "all_ngrams", "all_ngrams_lists", "all_ngrams_flat", "all_ngrams_tuples", "end_grams", "skip_bigrams",
           "appendRelativePositionToTokens", "remove_stopwords", "is_alpha_numeric", "has_alpha_numeric",
           "extractNLTKStems", "strip_quotes", "split_strip", "split_words",
           "split_camelcase", "split_number_uom", "split_edge_punctuation", "split_edge_quotes",
           "join_oxford", "join_sentence", "strip_unicode_accents",
           "clean_text", "clean_title", "clean_description",
           "clean_group_of_dicts", "clean_list", "remove_all_punctuation", "are_substrings_consecutive",
           "edit_distance", "text_differences", "extract_nltk_stems",
           "text_list_to_ngram_list", "ngram_freq_coverage_in_list",
           "ngram_freq_in_list", "tuple_ngram_freq_in_list", "ngram_coverage_in_list",
           "ngram_table", "get_ngram_count", "combine_plural_singular_stems",
           "word_in_text", "word_instr", "find_char_sequence", "find_word_sequence",
           "pluralize", "singularize", "pluralize_tuple", "singularize_tuple",
           "get_token_spans",
           "get_phrase_position", "remove_doc_substring", "split_on_edge_punctuation",
           "get_quoted_phrases", "get_quoted_phrases_and_pos", "get_quoted_phrase_words", 
           "is_compound_word", "compound_word_in_ngrams", "get_compounds",
           "e_dimension_names", "are_two_substrings_consecutive", "are_consecutive_ordered",
           "tuple_ngram_coverage_in_list",
           "re_mpn", "SRE_MATCH_TYPE", "nltk_stemmer",
           "s_utf8_punctuation" , "re_punct_noperiod", "re_alphanum", "re_alpha", "re_num",
           ]
 
SRE_MATCH_TYPE = type(re.match("", ""))

s_utf8_punctuation = r"!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~¡¢£¤¥¦§¨ª«¬¯°±´µ¶·¹º»¿×÷∞"
re_alphanum = re.compile(r"[A-Z0-9]+", re.I)
re_alpha = re.compile(r"[A-Z]+", re.I)
re_num = re.compile(r"[0-9]+")
re_punct = re.compile(r"[^A-Z0-9]+", re.I)
re_punct_nospace = re.compile(r"[^A-Z0-9' \t]+", re.I)
#re_punct_noperiod = re.compile("[^A-Z0-9'.-]+", re.I)
re_punct_noperiod = re.compile(r"[{}]+".format(''.join(
    [re.escape(s_char) for s_char in list(s_utf8_punctuation)
     if s_char not in "'./-&"]
)), re.I)
re_punct_nospace_noperiod = re.compile(r"[^A-Z0-9'./ \t]+", re.I)
re_whitespace = re.compile(r"[ \t\r\n]+", re.I)
re_str_punct = re.compile("r[" + re.escape(''.join([x for x in string.punctuation if x not in "'-_"])) + "]+")
s_qt = '"'
re_trademark_symbols = re.compile(r"([™®©]|(?<! [A-Z])[T][Mm](?=[ .,?!():;" + s_qt + "\r\n-]|'[Ss]|$))")
re_trademark_words = re.compile(r"(trademark|registered|copyright|&trade;|&reg;|&copy;)", re.I)

nltk_stemmer = stem.SnowballStemmer("english").stem

# English Stopwords from nltk.corpus
cpdef set c_english_stopw = set(stopwords.words("english"))
# # #

re_misc = re.compile("(tm +|\(tm\)|\(registered\)|\(r\)|'s )", re.I)
re_digits = re.compile("\d")
a_mpn_patterns = [
    r"#?[a-z]{2,}-?0+(-?[a-z]+0+)+",  # mpn_1
    r"#?[a-z]{2,}(-?0+-?[a-z]+)+",  # mpn_2
    r"#?[a-z]{2,}(-?0+[a-z]+)+",  # mpn_3
    r"#?[a-z]{2,}-?0+",  # mpn_4
    r"#?0{4,}",  # mpn_5
    r"#?0{2,}-?([a-z]{3,}-?|[a-z]+)0+(-?([a-z]+|0+))*",  # mpn_6
    r"#?0{2,}-?[a-z]{3,}",  # mpn_7
    r"[a-z]+0+([a-z]+0+)+",  # mpn_8
    # r"0+\.0+(\.0+)+"  # mpn_9
    r"0+(\.0+){2,}"  # mpn_9
]

re_mpn = re.compile("({})".format('|'.join(a_mpn_patterns)), re.I)
re_punct_utf = re.compile(u"[+(),;" + b'\xc2\xa9\xc2\xae\xe2\x80\xa2\xe2\x84\xa2'.decode("utf8") + "]")
re_spaces = re.compile(" {2,}")

cdef set c_ascii = set(
    [s for s in
     r'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,?!@#$%^&*()-_+=";:<>[]{}\|/']
)
cdef set c_digits = set("0123456789")

#re_punctuation_with_exceptions = re.compile("[^A-Z0-9 .'&_-]+", re.I)
re_punctuation_with_exceptions = re.compile("[{}]+".format(''.join(
    [re.escape(s_char) for s_char in s_utf8_punctuation if s_char not in " .'&_-"]
)), re.I)

cdef set c_no_sep = set("/-")
cdef set c_no_preceding = set(",.?!%")
cdef set c_no_following = set("#$")


cpdef list title_tokenize(
    str title, stop_words=None, phrases=None, int max_phrase_len=4,
    bint use_underscore=True, bint return_tuples=False, bint return_lower=False
):
    """
    @description: Title tokenizer that splits on spaces and some punctuation.
    Pre-parses to handle standard title expectations.
    Can remove stopwords, and join phrases in a passed in dictionary/set.
    @arg title: {str or unicode} The string to tokenize.
    @arg stop_words: Either the string "english" to remove English stopwords,
    or a list of words to be cleaned from the string.
    @arg phrases: {set|dict} a set of phrases to consolidate into single tokens
    @arg max_phrase_len: {int} the maximum length of an ngram to look for in the phrase list
    @arg use_underscore: {bin} whether to use '_' in joining phrases
    @arg return_tuples: {bin} return subphrases as tuples in the tokenized list instead of joining with ' ' or '_'
    @arg return_lowercase: {bin} convert words to lowercase before returning
    @return: {list} A list of the tokens extracted from the document
    """
    cdef int i_first_word, i_max_phrase_len, i_last_word
    cdef tuple t_dims, t_phrase, t_phrase_lower
    cdef set c_dims, c_stop_words
    cdef str s_word, s_phrase
    cdef str s_title = strip_unicode_accents(title)
    
    t_dims = dimensionizer(s_title)
    if use_underscore:
        c_dims = set([re.sub(' ', '_', clean_title(dim.string_match)) for dim in t_dims[1]])
    else:
        c_dims = set([clean_title(dim.string_match) for dim in t_dims[1]])
    
    s_title = clean_title(s_title)
    cdef list a_tokenized = s_title.split()
    
    if not phrases: phrases = []
    
    if a_tokenized and (phrases or c_dims):
        i_first_word = 0
        while i_first_word < len(a_tokenized) - 1:
            for i_last_word in reversed(range(i_first_word + 2, max(len(a_tokenized) + 1, 1 + max_phrase_len))):
                t_phrase = tuple(a_tokenized[i_first_word:i_last_word])
                t_phrase_lower = tuple([word.lower() for word in a_tokenized[i_first_word:i_last_word]])
                if use_underscore:
                    s_phrase = '_'.join(t_phrase)
                else:
                    s_phrase = ' '.join(t_phrase)

                if (
                    s_phrase in c_dims or t_phrase in phrases or
                    s_phrase.lower() in phrases or t_phrase_lower in phrases
                ):
                    if return_tuples:
                        a_tokenized[i_first_word:i_last_word] = [t_phrase]
                    else:
                        a_tokenized[i_first_word:i_last_word] = [s_phrase]
                    break

            i_first_word += 1
    
    if stop_words:
        if stop_words is "english": c_stop_words = c_english_stopw
        elif isinstance(stop_words, set): c_stop_words = stop_words
        elif isinstance(stop_words, (list, tuple)): c_stop_words = set([s_word.lower() for s_word in stop_words])
        a_tokenized = [x for x in a_tokenized
                       if not isinstance(x, str) or x.lower() not in c_stop_words]

    if return_lower:
        a_tokenized = [x.lower() if isinstance(x, str)
                       else tuple([s_word.lower() for s_word in x])
                       for x in a_tokenized]

    return a_tokenized
    

cpdef list description_tokenize(
    str description, stop_words=None,
    phrases=None, int max_phrase_len=4,
    bint use_underscore=True, bint return_sentences=True,
    bint return_tuples=False, bint return_lower=False,
):
    """
    @description: Description tokenizer that divides the description into sentences, and then into words.
                  Splits on spaces and some punctuation.
    Pre-parses to handle standard description expectations.
    Can remove stopwords, and join phrases in a passed in dictionary/set.
    @arg description: {str or unicode} The string to tokenize.
    @arg stop_words: Either the string "english" to remove English stopwords,
    or a list of words to be cleaned from the string.
    @arg phrases: {set|dict} a set of phrases to consolidate into single tokens
    @arg max_phrase_len: {int} the maximum length of an ngram to look for in the phrase list
    @arg use_underscore: {bin} whether to use '_' in joining phrases
    @arg return_sentences: {bin} return list of sentences that are list of words, instead of just a list of all words
    @arg return_tuples: {bin} return subphrases as tuples in the tokenized list instead of joining with ' ' or '_'
    @return: {list} A list of the tokens extracted from the document
    """
    cdef int i_first_word, i_max_phrase_len, i_last_word
    cdef tuple t_dims, t_phrase, t_phrase_lower
    cdef list a_description, a_sentences, a_tokenized
    cdef set c_dims, c_stop_words
    cdef str s_word, s_phrase
    cdef str s_description = strip_unicode_accents(description)
    
    t_dims = dimensionizer(description)
    if use_underscore:
        c_dims = set([re.sub(' ', '_', clean_description(dim.string_match)) for dim in t_dims[1]])
    else:
        c_dims = set([clean_description(dim.string_match) for dim in t_dims[1]])
    
    a_sentences = [clean_description(sentence) for sentence in sent_tokenize(description)]
    
    if not phrases: phrases = []
    
    a_description = []
    for s_sentence in a_sentences:
        a_tokenized = s_sentence.split()

        if a_tokenized and (phrases or c_dims):
            i_first_word = 0
            while i_first_word < len(a_tokenized) - 1:
                for i_last_word in reversed(range(i_first_word + 2, max(len(a_tokenized) + 1, 1 + max_phrase_len))):
                    t_phrase = tuple(a_tokenized[i_first_word:i_last_word])
                    t_phrase_lower = tuple([word.lower() for word in a_tokenized[i_first_word:i_last_word]])
                    if use_underscore:
                        s_phrase = '_'.join(t_phrase)
                    else:
                        s_phrase = ' '.join(t_phrase)

                    if (
                        s_phrase in c_dims or t_phrase in phrases or
                        s_phrase.lower() in phrases or t_phrase_lower in phrases
                    ):
                        if return_tuples:
                            a_tokenized[i_first_word:i_last_word] = [t_phrase]
                        else:
                            a_tokenized[i_first_word:i_last_word] = [s_phrase]
                        break

                i_first_word += 1

        if stop_words:
            if stop_words is "english": c_stop_words = c_english_stopw
            elif isinstance(stop_words, set): c_stop_words = stop_words
            elif isinstance(stop_words, (list, tuple)): c_stop_words = set([s_word.lower() for s_word in stop_words])
            a_tokenized = [x for x in a_tokenized
                           if not isinstance(x, str) or x.lower() not in c_stop_words]

        if return_lower:
            a_tokenized = [x.lower() if isinstance(x, str)
                           else tuple([s_word.lower() for s_word in x])
                           for x in a_tokenized]
        
        if return_sentences:
            a_description.append(a_tokenized)
        else:
            a_description.extend(a_tokenized)
        
    return a_description
    

cpdef list word_tokenize(str s_text, bint remove_punct=True, stop_words="english",
                         vocabulary=None, bint return_lower=False, bint convert_ampersand=False):
    """
    @description: Simple tokenizer that splits on spaces.Can remove punctuation
    and stopwords.
    @arg text: {str or unicode} The string to tokenize.
    @arg remove_punct: {bool} Whether to remove punctuation.
    @arg stop_words: Either the string "english" to remove English stopwords,
    or a list of words to be cleaned from the string.
    @return: {list} A list of the tokens extracted from the document
    """
    cdef list a_tokenized
    cdef str s_word
    
    if convert_ampersand:
        s_text = replace_ampersands(s_text)
        
    if remove_punct:
        s_text = remove_hyphens(
            remove_slashes(
                remove_apostrophes(
                    remove_periods(
                        re_punct_noperiod.sub(' ', s_text)
                    )
                )
            ),
            b_dehyphenate_phrases=True
        )

    if stop_words:
        a_tokenized = remove_stopwords(s_text.split(), stop_words=stop_words)
    else:
        a_tokenized = s_text.split()

    if return_lower:
        a_tokenized = [x.lower() for x in a_tokenized]
                       
    if vocabulary is not None:
        return [s_word for s_word in a_tokenized if s_word in vocabulary]
    else:
        return a_tokenized


cpdef list word_indexes(list a_text, str s_text, bint b_case_insensitive=False):
    ''' Turn word list into a list of tuples of character indexes in a string.
        This assumes the word list is in the exact same order as the string,
        but that punctuation may have been added or removed. '''
    if not s_text or not a_text: return []
    if b_case_insensitive:
        s_text = s_text.lower()
        a_text = [word.lower() for word in a_text]
    
    cdef int i_pos = 0
    cdef list a_pos = []
    cdef str s_word
    cdef int i_start, i_end
    
    for s_word in a_text:
        i_start = s_text.find(s_word, i_pos)
        if i_start == -1:
            a_pos.append(None)
        else:
            i_end = i_start + len(s_word)
            a_pos.append((i_start, i_end))
            i_pos = i_end
    return a_pos


cpdef trim(str text):
    ''' Remove leading and trailing whitespace characters,
        and replace multiple consecutive ones inside text with single space. '''
    return re_whitespace.sub(' ', text).strip()
    
    
cdef int i_min_char_overlap = 4
cdef int i_min_word_len = 3
cpdef str remove_hyphens(str s_text, bint b_dehyphenate_phrases=False):
    ''' The purpose of this function to remove dashes,
        but keep hyphenated phrases and acronyms (like "multi-colored" and "CD-ROM").
        I'm not sure yet if this will eventually be included in the standard word_tokenize
        pipeline the way remove_periods or remove_apostrophes are.
        Remove hyphens with a space on either side,
        Remove when part of a two-word phrase where one piece is
        contained in the other and longer than 3 characters.
        b_dehyphenate_words: optionally de-hypenate full words where both are longer. '''
    s_text = re.sub(r"^-|-$", '', re.sub(r"(?<![A-Za-z0-9])-(?![A-Za-z0-9])", ' ', s_text))
    a_bigrams = re.finditer(r"((?<![A-Za-z-])[A-Za-z]+)-([A-Za-z]+(?![A-Za-z-]))", s_text)
    cdef list as_text = list(s_text)
    cdef str s_prefix, s_suffix
    cdef int i_hyphen
    cdef bint b_overlap
    
    if a_bigrams:
        for re_match in a_bigrams:
            s_prefix, s_suffix = re_match.group(1), re_match.group(2)
            s_pre_lower, s_suf_lower = s_prefix.lower(), s_suffix.lower()
            
            if not s_prefix or not s_suffix or \
               min(len(s_prefix), len(s_suffix)) < i_min_char_overlap: b_overlap = False
            if s_pre_lower in s_suf_lower or s_suf_lower in s_pre_lower: b_overlap = True
            else: b_overlap = False
                
            if b_overlap:
                i_hyphen = re_match.start() + len(s_prefix)
                as_text[i_hyphen] = ' '
            elif (
                b_dehyphenate_phrases and
                min(len(s_prefix), len(s_suffix)) >= i_min_word_len and
                not s_prefix.isupper() and
                not s_suffix.isupper()
            ):
                i_hyphen = re_match.start() + len(s_prefix)
                as_text[i_hyphen] = ' '
                
    return ''.join(as_text)
    
    
cpdef str remove_apostrophes(str s_text):
    """ Finds '' surrounding words and removes them. ' in the middle of a word
    (o'clock, Edd's, etc.) are not replaced. """
    return re.sub(r"(?<![A-Za-z0-9])'|'(?![A-Za-z])", '', s_text)


cpdef str remove_possessives(str s_text):
    """ Find possessive like "'s" or "s'" at the end of words and removes it. """
    # singular possessive
    s_text = re.sub(r"(?<=[A-Za-z0-9])'[Ss]", '', s_text)
    # plural possessive
    s_text = re.sub(r"(?<=[A-Za-z0-9])[Ss]'", 's', s_text)
    return s_text


cpdef str remove_periods(str s_text, s_replace=' '):
    ''' Remove periods in text that are there as punctuation
        while leaving in those acting as decimal points or in acronyms. '''
    cdef int i_pos, ind, i_len
    cdef list clean_chars
    cdef str char
    i_len = len(s_text)
    
    clean_chars = []
    for ind, char in enumerate(s_text):
        if char != ".":
            clean_chars.append(char)
        elif ind + 1 < i_len:
            if s_text[max(ind - 1, 0)] in c_digits and s_text[ind + 1] in c_digits:
                clean_chars.append(char)
            elif s_text[max(ind - 1, 0)].isupper() and s_text[ind + 1].isupper():
                clean_chars.append(char)
            else:
                clean_chars.append(s_replace)
        else:
            clean_chars.append(s_replace)

    return "".join(clean_chars).strip()


cpdef str fix_sentence_periods(str s_text):
    ''' Fix periods in text that are supposed to be acting as sentence separators,
        but are missing trailing spaces.
        Ignore periods between digits (decimals),
        and periods between uppercase letters (Acronyms). '''
    cdef int i_pos, ind, i_len
    cdef list clean_chars
    cdef str char
    i_len = len(s_text)
    
    clean_chars = []
    for ind, char in enumerate(s_text):
        if char != ".":
            clean_chars.append(char)
        elif ind + 1 < i_len and not re_whitespace.search(s_text[ind + 1]):
            
            if s_text[max(ind - 1, 0)] in c_digits:
                if s_text[ind + 1] in c_digits:
                    clean_chars.append(".")
                else:
                    clean_chars.append(". ")
            elif s_text[max(ind - 1, 0)].isupper() and s_text[ind + 1].isupper():
                clean_chars.append(".")
            else:
                clean_chars.append(". ")
        else:
            clean_chars.append(".")

    return "".join(clean_chars).strip()


cpdef str replace_ampersands(str s_text):
    ''' Finds & between words and replaces them with " and "
        Not sure whether or not to do the same inside abbreviation (like "A&W").
        HTML character codes should be removed before calling this function.
    '''
    s_text = re.sub(r"^(\W)*&+ +|&+(\W)*$", '\g<1>', s_text)
    s_text = re.sub(r"(?<=[a-z])&(?=[a-z])", ' and ', s_text)
    return re.sub(r" *& +", ' and ', s_text)


cpdef str replace_with(str s_text):
    ''' Finds abbreviated version of "with" and replaces them with the word '''
    return re.sub(r"(^| +)w(/ *| +)(?![.?!])", ' with ', s_text, flags=re.I).strip()


cpdef str remove_trademarks(str s_text):
    ''' Remove UTF/HTML chars and words for trademark, registered, and copyright. '''
    return re_trademark_words.sub('', re_trademark_symbols.sub('', s_text))


cpdef str remove_slashes(str s_text):
    ''' remove slashes that aren't part of a fraction '''
    s_text = re.sub(r"((?<=[^0-9 ])|^) */", " ", s_text)
    return re.sub(r"/ *((?=[^0-9 ])|$)", " ", s_text).strip()


cpdef str remove_html_characters(str s_text):
    ''' Finds html characters in the pattern of &___; and removes '''
    return re.sub(r"&#?[A-Z0-9a-z]{2,8};", '', s_text, re.I)

# dict of html character codes that point to the unicode character they represent
cdef dict e_HTML_CHARS = get_html_char_codes()


cpdef str replace_html_characters(str s_text):
    cdef list a_text = list(s_text)
    cdef str s_match, s_char
    for re_match in reversed(list(re.finditer(r"(&#?[A-Z0-9a-z]{2,8};)", s_text))):
        s_match = s_text[re_match.start():re_match.end()]
        s_char = e_HTML_CHARS.get(s_match, '')
        a_text[re_match.start():re_match.end()] = [s_char]
    return ''.join(a_text)
    
    
def sort_tokens_by_length(a_matches, b_descending=True, index=0):
    ''' sort a list-tuple of tokens/regex matches/lists-tuples or re matches by their lengths '''
    if not a_matches: return a_matches
    if isinstance(a_matches, set): a_matches = tuple(a_matches)
    
    if isinstance(a_matches[0], SRE_MATCH_TYPE):
        # if list contains re match objects
        texts,lengths = zip(
            *sorted([(match, match.end() - match.start()) for match in a_matches],
                    key=itemgetter(1),
                    reverse=b_descending)
        )
    elif isinstance(a_matches[0], (list, tuple)):
        # if list contains lists-tuples
        texts,lengths = zip(
            *sorted([(element, len(element[index])) for element in a_matches],
                    key=itemgetter(1),
                    reverse=b_descending)
        )
    else:
        # if list contains strings
        texts,lengths = zip(
            *sorted([(text, len(text)) for text in a_matches],
                    key=itemgetter(1),
                    reverse=b_descending)
        )
        
    if isinstance(a_matches, list): texts = list(texts)
    elif isinstance(a_matches, tuple): texts = tuple(texts)
    return texts


cpdef bint pos_contained_in_spans(t_pos, a_spans):
    ''' check each span in the list to see
        if the pos span is completely contained inside of it '''
    if not t_pos or not a_spans: return False
    for t_span in a_spans:
        if t_pos[0] >= t_span[0] and t_pos[1] <= t_span[1]:
            return True
    return False


cpdef unique_subset_strings(a_strings, a_tokenized_text, b_case_insensitive=True):
    ''' filter short regex results that only appear inside of found larger results.
        This is to be used when the list of results comes from a combination of
        separate regexes.
        For example,
            if a product title was
                "Web exclusive brandnew red doll"
            and the found regex phrases were
                ['Web exclusive', 'exclusive']
            then exclusive should only be included as part of "web exclusive"
            
            but if a product title was
                "Web exclusive brandnew red exclusive doll"
            then exclusive should also be listed be included as part of "web exclusive"
        
        This is done on a tokenized word basis with ngrams
    '''
    if not a_tokenized_text: return a_strings
    
    # get the ngrams of the words in the text to be turned into frequencies
    a_ngrams = all_ngrams_flat(
        [word.lower() for word in a_tokenized_text]
    )
    cdef dict e_ngrams = {}
    cdef str s_ngram
    for s_ngram in a_ngrams:
        if s_ngram in e_ngrams:
            e_ngrams[s_ngram] += 1
        else:
            e_ngrams[s_ngram] = 1
            
    # sort the results in descending order by length
    a_strings = sort_tokens_by_length(a_strings, b_descending=True)
    
    # filter down to the ones that are not completely contained within another
    a_filter = []
    for s_result in a_strings:
        if not s_result: continue
        a_result_ngrams = all_ngrams_flat(
            word_tokenize(
                s_result, remove_punct=False, stop_words=None, return_lower=True
            )
        )
        if e_ngrams.get(a_result_ngrams[0], 0) == 0: continue
        a_filter.append(s_result)
            
        for ngram in a_result_ngrams:
            if not ngram in e_ngrams: continue
            i_remaining_freq = e_ngrams.get(ngram,0)
            e_ngrams[ngram] -= 1
            
    return a_filter


cpdef list unique_regex_results(a_regexes, str s_text):
    ''' filter short regex results that only appear inside of found larger results.
        This is to be used when the list of results comes from a combination of
        separate regexes.
        For example,
            if a product title was
                "Web exclusive brandnew red doll"
            and the found regex phrases were
                ['Web exclusive', 'exclusive']
            then exclusive should only be included as part of "web exclusive"
            
            but if a product title was
                "Web exclusive brandnew red exclusive doll"
            then exclusive should also be listed be included as part of "web exclusive"
        
        This is done on a string basis
    '''
    # Get the matches for each regex
    cdef list a_matches = []
    for o_re in a_regexes:
        for o_match in o_re.finditer(s_text):
            a_matches.append(o_match)
    
    #sort matches in descending order by length
    a_matches = sort_tokens_by_length(a_matches)
    
    # filter down to the ones that are not completely contained within another
    cdef list a_spans = []
    for o_match in a_matches:
        if pos_contained_in_spans(o_match.span(), a_spans): continue
        a_spans.append(o_match.span())
    
    # turn spans into strings
    cdef tuple t_span
    as_unique = [s_text[t_span[0]:t_span[1]] for t_span in a_spans]
    return as_unique


cpdef list all_ngrams(text, int i_max_size=6, int i_min_size=1,
                      stop_words="english", remove_punct=True):
    ''' Returns a list of lists of ngrams from longest to shortest with sub-lists from first to last.'''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [' '.join(x) for x in z_ngrams]
        a_all_ngrams.append(a_ngrams)
    return a_all_ngrams


cpdef list all_ngrams_lists(text, int i_max_size=6, int i_min_size=1,
                            stop_words="english", remove_punct=True):
    ''' Returns a list of lists of ngrams from longest to shortest with
        sub-lists from first to last with each ngram being a list containing words. '''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [x for x in z_ngrams]
        a_all_ngrams.append(a_ngrams)
    return a_all_ngrams


cpdef list all_ngrams_flat(text, int i_max_size=6, int i_min_size=1, stop_words="english", remove_punct=True):
    ''' Returns a list of ngrams strings from longest to shortest from first to last for each size.'''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [' '.join(x) for x in z_ngrams]
        a_all_ngrams.extend(a_ngrams)
    return a_all_ngrams


cpdef list all_ngrams_tuples(text, int i_max_size=6, int i_min_size=1, stop_words="english", remove_punct=True):
    ''' Returns a list of ngrams tuples from longest to shortest from first to last for each size.'''
    cdef list a_words, a_all_ngrams, a_ngrams
    cdef int i_size, i

    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)

    a_all_ngrams = []
    for i_size in reversed(range(i_min_size, min(i_max_size + 1, len(a_words) + 1))):
        z_ngrams = zip(*[a_words[i:] for i in range(i_size)])
        a_ngrams = [tuple(x) for x in z_ngrams]
        a_all_ngrams.extend(a_ngrams)
    return a_all_ngrams


cpdef list end_grams(words, int i_max_length=4, return_tuples=None):
    ''' pass in list of words and return the ngrams anchored on end word '''
    cdef list a_words, a_ngrams
    cdef int i_size
    
    if not words: return []
    if isinstance(words, str):
        a_words = words.split()
        if return_tuples is None: return_tuples = False
    elif isinstance(words, (list, tuple)):
        a_words = words
        if return_tuples is None: return_tuples = True
    else:
        return []
    
    a_ngrams = []
    if return_tuples:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(tuple(a_words[-i_size:]))
    else:
        for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
            a_ngrams.append(' '.join(a_words[-i_size:]))
    return a_ngrams
    

cpdef list skip_bigrams(text, int i_max_dist=4, int i_min_dist=1,
                        stop_words="english", bint remove_punct=True):
    ''' get a list of skip bigrams
        i_min_dist=1 includes normal bigrams '''
    cdef list a_words, a_all_bigrams
    cdef int i_start, i_end, i_words
    
    if isinstance(text, (list, tuple)):
        a_words = text
    else:
        a_words = word_tokenize(text, stop_words=stop_words, remove_punct=remove_punct)
    
    i_words = len(a_words)
    #print(a_words, i_start)
    a_all_bigrams = []
    for i_start in range(0, i_words - i_min_dist):
        #print(i_start)
        for i_end in range(i_start + i_min_dist, min(i_start + i_max_dist + 1, i_words)):
            a_all_bigrams.append((a_words[i_start], a_words[i_end]))
            
    return a_all_bigrams
    

# Consider removing this
cpdef appendRelativePositionToTokens(doc, int decimal_places=1, stop_words="english"):
    """ This function concatenates each token with its relative position within the string.
        The decimal_places argument states the number of decimal digits to use
        in the rounding of the relative position.
        It can be passed as preprocessor argument to scikit models. """
    cdef list tokens
    cdef float tokens_length

    if type(doc) != list:
        tokens = word_tokenize(doc, stop_words=stop_words)
    else:
        tokens = doc
    tokens_length = float(len(tokens))
    return " ".join([
        "{}{}".format(
            t,  # Concatenate each token
            # With its relative position within the string
            int(100 * round(float(tokens.index(t)) / tokens_length, decimal_places))
        )
        for t in tokens
    ])


cpdef extract_nltk_stems(doc, stop_words="english"):
    """Turns every token in the given doc, into its stem form."""
    return " ".join([
        nltk_stemmer(token)
        for token in word_tokenize(doc, stop_words=stop_words)
    ])


# Deprecate this function, in favor of its consistently named version
cpdef extractNLTKStems(doc, stop_words="english"):
    return extract_nltk_stems(doc, stop_words=stop_words)


#Cython and Spacy aren't playing along. Look into it.
#def generateSpacyLemmasExtractor():
#    """Loading spacy takes alot of time and memory. This function prevents
#    loading spacy every time this module is imported.
#    There's probably a clenear workaround, turning ExtractSpacyLemmas into a class
#    that contains loads spacy on __init__, and making use of the __call__ magic method."""
#    spacy_singleton = spacy.load("en")
#    def extractSpacyLemmas(cod):
#        """This function turns every token in the document into its lemma. """
#        return " ".join([
#            token.lemma_ for token
#            in spacy_singleton(doc.encode('ASCII', 'ignore').decode('ASCII').lower())
#        ])

#    return extractSpacyLemmas


cpdef bint is_alpha_numeric(str s_string):
    if re_num.search(s_string) and re_alpha.search(s_string):
        return True
    else:
        return False


cpdef bint has_alpha_numeric(str s_string):
    if re_alphanum.search(s_string):
        return True
    else:
        return False


cpdef list remove_stopwords(list tokenized_document, stop_words="english"):
    """
    @description: removes stopwords from a tokenized document.
    @arg tokenized_document: {list} A list of words to clean up.
    @arg stopwords_list: {list or str} A list of stopwords,
    or the string "english" to remove English stopwords.
    @return: {list} A list of words, cleaned from stopwords.
    """
    cdef str word
    cdef set c_stop_words

    if stop_words == "english":
        c_stop_words = c_english_stopw
    elif stop_words is None:
        c_stop_words = set([])
    elif isinstance(stop_words, set):  # could optimize by not doing this reconversion
        #c_stop_words = set([word.lower() for word in stop_words])
        c_stop_words = stop_words
    elif isinstance(stop_words, list):
        c_stop_words = set([word.lower() for word in stop_words])
    else:
        raise Exception("Unrecognized set of stopwords")

    return [
        word for word in tokenized_document
        if not word.lower() in c_stop_words
    ]


cpdef str strip_quotes(str s_text, bint b_unmatched=False):
    ''' Remove end quotes from a string, but not if the string
        is empty or containing a single quote or only two quotes '''
    cdef bint b_start, b_end
    if len(s_text) <= 2: return s_text
    if not b_unmatched:
        if s_text[0] != '"' or s_text[-1] != '"': return s_text
        return s_text[1:-1]
    else:
        b_start = s_text[0] == '"'
        b_end = s_text[-1] == '"'
        if b_start and b_end:
            return s_text[1:-1]
        elif b_start:
            return s_text[1:]
        elif b_end:
            return s_text[:-1]
        else:
            return s_text


cpdef list split_strip(str s_text, str s_delimiter=None, int i_max_split=-1, str s_chars=None):
    ''' Split a string into pieces by a delimiter and strip whitespace from each piece.
        Example "This : Example" split on ":" would yield a list of ["This", "Example"]. '''
    #return list(map(lambda s: s.strip(s_chars), s_text.split(s_delimiter, i_max_split)))
    cdef str s
    return [s.strip(s_chars) for s in s_text.split(s_delimiter, i_max_split)]


cpdef split_words(list a_words, f_split):
    ''' Go through the words and split using the specified funtion
        Expects the results of f_split to be a tuple or list if successful.
        Words that weren't split can be return as string or None
        Operates in place on a_words list
    '''
    cdef int index
    cdef str word
    for index, word in reversed(list(enumerate(a_words))):
        split = f_split(word)
        if not split or isinstance(split, str): continue
        if isinstance(split, tuple):
            a_words[index:index+1] = list(split)
        elif isinstance(split, list):
            a_words[index:index+1] = split

re_camelcase = re.compile(r'([A-Z][a-z]+)([A-Z][a-z]+)+')
re_camelcase_split = re.compile(r'([A-Z][a-z]+)')
cpdef tuple split_camelcase(str s_word):
    ''' Examine a word to see if it is camel case.
        If it is, return a tuple that splits it into its constituent pieces
    '''
    cdef str s_x
    cdef tuple t_split
    if re_camelcase.search(s_word):
        t_split = tuple([s_x for s_x in re_camelcase_split.split(s_word) if s_x])
        return t_split
    else:
        return tuple()


re_uom_split = re.compile(r'^([^A-Z]+?)[-_ ,.]*([A-Z][A-Z_.-]+)$', flags=re.I)
cpdef tuple split_number_uom(str s_word):
    ''' Examine a word to see if it is a combination of number
        and unit of measurement. If it is, then split it.
    '''
    o_re = re_uom_split.search(s_word)
    if not o_re: return tuple()
    s_num, s_uom = o_re.groups()
    if not re_num.search(s_num) or not s_uom.lower() in e_dimension_names:
        return tuple()
    
    return (s_num, s_uom)


def split_edge_punctuation(str s_word):
    ''' Split word that contains leading or trailing punctuation. '''
    cdef int i_lead_len, i_trail_len
    cdef list a_word
    
    o_leading = re.search(r'^[-/\[\](),!%&*:;]+(?=(\w|[\'"]))', s_word)
    o_trailing = re.search(r'(?<=(\w|[\'"]))[-/\[\](),!?.$&*:;]+$', s_word)
    
    if not o_leading and not o_trailing: return s_word
    
    if o_leading:
        i_lead_len = o_leading.end()
        a_word = [s_word[:i_lead_len], s_word[i_lead_len:]]
    else:
        a_word = [s_word]

    if o_trailing:
        i_trail_len = o_trailing.end() - o_trailing.start()
        a_word[-1] = a_word[-1][:-i_trail_len]
        a_word.append(s_word[-i_trail_len:])
    
    return a_word


def split_edge_quotes(str s_word):
    ''' Split leading and trailing quotes off. Leave dimensions. '''
    cdef list a_word
    if len(s_word) <= 1: return s_word
    if len(s_word) == 2 and s_word == '""' or s_word == "''": return s_word
    
    if s_word[0] == '"' and s_word[-1] == '"':
        return ['"', s_word[1:-1], '"']
    
    elif s_word[0] == "'" and s_word[-1] == "'":
        return ["'", s_word[1:-1], "'"]
    
    elif s_word[0] == "'" or s_word[0] == '"':
        return [s_word[0], s_word[1:]]
    
    elif (s_word[-1] == "'" or s_word[-1] == '"') and not s_word[-2] in set(list('0123456789')):
        return [s_word[:-1], s_word[-1]]
    
    else:
        return s_word
        

cpdef str join_oxford(list a_list):
    ''' Join a list with commas between elements
    and the last word separated by "and" '''
    if not a_list:
        return ''
    elif len(a_list) == 1:
        return str(a_list[0])
    else:
        return ''.join([
            ', '.join(a_list[:-1]),
            ', and ',
            str(a_list[-1])
        ])
    

cpdef str join_sentence(list a_list, str s_delimiter=' '):
    ''' Try to intelligently join words taking into account punctuation
        that should not have spaces before/after. '''
    if not a_list: return ''
    
    cdef list a_new = [a_list[0]]
    cdef int i_len = len(a_list)
    cdef int index = 1
    cdef str s_elem, s_prec, s_foll
    
    while index < i_len:
        s_elem, s_prec = a_list[index], a_list[index - 1]
        s_foll = a_list[index + 1] if index < i_len - 1 else ''
        if not s_elem: continue
            
        if s_elem in c_no_sep and re_alphanum.search(s_prec) and re_alphanum.search(s_foll):
            a_new[-1] += s_elem + s_foll
            index += 1
        elif s_elem in c_no_preceding and re_alphanum.search(s_prec):
            a_new[-1] += s_elem
        elif  s_prec in c_no_following and re_alphanum.search(s_elem):
            a_new[-1] += s_elem
        else:
            a_new.append(s_elem)
            
        index += 1
    return s_delimiter.join(a_new)


cpdef str strip_unicode_accents(str s_unicode_text):
    ''' First check if character is in ascii list, then check for unicode category to speed things up. '''
    cdef str s_char
    return ''.join([
        s_char if s_char in c_ascii
        else unicodedata.normalize('NFD', s_char)[0] if category(s_char) != 'Mn' else ''
        for s_char in s_unicode_text
    ])


cpdef str clean_title(str s_text):
    #s_text = strip_unicode_accents(s_text)
    s_text = remove_hyphens(s_text)
    s_text = remove_apostrophes(s_text)
    s_text = remove_periods(s_text)
    s_text = remove_trademarks(s_text)
    s_text = replace_with(s_text)
    #s_text = remove_html_characters(s_text)
    s_text = replace_html_characters(s_text)
    s_text = replace_ampersands(s_text)
    s_text = re_punctuation_with_exceptions.sub(' ', s_text)
    s_text = re_spaces.sub(' ', s_text).strip()
    return s_text


cpdef str clean_description(str s_text):
    #s_text = strip_unicode_accents(s_text)
    s_text = remove_hyphens(s_text)
    s_text = remove_apostrophes(s_text)
    s_text = remove_periods(s_text)
    s_text = remove_trademarks(s_text)
    s_text = replace_with(s_text)
    #s_text = remove_html_characters(s_text)
    s_text = replace_html_characters(s_text)
    s_text = replace_ampersands(s_text)
    s_text = re_punctuation_with_exceptions.sub(' ', s_text)
    s_text = re_spaces.sub(' ', s_text).strip()
    return s_text


cpdef str clean_text(s_text, bint b_accents=True,
                     bint b_lowercase=False,
                     bint b_punctuation=False,
                     bint b_normalize_digits=False,
                     bint b_product_numbers=False,
                     bint b_product_dimensions=False,
                     bint b_trademarks=False,
                     bint b_html_characters=False,
                     bint b_ampersands=False,
                     bint b_periods=False
                     ):
    ''' Clean and simplify text before it is turned into tokens to
        improve matching and reduce memory requirements. '''
    s_clean = str(s_text)
    
    if b_html_characters:
        s_clean = replace_html_characters(s_clean)
        #s_clean = remove_html_characters(s_clean)
        
    if b_accents:
        s_clean = strip_unicode_accents(s_clean)

    s_clean = re_misc.sub(' ', s_clean)

    if b_normalize_digits:
        s_clean = re_digits.sub('0', s_clean)

    if b_product_numbers:
        s_clean = re_mpn.sub(' ', s_clean)

    if b_product_dimensions:
        s_clean, l_dimensions = dimensions_tokenizer.tokenize_dimensions(s_clean)

    if b_punctuation:
        s_clean = re_punct_utf.sub(' ', s_clean)
        
    if b_trademarks:
        s_clean = remove_trademarks(s_clean)
        
    if b_ampersands:
        s_clean = replace_ampersands(s_clean)
        
    if b_periods:
        s_clean = remove_periods(s_clean)

    if b_lowercase:
        s_clean = s_clean.lower()

    s_clean = re_spaces.sub(' ', s_clean).strip()
    return s_clean


cpdef str remove_all_punctuation(str s_text, bint b_keepspaces=True, bint b_dehyphenate_phrases=True):
    ''' Remove all punctuation, includes dashes and apostrophes. Spaces optional. '''
    if b_keepspaces:
        s_text = remove_hyphens(
            replace_ampersands(
                remove_slashes(
                    remove_apostrophes(
                        remove_periods(
                            re_punct_nospace_noperiod.sub('', s_text), s_replace=' ')
                    )
                )
            ),
            b_dehyphenate_phrases=b_dehyphenate_phrases
        )
        return trim(s_text)
    else:
        s_text = remove_hyphens(
            replace_ampersands(
                remove_slashes(
                    remove_apostrophes(
                        remove_periods(
                            re_punct_noperiod.sub('', s_text),
                            s_replace=''
                        )
                    )
                )
            ),
            b_dehyphenate_phrases=b_dehyphenate_phrases
        )
        return re_whitespace.sub('', s_text)


# considermaking group a position argument, because it's required
cpdef clean_group_of_dicts(group=None, bint b_accents=True, bint b_lowercase=False,
                           bint b_punctuation=False, bint b_normalize_digits=False,
                           bint b_product_numbers=False):
    ''' Clean up keys/values of group of dictionaries. '''
    cdef dict e_dict
    if isinstance(group, list):
        return [{clean_text(key, b_accents, b_lowercase, b_punctuation,
                            b_normalize_digits, b_product_numbers):
                 clean_text(val, b_accents, b_lowercase, b_punctuation,
                            b_normalize_digits, b_product_numbers)
                 for key, val in e_dict.items()}
                for e_dict in group]
    elif isinstance(group, tuple):
        return tuple(
            [{clean_text(key, b_accents, b_lowercase, b_punctuation,
                        b_normalize_digits, b_product_numbers):
              clean_text(val, b_accents, b_lowercase, b_punctuation,
                         b_normalize_digits, b_product_numbers)
              for key, val in e_dict.items()}
             for e_dict in group]
    )
    elif isinstance(group, dict):
        return {dict_key: {clean_text(key, b_accents, b_lowercase, b_punctuation,
                                      b_normalize_digits, b_product_numbers):
                           clean_text(val, b_accents, b_lowercase, b_punctuation,
                                      b_normalize_digits, b_product_numbers)
                           for key, val in e_dict.items()}
                for dict_key, e_dict in group.items()}


# Consider making a_list a positional argument, because it's required
cpdef list clean_list(a_list=None, bint b_accents=True, bint b_lowercase=False,
                      bint b_punctuation=False, bint b_normalize_digits=False, int b_product_numbers=False):
    ''' Clean up keys/values of group of dictionaries. '''
    if isinstance(a_list, list):
        return [clean_text(s_text, b_accents, b_lowercase, b_punctuation, b_normalize_digits, b_product_numbers)
                for s_text in a_list]


cpdef bint are_two_substrings_consecutive(str outter_string, list substrings,
                                          bint case_sensitive=False) except -1:
    '''
    @description: checks whether two substrings are consecutive, in a given order,
    within a longer string.
    @arg outter_string: {str} The outter, longer string.
    @arg substrings: {list} An ordered list of two strings.
    @return: {bool} Whether the substrings are consecutive, within outter_string,
    in an ordered fashion.
    '''
    if len(substrings) != 2:
        raise Exception("This function only takes 2 substrings")
    cdef re_consecutive, match

    if case_sensitive:
        re_consecutive = re.compile(
            r"\b{}\b[^a-z0-9]*\b{}\b".format(substrings[0], substrings[1])
        )
    else:
        re_consecutive = re.compile(
            r"\b{}\b[^a-z0-9]*\b{}\b".format(substrings[0], substrings[1]),
            re.I
        )
    match = re_consecutive.search(outter_string)
    if match:
        return True
    else:
        return False


cpdef bint are_consecutive_ordered(str outter_string, list substrings,
                                   bint case_sensitive=False):
    '''
    @description: Validates whether a list of strings, are present in a consecutive
    and ordered fashion, within a longer outter string.
    @arg outter_string: {str}
    @arg substrings: {list} The ordered list of substrings.
    @return: {bool} Whether the substring are present in a consecutive and ordered
    fashion, within the outter string.
    '''
    cdef int ind
    cdef str substring
    for ind, substring in enumerate(substrings[:-1]):
        if not are_two_substrings_consecutive(outter_string,
                                              [substrings[ind], substrings[ind + 1]],
                                              case_sensitive=case_sensitive):
            return False
    return True


cpdef bint are_substrings_consecutive(str s_string, list a_substrings,
                                      bint case_sensitive=False):
    ''' Check if list of strings are consecutive in any order in a larger string
    (not separated by other letters or numbers. '''
    cdef tuple permutation
    for permutation in itertools.permutations(a_substrings):
        if are_consecutive_ordered(s_string, list(permutation),
                                   case_sensitive=case_sensitive):
            return True
    return False
    
    
cpdef int edit_distance(str s_1, str s_2, bint b_case_insensitive=False):
    ''' returns: character levenshtein distance '''
    cdef int i_len1, i_len2, x, y, i_x, i_y
    cdef str s_x, s_y
    
    if s_1 == s_2: return 0
    i_len1, i_len2 = len(s_1), len(s_2)
    if i_len1 == 0: return i_len2
    if i_len2 == 0: return i_len1
    
    np_dist = np.zeros([i_len1 + 1, i_len2 + 1], dtype=int)
    
    if b_case_insensitive:
        s_1, s_2 = s_1.lower(), s_2.lower()
      
    np_dist[:, 0] = np.array(range(i_len1 + 1), dtype=int)
    np_dist[0, :] = np.array(range(i_len2 + 1), dtype=int)
    
    for i_x, s_x in enumerate(s_1):
        x = i_x + 1
        for i_y, s_y in enumerate(s_2):
            y = i_y + 1
            if s_x == s_y:
                np_dist[x, y] = np_dist[x - 1, y - 1]
            else:
                np_dist[x, y] = 1 + min(np_dist[x - 1, y], np_dist[x, y - 1], np_dist[x - 1, y - 1])

    return np_dist[-1, -1]


cpdef dict text_differences(str s_text1, str s_text2):
    ''' Compare two strings. '''
    cdef str s_alphanum1, s_alphanum2, s_alpha1, s_alpha2, s_num1, s_num2, s_punct1, s_punct2
    cdef dict e_diff = {}
        
    if s_text1 == s_text2:
        return {}
    elif s_text1.lower() == s_text2.lower():
        return {'case': edit_distance(s_text1, s_text2)}
    else:
        s_alphanum1, s_alphanum2 = ''.join(re_alphanum.findall(s_text1)), ''.join(re_alphanum.findall(s_text2))
        if s_alphanum1 == s_alphanum2:
            return {'punctuation': edit_distance(s_text1, s_text2)}
        
        s_alpha1, s_alpha2 = ''.join(re_alpha.findall(s_alphanum1)), ''.join(re_alpha.findall(s_alphanum2))
        s_num1, s_num2 = ''.join(re_num.findall(s_alphanum1)), ''.join(re_num.findall(s_alphanum2))
        s_punct1, s_punct2 = ''.join(re_punct.findall(s_text1)), ''.join(re_punct.findall(s_text2))
        
        if s_alpha1.lower() != s_alpha2.lower():
            e_diff['letters'] = edit_distance(s_alpha1, s_alpha2)
        elif s_alpha1 != s_alpha2:
            e_diff['case'] = edit_distance(s_alpha1, s_alpha2)
        
        if s_num1 != s_num2:
            e_diff['numbers'] = edit_distance(s_num1, s_num2)
            
        if s_punct1 != s_punct2:
            e_diff['punctuation'] = edit_distance(s_punct1, s_punct2)
            
        return e_diff


cpdef list text_list_to_ngram_list(list a_text_list, int i_max_size=6,
                                   int i_min_size=1, stop_words="english",
                                   bint remove_punctuation=True,
                                   bint b_case_insensitive=True,
                                   bint b_return_tuples=False):
    ''' Pass in a list of docs (strings such as sentences or attribute values).
        Calculuate the frequency (absolute count of occurences)
        and coverage (count of docs containing at least one occurence)
    '''
    cdef list a_words, a_ngrams
    cdef list a_ngram_list = []
    
    for text in a_text_list:
        if isinstance(text, str):
            if b_case_insensitive: text = text.lower()
            a_words = word_tokenize(text, remove_punctuation, stop_words)
        else:
            a_words = list(text)
        
        if b_return_tuples:
            a_ngrams = all_ngrams_tuples(a_words, i_max_size, i_min_size, stop_words)
        else:
            a_ngrams = all_ngrams_flat(a_words, i_max_size, i_min_size, stop_words)
        a_ngram_list.append(a_ngrams)
    return a_ngram_list
    

cpdef ngram_freq_coverage_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams.
        Calculuate the frequency (absolute count of occurences)
        and coverage (count of docs containing at least one occurence)
        and max occurence in any single doc. '''
    cdef list a_ngrams
    cdef dict e_frequency = {}
    cdef dict e_coverage = {}
    cdef dict e_max = {}
    cdef dict e_ngrams
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for s_ngram in a_ngrams:
            if s_ngram in e_ngrams: e_ngrams[s_ngram] += 1
            else: e_ngrams[s_ngram] = 1
            
        for s_ngram, i_count in e_ngrams.items():
            if s_ngram in e_frequency:
                e_frequency[s_ngram] += i_count
                e_coverage[s_ngram] += 1
                e_max[s_ngram] = max(i_count, e_max[s_ngram])
            else:
                e_frequency[s_ngram] = i_count
                e_coverage[s_ngram] = 1
                e_max[s_ngram] = i_count
                
    return e_frequency, e_coverage, e_max


cpdef dict ngram_freq_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams as strings. Calculuate the frequency (absolute count of occurences) '''
    cdef list a_ngrams
    cdef dict e_frequency = {}
    cdef dict e_ngrams
    cdef str s_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for s_ngram in a_ngrams:
            if s_ngram in e_ngrams: e_ngrams[s_ngram] += 1
            else: e_ngrams[s_ngram] = 1
        for s_ngram, i_count in e_ngrams.items():
            if s_ngram in e_frequency:
                e_frequency[s_ngram] += i_count
            else:
                e_frequency[s_ngram] = i_count
    return e_frequency


cpdef dict tuple_ngram_freq_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams as tuples. Calculuate the frequency (absolute count of occurences) '''
    cdef list a_ngrams
    cdef dict e_frequency = {}
    cdef dict e_ngrams
    cdef tuple t_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for t_ngram in a_ngrams:
            if t_ngram in e_ngrams: e_ngrams[t_ngram] += 1
            else: e_ngrams[t_ngram] = 1
        for t_ngram, i_count in e_ngrams.items():
            if t_ngram in e_frequency:
                e_frequency[t_ngram] += i_count
            else:
                e_frequency[t_ngram] = i_count
    return e_frequency


cpdef dict ngram_coverage_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams.
        Calculuate the coverage (count of docs containing at least one occurence) '''
    cdef list a_ngrams
    cdef dict e_coverage = {}
    cdef dict e_ngrams
    cdef str s_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for s_ngram in a_ngrams:
            if s_ngram in e_ngrams: e_ngrams[s_ngram] += 1
            else: e_ngrams[s_ngram] = 1
        for s_ngram, i_count in e_ngrams.items():
            if s_ngram in e_coverage:
                e_coverage[s_ngram] += 1
            else:
                e_coverage[s_ngram] = 1
    return e_coverage


cpdef dict tuple_ngram_coverage_in_list(list a_ngram_list):
    ''' Pass in a list of lists of ngrams. Calculuate the coverage
    (count of docs containing at least one occurence) '''
    cdef list a_ngrams
    cdef dict e_coverage = {}
    cdef dict e_ngrams
    cdef tuple t_ngram
    cdef int i_count
    
    for a_ngrams in a_ngram_list:
        e_ngrams = {}
        for t_ngram in a_ngrams:
            if t_ngram in e_ngrams: e_ngrams[t_ngram] += 1
            else: e_ngrams[t_ngram] = 1
        for t_ngram, i_count in e_ngrams.items():
            if t_ngram in e_coverage:
                e_coverage[t_ngram] += 1
            else:
                e_coverage[t_ngram] = 1
    return e_coverage
    

cpdef list ngram_table(list a_text_list, int i_max_size=6, int i_min_size=1,
                       stop_words="english", bint remove_punctuation=True,
                       bint b_case_insensitive=True, bint b_use_tuples=True):
    ''' Pass in a list of texts/docs.
        Calculuate the frequency (absolute count of occurences)
        and coverage (count of docs containing at least one occurence)
        and max (most occurences in a single doc).
        Return a list of tuples of index,ngram,freq,coverage,max,length. '''
    cdef list a_ngram_list
    cdef dict e_frequency, e_coverage, e_max, e_frequency_ci, e_coverage_ci, e_max_ci
    cdef tuple t_ngram, t_lower
    cdef str s_ngram, s_word, s_lower
    cdef int i_index, i_freq, i_count, i_coverage, i_max
    cdef list at_ngrams = []
    
    a_ngram_list = text_list_to_ngram_list(a_text_list, i_max_size,
                                           i_min_size, stop_words,
                                           remove_punctuation, False, b_use_tuples)

    e_frequency, e_coverage, e_max = ngram_freq_coverage_in_list(a_ngram_list)
    if b_case_insensitive:
        e_frequency_ci = freq_dict_to_lower(e_frequency)
        e_coverage_ci = freq_dict_to_lower(e_coverage)
        e_max_ci = freq_dict_to_lower(e_max)
        
        if b_use_tuples:
            for i_index, t_ngram in enumerate(e_frequency):
                t_lower = tuple([s_word.lower() for s_word in t_ngram])
                if t_lower in e_frequency_ci:
                    i_freq = e_frequency_ci[t_lower]
                    i_coverage = e_coverage_ci.get(t_lower, 0)
                    i_max = e_max_ci.get(t_lower[0], 0)
                    at_ngrams.append(
                        (i_index, t_ngram, i_freq, i_coverage, i_max, len(t_ngram))
                    )
                    del e_frequency_ci[t_lower]
                    del e_coverage_ci[t_lower]
                    del e_max_ci[t_lower]
        else:
            for i_index, s_ngram in enumerate(e_frequency):
                s_lower = s_ngram.lower()
                if s_lower in e_frequency_ci:
                    i_freq = e_frequency_ci[s_lower]
                    i_coverage = e_coverage_ci.get(s_lower, 0)
                    i_max = e_max_ci.get(s_lower[0], 0)
                    at_ngrams.append(
                        (i_index, s_ngram, i_freq, i_coverage, i_max, len(s_lower))
                    )
                    del e_frequency_ci[s_lower]
                    del e_coverage_ci[s_lower]
                    del e_max_ci[s_lower]
    else:
        for i_index, t_ngram_freq in enumerate(e_frequency.items()):
            i_coverage = e_coverage.get(t_ngram_freq[0], 0)
            i_max = e_max.get(t_ngram_freq[0], 0)
            at_ngrams.append(
                (i_index, t_ngram_freq[0], t_ngram_freq[1], i_coverage, i_max, len(t_ngram_freq[0]))
            )
    
    return at_ngrams


def get_ngram_count(t_ngram, e_ngrams):
    ''' Get the count of the ngram in all it's variations from the ngram freq dict '''
    if not t_ngram: return 0
    i_count = e_ngrams.get(t_ngram, 0)
    t_plural, t_singular = pluralize_tuple(t_ngram), singularize_tuple(t_ngram)
    
    if t_plural != t_ngram:
        i_count += e_ngrams.get(t_plural, 0)
    if t_singular != t_ngram:
        i_count += e_ngrams.get(t_singular, 0)
    
    if len(t_ngram) == 1:
        t_compound = compound_word_in_ngrams(t_ngram[0], e_ngrams)
        if t_compound:
            i_count += e_ngrams.get(t_compound, 0)
            t_plural, t_singular = pluralize_tuple(t_compound), singularize_tuple(t_compound)
            if t_plural != t_compound:
                i_count += e_ngrams.get(t_plural, 0)
            if t_singular != t_compound:
                i_count += e_ngrams.get(t_singular, 0)
            
            s_start, s_end = t_compound
            s_plural_end, s_singular_end = pluralize(s_end), singularize(s_end)
            i_count += e_ngrams.get(('{}-{}'.format(s_start, s_end),), 0)
            i_count += e_ngrams.get(('{}_{}'.format(s_start, s_end),), 0)
            if s_plural_end != s_end:
                i_count += e_ngrams.get(('{}-{}'.format(s_start, s_plural_end),), 0)
                i_count += e_ngrams.get(('{}_{}'.format(s_start, s_plural_end),), 0)
            if s_singular_end != s_end:
                i_count += e_ngrams.get(('{}-{}'.format(s_start, s_singular_end),), 0)
                i_count += e_ngrams.get(('{}_{}'.format(s_start, s_singular_end),), 0)
            
    elif len(t_ngram) > 1:
            s_start, s_end = t_ngram
            s_plural_end, s_singular_end = pluralize(s_end), singularize(s_end)
            i_count += e_ngrams.get((''.join([s_start, s_end]),), 0)
            i_count += e_ngrams.get(('-'.join([s_start, s_end]),), 0)
            i_count += e_ngrams.get(('_'.join([s_start, s_end]),), 0)
            
            if s_plural_end != s_end:
                i_count += e_ngrams.get((''.join([s_start, s_plural_end]),), 0)
                i_count += e_ngrams.get((''.join([s_start, s_singular_end]),), 0)
                i_count += e_ngrams.get(('-'.join([s_start, s_plural_end]),), 0)
                
            if s_singular_end != s_end:
                i_count += e_ngrams.get(('-'.join([s_start, s_singular_end]),), 0)
                i_count += e_ngrams.get(('_'.join([s_start, s_plural_end]),), 0)
                i_count += e_ngrams.get(('_'.join([s_start, s_singular_end]),), 0)
            
    return i_count
    

def combine_plural_singular_stems(e_ngrams):
    ''' Combine the counts of the plural and singular forms of the ngram counts. '''
    e_combined = {}
    at_ngrams = e_ngrams.keys()
    
    for t_ngram in at_ngrams:
        if t_ngram in e_combined: continue
        t_plural, t_singular = pluralize_tuple(t_ngram), singularize_tuple(t_ngram)
        if t_plural in e_combined or t_singular in e_combined: continue
        t_stem = tuple([nltk_stemmer(word) for word in t_ngram])
        if t_stem in e_combined: continue
        
        # print("t_ngram   ", t_ngram, e_ngrams.get(t_ngram, 0))
        i_count = e_ngrams[t_ngram]
        
        if t_plural != t_ngram:
            i_count += e_ngrams.get(t_plural, 0)
            # print("t_plural  ", t_plural, e_ngrams.get(t_plural, 0))
        if t_singular != t_ngram:
            i_count += e_ngrams.get(t_singular, 0)
            # print("t_singular", t_singular, e_ngrams.get(t_singular, 0))
        if t_stem != t_ngram and t_stem != t_plural and t_stem != t_singular:
            i_count += e_ngrams.get(t_stem, 0)
            # print("t_stem    ", t_stem, e_ngrams.get(t_stem, 0))
        
        print("i_count", i_count)
        
        e_combined[t_ngram] = i_count
        e_combined[t_plural] = i_count
        e_combined[t_singular] = i_count
        if t_stem in e_combined: e_combined[t_stem] = i_count
    return e_combined


cpdef word_in_text(str s_text, str s_word, position_function=None,
                   bint b_try_plural=True, bint b_try_singular=True):
    ''' Look for word in text, checking exact, plural, singular, and stem.
        If a custom function is passed in for position_function, it needs to
        return tuple of start and end position of word in s_text when called
        with (s_text, s_word)
        word_instr is exact word match
        find_word_sequence will match between words parts and whole word in text, but not vice versa
        find_char_sequence will fuzzy match in both directions
    '''
    cdef tuple t_pos
    cdef str s_plural, s_singular
    
    if position_function is None:
        f_position = word_instr
    elif position_function == 'exact':
        f_position = word_instr
    elif position_function == 'word':
        f_position = find_word_sequence
    elif position_function == 'character':
        f_position = find_char_sequence
    else:
        f_position = position_function
    
    t_pos = f_position(s_text, s_word)
    if t_pos: return t_pos
    
    if b_try_plural:
        s_plural = pluralize(s_word)
        t_pos = f_position(s_text, s_plural)
        if t_pos: return t_pos
        
    if b_try_singular:
        s_singular = singularize(s_word)
        t_pos = f_position(s_text, s_singular)
        if t_pos: return t_pos


cpdef tuple word_instr(str s_text, str s_search):
    ''' Check for word/phrase in text, requiring word boundaries before and after.
        Example: "cat" is word in "black cat", but not word in "category" '''
    o_match = re.search(r"(^|\b|\W){}(\W|\b|$)".format(re.escape(s_search)), s_text)
    if o_match:
        return o_match.span()
    else:
        return None

re_default_match_chars = re.compile("[A-Z0-9()/+]+", re.I)
re_default_punct_ignore = re.compile("[^ '-]+", re.I)


cpdef tuple find_char_sequence(str s_text, str s_substring, int i_max_insert_len=2,
                               str s_insert_chars=" _:;+/()-", int i_start=0):
    ''' Another way of finding sequences of characters in another string,
        disregarding certain punctuation
        characters that may appear between the letters.
        Return the start and end positions of the first match if it is found. '''
    cdef set c_insert_chars = set(list(s_insert_chars))
    cdef list a_sub_chars = [
        "({})?".format(re.escape(s_char))
        if s_char in c_insert_chars
        else re.escape(s_char)
        for s_char in s_substring
    ]
    cdef str s_sub_regex = str("[%s]{0,%i}" % (s_insert_chars, i_max_insert_len)).join(a_sub_chars)
    o_match = re.search(s_sub_regex, s_text[i_start:], re.I)
    if o_match:
        t_pos = o_match.span()
        return (t_pos[0] + i_start, t_pos[1] + i_start)
    else:
        return None
    

cpdef tuple find_word_sequence(str s_text, str s_substring,
                               bint b_try_plural_singular=False, str s_match_regex="[^ '-/]+",
                               int i_start_pos=0, bint b_word_boundaries=True):
    ''' Look for substring in text, ignoring spaces and other punctuation.
        Can optionally look for plural and singular variants of each word.
        Return a tuple with the start and end positions of the substring within the text. '''
    if s_match_regex == "[A-Z0-9()/+]+":
        re_match = re_default_match_chars
    elif s_match_regex == "[^ '-]+":
        re_match = re_default_punct_ignore
    else:
        re_match = re.compile(s_match_regex, re.I)
    
    # start by dividing sub into the alphanumeric pieces to match on
    cdef list a_alpha = re_match.findall(s_substring.lower())
    if not a_alpha: return None
    
    s_text = s_text.lower()
    cdef int i_pos, i_len, i_total_len
    cdef tuple t_sub_pos
    cdef str s_plural, s_singular
    i_total_len = len(s_text)
    
    while i_start_pos < i_total_len and i_start_pos != -1:
        i_pos = s_text.find(a_alpha[0], i_start_pos)  # find the position of the first element
        
        if i_pos != -1:
            i_len = len(a_alpha[0])
            i_start_pos = i_pos
            
        elif b_try_plural_singular:
            s_plural = pluralize(a_alpha[0])
            if not s_plural: return None
            i_pos = s_text.find(s_plural, i_start_pos)
            if i_pos != -1:
                i_len = len(s_plural)
                i_start_pos = i_pos
            else:
                s_singular = (a_alpha[0])
                if not s_singular: return None
                i_pos = s_text.find(s_singular, i_start_pos)
                if i_pos != -1:
                    i_len = len(s_singular)
                    i_start_pos = i_pos
                else:
                    return None  # if the first element isn't found, then the whole substring can't be found
        else:
            return None  # if the first element isn't found, then the whole substring can't be found
        
        if not b_word_boundaries or i_start_pos == 0 or not re_alphanum.search(s_text[i_start_pos - 1]):
            if len(a_alpha) == 1:
                if not b_word_boundaries or i_start_pos + i_len == len(s_text) or \
                   not re_alphanum.search(s_text[i_start_pos + i_len]):
                    # if first element is found and there's only 1, then return its start and end
                    return (i_start_pos, i_start_pos + i_len)
            
            # search for the rest of the substring pieces in order in the string
            t_sub_pos = _find_other_pieces(s_text, a_alpha, i_start_pos, i_len, re_match, b_try_plural_singular)

            if t_sub_pos:
                if not b_word_boundaries or t_sub_pos[1] == len(s_text) or \
                   not re_alphanum.search(s_text[t_sub_pos[1]]):
                    return t_sub_pos  # if they were all found
        
        i_start_pos += 1  # else, increment the start pos and try again
    return None


cpdef tuple _find_other_pieces(str s_string, list a_pieces, int i_start,
                               int i_first_len, re_match, bint b_try_plural_singular=False):
    ''' Find the remainder of the words in the sequence to search for starting from the end
        of the matched first word. Can optionally try plural and singular variants of each word. '''
    cdef int i_pos, i_end, i_len
    cdef str s_piece, s_between, s_plural, s_singular
    
    i_end = i_start + i_first_len
    for s_piece in a_pieces[1:]:
        i_pos = s_string.find(s_piece, i_end)
        
        if i_pos != -1:
            i_len = len(s_piece)
        elif b_try_plural_singular:
            s_plural = pluralize(s_piece)
            i_pos = s_string.find(s_plural, i_end)
            if i_pos != -1:
                i_len = len(s_plural)
            else:
                s_singular = (s_piece)
                i_pos = s_string.find(s_singular, i_end)
                if i_pos != -1:
                    i_len = len(s_singular)
                else:
                    return None
        else:
            return None
        
        s_between = s_string[i_end:i_pos]
        if re_match.search(s_between): return None
        i_end = i_pos + i_len
    return (i_start, i_end)


at_irregular = [
    ("Oxen", "Ox"), ("Feet", "Foot"), ("Teeth", "Tooth"), ("Children", "Child"), ("People", "Person"),
    ("Geese", "Goose"), ("Mice", "Mouse"), ("Criteria", "Criterion"), ("Dice", "Die"), ("Curricula", "Curriculum"),
    ("Safes", "Safe"), ("Humans", "Human"), ("Echoes", "Echo"), ("Embargoes", "Embargo"), ("Heroes", "Hero"),
    ("Potatoes", "Potato"), ("Tomatoes", "Tomato"), ("Fungi", "Fungus"), ("Torpedoes", "Torpedo"),
    ("Vetoes", "Veto"), ("Does", "Doe"), ("Foes", "Foe"), ("Hoes", "Hoe"), ("Joes", "Joe"),
    ("Noes", "No"), ("Poes", "Poe"), ("Toes", "Toe"), ("Woes", "Woe"), ("Ho's", "Ho"), ("To's", "To"),
    ("Jewelry", "Jewelry"), ("Storage", "Storage"), ("Barracks", "Barracks"), ("Means", "Means"),
    ("Offspring", "Offspring"), ("Species", "Species"), ("Series", "Series"),
    ("Deer", "Deer"), ("Moose", "Moose"), ("Fish", "Fish"), ("Shrimp", "Shrimp"), ("Swine", "Swine"),
    ("Go", "Goes"), ("Are", "Is"), ("Aren't", "Isn't"), ("Were", "Was"), ("Weren't", "Wasn't"), ("These", "This"),
    ("Gasses", "Gas"), ("Tonnes", "Ton"), ("Shoes", "Shoes"), ("Tennis", "Tennis"), ("Knife", "Knives"),
    ("Lives", "Life"), ("Halves", "Half"), ("Calves", "Calf"), ("Loaves", "Loaf"), ("Leaves", "Leaf"),
    ("Wives", "Wife"), ("Shelves", "Shelf"), ("Yourselves", "Yourself"), ("Themselves", "Themself"),
]

cdef dict e_irregular_singular = {s_singular.lower(): s_plural for s_plural, s_singular in at_irregular}
cdef dict e_irregular_plural = {s_plural.lower(): s_singular for s_plural, s_singular in at_irregular}


cpdef str pluralize(str s_word):
    ''' Try to pluralize the word using standard plural forms and a list of irregular plural/singular words. '''
    cdef str s_plural
    cdef str s_suffix
    cdef bint b_allcaps, b_titlecase, b_lowercase
    
    s_word = s_word.strip()
    if not s_word: return None
    if s_word[0].isupper():
        if all([s_char.isupper() for s_char in s_word[1:]]):
            b_allcaps, b_titlecase, b_lowercase = True, False, False
        else:
            b_allcaps, b_titlecase, b_lowercase = False, True, False
    elif s_word.islower():
            b_allcaps, b_titlecase, b_lowercase = False, False, True
    else:
        b_allcaps, b_titlecase, b_lowercase = False, False, False
    
    s_word = s_word.lower()
    cdef int i_len = len(s_word)
    s_plural = None
    
    if s_word in e_irregular_singular:
        s_plural = e_irregular_singular[s_word]
        if b_allcaps:
            return s_plural.upper()
        elif b_titlecase:
            return "".format(s_plural[0].upper(), s_plural[1:].lower())
        elif b_lowercase:
            return s_plural.lower()
        else:
            return s_plural
            
    if s_word.find("'") >= 0: return s_word

    s_suffix = s_word[-3:]
    if s_suffix == "tch":
        s_plural = s_word + "es"
    elif s_suffix == "man":
        s_plural = s_word[:-2] + "en"
    elif s_suffix == "dex":
        s_plural = s_word[:-2] + "ices"
    elif s_suffix == "ies":
        s_plural = s_word
    
    if s_plural:
        if b_allcaps:
            return s_plural.upper()
        elif b_titlecase:
            return s_plural[0].upper() + s_plural[1:]
        elif b_lowercase:
            return s_plural.lower()
        else:
            return s_plural
    
    s_suffix = s_word[-2:]
    #print(s_suffix)
    if s_suffix in ["ed", "bs", "cs", "ds", "es", "gs", "ks", "ls", "ms", "ns", "os", "ps", "rs", "ts", "ws"]:
        return s_word
    elif s_suffix in ["ss", "ch", "sh", "ox", "ex"]:
        s_plural = s_word + "es"
    elif s_suffix in ["er", "oo"]:
        s_plural = s_word + "s"
    elif s_suffix in ["ay", "ey", "oy", "uy"]:
        s_plural = s_word + "s"
    elif s_suffix == "is":
        s_plural = s_word[:-2] + "es"
    elif s_suffix == "fe":
        s_plural = s_word[:-2] + "ves"
    elif s_suffix == "ix":
        if i_len > 3:
            s_plural = s_word[:-1] + "ces"
        else:
            s_plural = s_word + "es"
    elif s_suffix == "us":
        if i_len > 4:
            s_plural = s_word[:-2] + "i"
        #if s_suffix == "um": #needed for latin words, but better to handle with irregular collection
        #    s_plural = s_word[:-2] + "a"
      
    if s_plural:
        if b_allcaps:
            return s_plural.upper()
        elif b_titlecase:
            return s_plural[0].upper() + s_plural[1:]
        elif b_lowercase:
            return s_plural.lower()
        else:
            return s_plural

    s_suffix = s_word[-1]
    #print(s_suffix)
    #if s_suffix == "a" 'if there are more latin words than modern english
    #    s_plural = s_word + "e"
    if s_suffix == "a":
        s_plural = s_word + "s"
    elif s_suffix in ["o", "s"]:
        s_plural = s_word + "es"
    elif s_suffix == "e":
        s_plural = s_word + "s"
    elif s_suffix == "z":
        s_plural = s_word + "zes"
    elif s_suffix == "y":
        #if i_Len > 4:
            s_plural = s_word[:-1] + "ies"
        #else:
        #    s_plural = s_word + "s"
    elif s_suffix == "f":
        s_plural = s_word[:-1] + "ves"
    
    else:
        if not s_plural: s_plural = s_word + "s"
    
    if b_allcaps:
        return s_plural.upper()
    elif b_titlecase:
        return s_plural[0].upper() + s_plural[1:]
    elif b_lowercase:
        return s_plural.lower()
    else:
        return s_plural


# The capitalization of the results is inconsistent with pluralize
cpdef str singularize(s_word):
    ''' Try to singularize the word using standard singular forms and a list of irregular plural/singular words. '''
    cdef str s_singular
    cdef str s_suffix
    cdef bint b_allcaps, b_titlecase
    
    s_word = s_word.strip()
    if not s_word: return None
    if len(s_word) == 1: return s_word
    if s_word.find("'") >= 0: return s_word
    b_allcaps, b_titlecase = False, False
    if s_word[0].isupper():
        if all([s_char.isupper() for s_char in s_word[1:]]):
            b_allcaps = True
        else:
            b_titlecase = True
    s_word = s_word.lower()
    cdef int i_len = len(s_word)
    s_singular = None
    
    if s_word in e_irregular_plural:
        s_singular = e_irregular_plural[s_word]
        if b_allcaps:
            return s_singular.upper()
        elif b_titlecase:
            return "".format(s_singular[0].upper(), s_singular[1:].lower())
        else:
            return s_singular.lower()
    
    if s_word[-5:] in ["arves"]:
        s_singular = s_word[:-3] + "f"
    
    elif s_word[-4:] in ["ches", "shes", "sses", "lles"]:
        s_singular = s_word[:-2]
    
    elif s_word[-3:] == "men":
        s_singular = s_word[:-2] + "an"
    elif s_word[-3:] == "ies" and i_len > 4:
        s_singular = s_word[:-3] + "y"
    elif s_word[-3:] == "xes":
        s_singular = s_word[:-2]
    elif len(s_word) > 4 and s_word[-3:] == "ies":
        s_singular = "{}y".format(s_word[:-3])
    elif s_word[-3:] == "oes":
        if i_len > 5:
            s_singular = s_word[:-2]
        else:
            s_singular = s_word[:-1]
            
    elif s_word[-2:] == "es":
        if s_word[-4:-2] in ["ll", "ss"]:
            s_singular = s_word[:-2]
        elif s_word[-4:-2] in ["nn"]:
            s_singular = s_word[:-3]
        else:
            s_singular = s_word[:-1]
        
    elif s_word[-1] == "s":
        s_singular = s_word[:-1]
    else:
        s_singular = s_word
        
    if b_allcaps:
        return s_singular.upper()
    elif b_titlecase:
        return s_singular[0].upper() + s_singular[1:]
    else:
        return s_singular
    

cpdef tuple pluralize_tuple(tuple t_words, bint b_last_word_only=True):
    cdef str s_word
    if not t_words: return t_words
    if b_last_word_only:
        return tuple(list(t_words[:-1]) + [pluralize(t_words[-1])])
    else:
        return tuple([pluralize(s_word) for s_word in t_words])

cpdef tuple singularize_tuple(tuple t_words, bint b_last_word_only=True):
    cdef str s_word
    if not t_words: return t_words
    if b_last_word_only:
        return tuple(list(t_words[:-1]) + [singularize(t_words[-1])])
    else:
        return tuple([singularize(s_word) for s_word in t_words])


cpdef list get_token_spans(str text, list tokens, int i_max_missing=20, bint b_case_insensitive=False):
    ''' @description: Get the spans for the tokens as characters within the text
                      Allowing for some punctuation that has been removed from the tokens
        @arg text: {str} The original unsplit text
        @arg tokens: {list} The tokenized form of the text
        @arg i_max_missing: {int} The maximum number of characters to possibly skip over in finding next token
        @arg b_case_insensitive: {bint} Optionally ignore case
    '''
    cdef int i_search, i_end, i_start
    cdef list a_spans = []
    cdef str tok
    i_search, i_end = 0, len(text)
    if b_case_insensitive:
        text = text.lower()
        tokens = [tok.lower() for tok in tokens]
    
    for tok in tokens:
        i_len = len(tok)
        #i_start = text.find(tok, i_search, min(i_end, i_search + i_len + i_max_missing))
        i_start = text.find(tok, i_search, i_search + i_len + i_max_missing)
        #print(i_search, i_start, tok, text[i_search: min(i_end, i_search + i_len + i_max_missing)])
        if i_start != -1:
            a_spans.append((i_start, i_start + i_len))
            i_search = i_start + i_len
        else:
            a_spans.append(None)
    return a_spans


cpdef tuple get_phrase_position(v_sent, v_phrase,
                                bint b_from_beginning=True,
                                bint b_case_insensitive=False,
                                bint b_punctuation_insensitive=True):
    ''' Look for a phrase of multiple sequential items in a larger set of items.
    Return a tuple of position. '''
    if not v_sent or not v_phrase: return None
    cdef int i_len_phrase = len(v_phrase)
    cdef int i_len_sent = len(v_sent)
    cdef int i_index, i_phrase_index, i_sent_index, i_adjusted_length
    cdef str s_word
    cdef list a_phrase, a_sent, a_phrase_orig, a_sent_orig
    # if the phrase is larger than the set, it can't exist in the set
    if i_len_phrase > i_len_sent: return None
    
    #if isinstance(v_phrase, (spSpan.Span, spTok.Token, spDoc.Doc) ):
    #    a_phrase = [x.text for x in v_phrase]
    if isinstance(v_phrase, str):
        a_phrase = word_tokenize(v_phrase, stop_words=None, remove_punct=False)
    elif isinstance(v_phrase, tuple):
        a_phrase = list(v_phrase)
    else:
        a_phrase = v_phrase
        
    #if isinstance(v_sent, (spSpan.Span, spTok.Token, spDoc.Doc) ):
    #    a_sent = [x.text for x in v_sent]
    if isinstance(v_sent, str):
        a_sent = word_tokenize(v_sent, stop_words=None, remove_punct=False)
    elif isinstance(v_sent, tuple):
        a_sent = list(v_sent)
    else:
        a_sent = v_sent
        
    if b_case_insensitive:
        a_phrase = [s_word.lower() for s_word in a_phrase]
        a_sent = [s_word.lower() for s_word in a_sent]
        
    a_phrase_orig = a_phrase
    a_sent_orig = a_sent
    if b_punctuation_insensitive:
        a_phrase = [remove_all_punctuation(s_word, b_keepspaces=False) for s_word in a_phrase]
        a_sent = [remove_all_punctuation(s_word, b_keepspaces=False) for s_word in a_sent]
        
    i_len_phrase = len(a_phrase)
    i_len_sent = len(a_sent)
    if i_len_phrase > i_len_sent: return None  # if the phrase is larger than the set, it can't exist in the set
    
    # find the indexes of the first non-blank, and last non-blank pieces of the phrase
    i_first_phrase, i_last_phrase = -1, -1
    for i_index, s_word in enumerate(a_phrase):
        if s_word:
            if i_first_phrase < 0: i_first_phrase = i_index
            i_last_phrase = i_index
    if i_last_phrase == -1:
        # phrase was only punctuation, might as well try matching with it instead of just returning none
        a_phrase, a_sent = a_phrase_orig, a_sent_orig
        i_first_phrase, i_last_phrase = 0, i_len_sent - 1

    i_adjusted_length = (i_last_phrase - i_first_phrase) + 1
    if b_from_beginning:
        r_order = range(i_len_sent - i_adjusted_length + 1)
    else:
        r_order = reversed(range(i_len_sent - i_adjusted_length + 1))
    
    for i_index in r_order:
        # find first occurence of the first word of the phrase in the larger set
        if a_phrase[i_first_phrase] == a_sent[i_index]:
            b_skip_position = False
            i_phrase_index, i_sent_index = i_first_phrase + 1, i_index + 1
            
            if i_phrase_index == i_len_phrase:  # if there are no trailing words to check
                # extend to capture leading/trailing punctuation
                if i_first_phrase != 0 and i_index > 0:
                    if not a_sent[i_index - 1]: i_index -= 1
                if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent:
                    if not a_sent[i_sent_index + 1]: i_sent_index += 1
                        
                return (i_index, i_sent_index)
                
            else:  # make sure rest of phrase matches
                while i_phrase_index <= i_last_phrase and i_sent_index < i_len_sent:
                    if a_phrase[i_phrase_index] == a_sent[i_sent_index]:
                        if i_phrase_index == i_last_phrase:
                            # extend to capture leading/trailing punctuation
                            if i_first_phrase != 0 and i_index > 0:
                                if not a_sent[i_index - 1]: i_index -= 1
                            if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent:
                                if not a_sent[i_sent_index + 1]: i_sent_index += 1
                            return (i_index, i_sent_index + 1)
                        else:
                            i_phrase_index += 1
                            i_sent_index += 1
                    else:
                        while not a_phrase[i_phrase_index]:
                            i_phrase_index += 1  # skip blank phrase pieces
                            if i_phrase_index > i_last_phrase:
                                b_skip_position = True
                                break

                        while not a_sent[i_sent_index]:
                            i_sent_index += 1  # skip blank sentence pieces
                            if i_sent_index >= i_len_sent:
                                b_skip_position = True
                                break

                        if i_phrase_index > i_last_phrase or i_sent_index >= i_len_sent:
                            pass
                        elif a_phrase[i_phrase_index] == a_sent[i_sent_index]:
                            if b_skip_position or i_phrase_index < i_last_phrase:
                                i_phrase_index += 1
                                i_sent_index += 1
                            else:
                                #extend to capture leading/trailing punctuation
                                if i_first_phrase != 0 and i_index > 0:
                                    if not a_sent[i_index - 1]: i_index -= 1
                                if i_last_phrase < i_len_phrase - 1 and i_sent_index < i_len_sent:
                                    if not a_sent[i_sent_index + 1]: i_sent_index += 1
                                return (i_index, i_sent_index)
                        else:
                            i_phrase_index += 1
                            i_sent_index += 1
                
    return None  # "Phrase not found"
    

def remove_doc_substring(a_doc, a_substring=[]):
    ''' remove a tokenized substring from a tokenized doc, for example, remove a brand from a title '''
    if isinstance(a_doc, tuple):
        a_doc = list(a_doc)
        b_return_tuple = True
    else:
        b_return_tuple = False

    t_substring_pos = get_phrase_position(a_doc, a_substring)
    if t_substring_pos:
        for i_pos in reversed(range(t_substring_pos[0], t_substring_pos[1])):
            a_doc.pop(i_pos)
    
    if b_return_tuple:
        return tuple(a_doc)
    else:
        return a_doc
    

cpdef list split_on_edge_punctuation(list a_words):
    ''' Split words in list containing with possible leading/trailing punctuation
        into separate tokens.
        Example: ["test/",",this-", "out"] becomes ["test", "/", ",", "this", "-", "out"]. '''
    cdef list a_new = []
    cdef str s_word, s_trailing
    cdef int i_lead_len, i_trail_len
    
    for s_word in a_words:
        o_leading = re.search("^[-/\[\](),!%&*:;]+(?=\w)", s_word)
        o_trailing = re.search("(?<=\w)[-/\[\](),!$&*:;]+$", s_word)
        if o_leading:
            i_lead_len = o_leading.end()
            a_new.append(s_word[:i_lead_len])
            s_word = s_word[i_lead_len:]
            
        if o_trailing:
            i_trail_len = o_trailing.end() - o_trailing.start()
            s_trailing = s_word[-i_trail_len:]
            s_word = s_word[:-i_trail_len]
            
        a_new.append(s_word)
        if o_trailing: a_new.append(s_trailing)
            
    return a_new
    

cpdef list get_quoted_phrases(text):
    ''' Get the quoted phrases from a text.
        The text may be a flat string,
        or
        tokenized still including quotes
        where quotes may either be individual tokens,
        or be at the beginning or end of word tokens.
        
        Quotes directly after a number will not start a phrase. '''
    cdef list a_phrases = []
    cdef bint b_phrase = False
    cdef str s_word, s_letter
    cdef list a_letters = []
    
    if isinstance(text, (list, tuple)):
        for s_word in text:
            if b_phrase:  # continue or end a phrase
                if s_word == '"' or s_word[0] == '"':
                    b_phrase = False
                elif s_word[-1] == '"':
                    b_phrase = False
                    a_phrases[-1].append(s_word[:-1])
                else:
                    a_phrases[-1].append(s_word)
            else:  # start phrase or skip word
                if s_word == '"':
                    b_phrase = True
                    a_phrases.append([])
                elif s_word[0] == '"' and s_word[-1] == '"':
                    a_phrases.append([s_word[1:-1]])
                elif s_word[0] == '"':
                    b_phrase = True
                    a_phrases.append([s_word[1:]])
                elif s_word[-1] == '"':
                    #don't start a phrase if the quote is directly after a number
                    if len(s_word) < 2 or s_word[-2] not in c_digits:
                        b_phrase = True
                        a_phrases.append([])
                else:
                    pass

        return [x for x in a_phrases if x]
    
    else:
        for index, s_letter in enumerate(text):
            if b_phrase:  # continue or end a phrase
                if s_letter == '"':
                    b_phrase = False
                else:
                    a_phrases[-1].append(s_letter)
            else:  # start phrase or skip letter
                if s_letter == '"' and (index == 0 or text[index - 1] not in c_digits):
                    #don't start a phrase if the quote is directly after a number
                    b_phrase = True
                    a_phrases.append([])
                else:
                    pass

        return [''.join(a_letters).strip() for a_letters in a_phrases if a_letters]
        
cpdef list get_quoted_phrases_and_pos(text):
    ''' Get the quoted phrases from a text.
        The text may be a flat string,
        or
        tokenized still including quotes
        where quotes may either be individual tokens,
        or be at the beginning or end of word tokens.
        
        Quotes directly after a number will not start a phrase.
        
        return a list of tuples of phrases and positions
    '''
    cdef list a_phrases = []
    cdef list a_phrase_pos = []
    cdef bint b_phrase = False
    cdef str s_word, s_letter
    cdef list a_letters = []
    cdef int i_word
    
    if isinstance(text, (list, tuple)):
        for i_word, s_word in enumerate(text):
            if b_phrase:  # continue or end a phrase
                if s_word == '"' or s_word[0] == '"':
                    b_phrase = False
                elif s_word[-1] == '"':
                    b_phrase = False
                    a_phrases[-1].append(s_word[:-1])
                    a_phrase_pos[-1][1] = i_word + 1
                else:
                    a_phrases[-1].append(s_word)
                    a_phrase_pos[-1][1] = i_word + 1
            else:  # start phrase or skip word
                if s_word == '"':
                    b_phrase = True
                    a_phrases.append([])
                    a_phrase_pos.append([i_word + 1, i_word + 1])
                elif s_word[0] == '"' and s_word[-1] == '"':
                    a_phrases.append([s_word[1:-1]])
                    a_phrase_pos.append([i_word, i_word + 1])
                elif s_word[0] == '"':
                    b_phrase = True
                    a_phrases.append([s_word[1:]])
                    a_phrase_pos.append([i_word, i_word + 1])
                elif s_word[-1] == '"':
                    #don't start a phrase if the quote is directly after a number
                    if len(s_word) < 2 or s_word[-2] not in c_digits:
                        b_phrase = True
                        a_phrases.append([])
                        a_phrase_pos.append([i_word + 1, i_word + 1])
                else:
                    pass

        return [(phr, tuple(pos))
                for phr, pos in zip(a_phrases, a_phrase_pos)
                if phr and pos[0] < pos[1]]
    
    else:
        for i_letter, s_letter in enumerate(text):
            if b_phrase:  # continue or end a phrase
                if s_letter == '"':
                    b_phrase = False
                else:
                    a_phrases[-1].append(s_letter)
                    a_phrase_pos[-1][1] = i_letter + 1
            else:  # start phrase or skip letter
                if s_letter == '"' and (i_letter == 0 or text[i_letter - 1] not in c_digits):
                    #don't start a phrase if the quote is directly after a number
                    b_phrase = True
                    a_phrases.append([])
                    a_phrase_pos.append([i_letter + 1, i_letter + 1])
                else:
                    pass
        return [(''.join(a_letters).strip(), (a_pos))
                for a_letters, a_pos in zip(a_phrases, a_phrase_pos)
                if a_letters and a_pos[0] < a_pos[1]]


cpdef set get_quoted_phrase_words(text):
    ''' Get a set of words that appear in quoted phrases in the text. '''
    cdef str s_word
    cdef list a_phrase
    cdef list a_phrases = get_quoted_phrases(text)
    if isinstance(text, str):
        a_phrases = [split_strip(phrase) for phrase in a_phrases if phrase]
        
    return set([s_word for a_phrase in a_phrases for s_word in a_phrase])


def is_compound_word(s_compound, e_dictionary, i_start=2):
    ''' Test to see if a compound word like "corkboard" exists in the set/dictionary
        as individual words. '''
    
    if len(s_compound) < 5: return False
    s_compound = s_compound.lower()
    
    for i_split in range(i_start, len(s_compound) - i_start + 1):
        s_start, s_end = s_compound[:i_split], s_compound[i_split:]
        if (s_start, s_end) in e_dictionary:
            return True
        
        if (s_start in e_dictionary or (s_start,) in e_dictionary) and \
           (s_end in e_dictionary or (s_end,) in e_dictionary):
            return True
        
        s_test = '{}_{}'.format(s_start, s_end)
        if s_test in e_dictionary or (s_test,) in e_dictionary:
            return True
        
        s_test = '{}-{}'.format(s_start, s_end)
        if s_test in e_dictionary or (s_test,) in e_dictionary:
            return True
            
    return False


def compound_word_in_ngrams(s_compound, e_ngrams, i_start=2):
    ''' Test to see if a compound word like "corkboard" exists in the ngram dictionary,
        as bigram in either original, hyphenated or ngram form. '''
    
    if len(s_compound) < 5: return None
    s_compound = s_compound.lower()
    # if s_compound in e_ngrams: return s_compound
    # t_compound = tuple([s_compound])
    # if t_compound in e_ngrams: return t_compound
    
    for i_split in range(i_start, len(s_compound) - i_start + 1):
        s_start, s_end = s_compound[:i_split], s_compound[i_split:]
        # if not s_start in e_ngrams and not tuple([s_start]) in e_ngrams: continue
        # if not s_end in e_ngrams and not tuple([s_end]) in e_ngrams: continue
        if (s_start, s_end) in e_ngrams: return (s_start, s_end)
        
        s_test = '{}_{}'.format(s_start, s_end)
        t_test = tuple([s_test])
        if s_test in e_ngrams: return s_test
        if t_test in e_ngrams: return t_test
        
        s_test = '{}-{}'.format(s_start, s_end)
        t_test = tuple([s_test])
        if s_test in e_ngrams: return s_test
        if t_test in e_ngrams: return t_test
    

def get_compounds(e_test_words, e_ngrams=None):
    ''' the words in e_test_words that appear as compound ngrams in e_ngrams '''
    e_compounds = {}
    if e_ngrams is None: e_ngrams = e_test_words
    for t_ngram, freq in e_test_words.items():
        if len(t_ngram) > 1: continue
        # if t_ngram[0] in ENGLISH: continue
        compound = compound_word_in_ngrams(t_ngram[0], e_ngrams, i_start=3)
        if not compound: continue
        e_compounds[t_ngram] = compound
    return e_compounds
    

cpdef dict freq_dict_to_lower(dict e_freq):
    ''' Convert a frequency dict to lower-case and return it.
    This is here only to be used internally. '''
    cdef dict e_ci_freq = {}
    cdef int i_count
    cdef str s_key
    cdef tuple t_key
    
    for v_key, i_count in e_freq.items():
        if isinstance(v_key, str):
            s_key = v_key.lower()
            if s_key in e_ci_freq:
                e_ci_freq[s_key] += i_count
            else:
                e_ci_freq[s_key] = i_count
        if isinstance(v_key, tuple):
            t_key = tuple([s_key.lower() for s_key in v_key])
            if t_key in e_ci_freq:
                e_ci_freq[t_key] += i_count
            else:
                e_ci_freq[t_key] = i_count
        else:
            if v_key in e_ci_freq:
                e_ci_freq[v_key] += i_count
            else:
                e_ci_freq[v_key] = i_count
                
    return e_ci_freq
