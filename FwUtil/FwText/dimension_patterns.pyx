import re

__all__ = ["DimensionsTokenizer", "e_dimension_names", "e_count_words",
           "dimensions_tokenizer", "dimensionizer"]

# need an additional DimensionsTokenizer method that returns a list of
# tuples of matched dimensions (matched dimension, pattern list, start
# character position, end character position)

#to add eventually: flow per time (cfm, etc)
#                   clothing sizes (for shirts, pants, dresses, skirts, shoes, bras)
#                   bed sizes
#                   dates

a_count_words = [
    'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
    'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
    'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety',
    'hundred', 'thousand', 'million', 'billion', 'trillion'
]
e_count_words = set(a_count_words)

e_dimension_names = set(
    ["inch", "inches", "in.", "in",
     "foot", "feet", "ft.", "ft",
     "yard", "yards", "yd.", "yds.", "yd", "yds",
     "millimeter", "millimeters", "mm.", "mm",
     "centimeter", "centimeters", "cm.", "cm",
     "meter", "meters", "m.", "m",
     "pound", "pounds", "lb.", "lbs.", "lb", "lbs",
     "gram", "grams", "g.", "gs.", "g", "gs", "gm.", "gms.", "gm", "gms",
     "kilogram", "kilograms", "kg.", "kgs.", "kg", "kgs",
     "ounce", "ounces", "oz.", "oz",
     "fluid ounce", "fluid ounces", "fluid oz.", "fluid oz",
     "fl. ounce", "fl. ounces", "fl oz.", "fl oz",
     "fl. ounce", "fl. ounces", "fl ounce", "fl ounces",
     "cup", "cups", "c.", "c",
     "quart", "quarts", "qt.", "qt",
     "gallon", "gallons", "gal.", "gal", "gall.", "gall",
     "liter", "liters", "litre", "litres", "ltr.", "ltr",
     "milliliter", "milliliters", "millilitre", "millilitres", "ml", "ml",
     "cubic cm", "cubic cms", "cubic centimeter", "cubic centimeters", "cc.", "cc", "ccs.", "ccs",
     "mil", "mils", "mil.", "mils.",
     "amp", "amps", "a.", "a",
     "milliamp", "milliamps", "mah.", "mah",
     "volt", "volts", "v.", "v",
     "millivolt", "millivolts", "mv.", "mv",
     "watt", "watts", "w.", "w",
     "milliwatt", "milliwatts", "mw.", "mw",
     "gauge", "gauges", "ga.", "ga",
     "hertz", "hz.", "hz",
     "kilohertz", "khz.", "khz",
     "megahertz", "mhz.", "mhz",
     "gigahertz", "ghz.", "ghz",
     "terrahertz", "thz.", "thz",
     "lumen", "lumens", "lm.", "lm",
     "bytes", "b.", "b",
     "kilobytes", "kb.", "kb",
     "megabytes", "mb.", "mb",
     "gigabytes", "gb.", "gb",
     "terrabytes", "tb.", "tb",
     "pack", "packs", "pk.", "pk", "pck.", "pck", "pks.", "pks", "pcks.", "pcks",
     "pair", "pairs", "pr.", "pr",
     "dozen", "dozens", "dz.", "dz",
     "gross",
     "piece", "pieces", "pc.", "pc", "pcs.", "pcs",
     "year", "years", "yr.", "yr", "yrs.", "yrs",
     "month", "months", "mon.", "mon", "mons.", "mons",
     "day", "days", "dy.", "dy", "dys.", "dys",
     "hour", "hours", "hr.", "hr", "hrs.", "hrs",
     "minute", "minutes", "min.", "min", "mins.", "mins",
     "second", "seconds", "sec.", "sec", "secs.", "secs",
     "karat", "karats", "k.", "k", "ks.", "ks", "kt.", "kt", "kts.", "kts",
     "carat", "carats", "c.", "c", "ct.", "ct", "cw.", "cw", "tw.", "tw", "ctw.", "ctw", "cwt.", "cwt", "cttw.", "cttw", "dw.", "dw", "dtw.", "dtw", "tdw.", "tdw",
     ]
)

cdef class DimensionsTokenizer(object):
    """
    @description: Holds methods to identify dimension patterns in documents, and turn
    those into single tokens. IE:
    27 inches ----> 27_inches  |  1/2 meters ------> 1/2_meters
    """
    cpdef public inches_patterns
    cpdef public feet_patterns
    cpdef public meters_patterns
    cpdef public volume_patterns
    cpdef public thickness_patterns
    cpdef public amps_patterns
    cpdef public voltage_patterns
    cpdef public wattage_patterns
    cpdef public gauge_patterns
    cpdef public hertz_patterns
    cpdef public lumens_patterns
    cpdef public number_of_outlets_patterns
    cpdef public pack_patterns
    cpdef public count_patterns
    cpdef public pieces_patterns
    cpdef public bytes_patterns
    cpdef public weight_patterns
    cpdef public time_patterns
    cpdef public carat_patterns
    cpdef public size_patterns
    cpdef public pattern_groups

    def __init__(self):
        """
        @description: Precompile the known patterns on object initialization.
        """
        self.inches_patterns = self._compile_patterns([
            r"\d+(\.\d+)?(inch(es)?)? ?(-|/|x|~) ?\d+(\.\d+)?(inch(es)?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?inch(es)?\)?",  # 1
            r"\d+(\.\d+)?(\"|in\.?)? ?(-|/|x|~) ?\d+(\.\d+)?(\"|in\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?(\"|\(?in(\.|\)|\b|$))",  # 2
            r"(\d+[ -]+)?\d+/\d+(inch(es)?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+(inch(es)?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?inch(es)?\)?",  # 3
            r"(\d+[ -]+)?\d+/\d+(\"|in\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+(\"|in\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?(\"|\(?in(\.|\)|\b|$))",  # 4
            # I think pattern #3 covers everything that pattern #5 would match
            r"\d+/\d+(inch(es)?)? ?(-|x|~) ?\d+/\d+(inch(es)?)? ?(-|x|~) ?\d+/\d+[ -]?\(?inch(es)?\)?",  # 5
            # I think pattern #4 covers everything that pattern #6 would match
            r"\d+/\d+(\"|in\.?)? ?(-|x|~) ?\d+/\d+(\"|in\.?)? ?(-|x|~) ?\d+/\d+[ -]?(\"|\(?in(\.|\)|\b|$))",  # 6
            # I think pattern #1 covers everything that pattern #7 would match
            r"\d+(inch(es)?)? ?(-|x|~) ?\d+(inch(es)?)? ?(-|x|~) ?\d+[ -]?\(?inch(es)?\)?",  # 7
            # I think pattern #2 covers everything that pattern #8 would match
            r"\d+(\"|in\.?)? ?(-|x|~) ?\d+(\"|in\.?)? ?(-|x|~) ?\d+[ -]?(\"|\(?in(\.|\)|\b|$))",  # 8
            # I think pattern #1 can be tweaked to cover everything pattern #9 tries to match
            r"\d+(\.\d+)?(inch(es)?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?inch(es)?\)?",  # 9
            # I think pattern #2 can be tweaked to cover everything pattern #10 tries to match
            r"\d+(\.\d+)?(\"|in\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?(\"|\(?in(\.|\)|\b|$))",  # 10
            # I think pattern #3 can be tweaked to cover everything pattern #11 tries to match
            r"(\d+[ -]+)?\d+/\d+(inch(es)?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?inch(es)?\)?",  # 11
            # I think pattern #4 can be tweaked to cover everything pattern #12 tries to match
            r"(\d+[ -]+)?\d+/\d+(\"|in\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?(\"|\(?in(\.|\)|\b|$))",  # 12
            # I think pattern #11 covers everything that pattern #13 tries to match
            r"\d+/\d+(inch(es)?)? ?(-|x|~) ?\d+/\d+[ -]?\(?inch(es)?\)?",  # 13
            # I think pattern #12 covers everything that pattern #14 tries to match
            r"\d+/\d+(\"|in\.?)? ?(-|x|~) ?\d+/\d+[ -]?(\"|\(?in(\.|\)|\b|$))",  # 14
            # I think pattern #9 covers everything pattern #15 tries to match
            r"\d+(inch(es)?)? ?(-|x|~) ?\d+[ -]?\(?inch(es)?\)?",  # 15
            # I think pattern #10 covers everything pattern #16 tries to match
            r"\d+(\"|in\.?)? ?(-|x|~) ?\d+[ -]?(\"|\(?in(\.|\)|\b|$))",  # 16
            # I think pattern #1 can be tweaked to cover everything pattern #17 tries to match
            r"\d+\.\d+[ -]?\(?inch(es)?\)?",  # 17
            # I think pattern #2 can be tweaked to cover everything pattern #18 tries to match
            r"\d+\.\d+[ -]?(\"|\(?in([^ a-z0-9-]|\)|$))",  # 18
            # I think pattern #3 can be tweaked to cover everything pattern #19 tries to match
            r"\d+[ -]+\d+/\d+[ -]?\(?inch(es)?\)?",  # 19
            # I think pattern #4 can be tweaked to cover everything pattern #20 tries to match
            r"\d+[ -]+\d+/\d+[ -]?(\"|\(?in([^ a-z0-9-]|\)|$))",  # 20
            # I think pattern #1 can be tweaked to cover everything pattern #21 tries to match
            r"\d+/\d+[ -]?\(?inch(es)?\)?",  # 21
            # I think pattern #2 can be tweaked to cover everything pattern #22 tries to match
            r"\d+/\d+[ -]?(\"|\(?in([^ a-z0-9-]|\)|$))",  # 22
            # I think pattern #1 can be tweaked to cover everything pattern #23 tries to match
            r"\d+[ -]?\(?inch(es)?\)?",  # 23
            # I think pattern #2 can be tweaked to cover everything pattern #24 tries to match
            r"\d+[ -]?(\"|\(?in([^ a-z0-9-]|\)|$))"  # 24
        ])
        self.inches_patterns.__name__ = "inches"
        
        self.feet_patterns = self._compile_patterns([
            r"\d+(\.\d+)?(f(ee|oo)t)? ?(-|/|x|~) ?\d+(\.\d+)?(f(ee|oo)t)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?f(ee|oo)t\)?",  # 25
            # feet pattern 25 could be tweaked to cover everything
            # feet pattern 26 is trying to match
            r"\d+(\.\d+)?('|ft\.?)? ?(-|/|x|~) ?\d+(\.\d+)?('|ft\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?('|\(?ft(\.|\)|\b|$))",  # 26
            r"(\d+[ -]+)?\d+/\d+(f(ee|oo)t)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+(f(ee|oo)t)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?f(ee|oo)t\)?",  # 27
            r"(\d+[ -]+)?\d+/\d+('|ft\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+('|ft\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 28
            # I think pattern 27 covers everything this
            # pattern is trying to match
            r"\d+/\d+(f(ee|oo)t)? ?(-|x|~) ?\d+/\d+(f(ee|oo)t)? ?(-|x|~) ?\d+/\d+[ -]?\(?f(ee|oo)t\)?",  # 29
            # I think pattern 28 covers everything this
            # pattern is trying to match
            r"\d+/\d+('|ft\.?)? ?(-|x|~) ?\d+/\d+('|ft\.?)? ?(-|x|~) ?\d+/\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 30
            # I think pattern 25 could be tweaked to cover
            # to cover everything this pattern tries to match.
            r"\d+(f(ee|oo)t)? ?(-|x|~) ?\d+(f(ee|oo)t)? ?(-|x|~) ?\d+[ -]?\(?f(ee|oo)t\)?",  # 31
            # I think pattern 26 coulod be tweaked to cover
            # everything this pattern is trying to match
            r"\d+('|ft\.?)? ?(-|x|~) ?\d+('|ft\.?)? ?(-|x|~) ?\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 32
            # I think pattern 25 could be tweaked to cover
            # everythin this pattern is trying to match.
            r"\d+(\.\d+)?(f(ee|oo)t)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?f(ee|oo)t\)?",  # 33
            # I think pattern 26 could be tweaked to cover
            # everthing this pattern is trying to match
            r"\d+(\.\d+)?('|ft\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?('|\(?ft(\.|\)|\b|$))",  # 34
            # I think pattern 27 can be tweaked to cover
            # everything this pattern is trying to match.
            r"(\d+[ -]+)?\d+/\d+(f(ee|oo)t)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?f(ee|oo)t\)?",  # 35
            # I think pattern 28 can be tweaked to cover
            # everything this pattern is trying to match.
            r"(\d+[ -]+)?\d+/\d+('|ft\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 36
            # I think pattern 35 covers everything
            # pattern 37 is trying to match.
            r"\d+/\d+(f(ee|oo)t)? ?(-|x|~) ?\d+/\d+[ -]?\(?f(ee|oo)t\)?",  # 37
            # I think pattern 36 covers everything
            # pattern 38 is trying to match.
            r"\d+/\d+('|ft\.?)? ?(-|x|~) ?\d+/\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 38
            # I think pattern 25 could be tweaked to
            # cover everything pattern 39 tries to match.
            r"\d+(f(ee|oo)t)? ?(-|x|~) ?\d+[ -]?\(?f(ee|oo)t\)?",  # 39
            # I think pattern 26 could be tweaked to
            # cover everything pattern 40 tries to match.
            r"\d+('|ft\.?)? ?(-|x|~) ?\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 40
            # I think pattern 25 can be tweaked to cover
            # everything pattern 41 tries to match
            r"\d+\.\d+[ -]?\(?f(ee|oo)t\)?",  # 41
            # I think patter 26 could be tweaked to cover
            # everything pattern 42 is trying to match.
            r"\d+\.\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 42
            # I think pattern 25 could be tweaked to cover
            # everything pattern 43 is trying to match.
            r"\d+[ -]+\d+/\d+[ -]?\(?f(ee|oo)t\)?",  # 43
            # I think pattern 26 could be tweaked to cover
            # everything pattern 44 is trying to match
            r"\d+[ -]+\d+/\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 44
            # I think pattern 25 could be tweaked to cover
            # everything pattern 45 is trying to match.
            r"\d+/\d+[ -]?\(?f(ee|oo)t\)?",  # 45
            # I think pattern 26 could be tweaked to cover
            # everything pattern 46 is trying to match.
            r"\d+/\d+[ -]?('|\(?ft(\.|\)|\b|$))",  # 46
            # I think pattern 25 could be tweaked to cover
            # everything pattern 47 is trying to match.
            r"\d+[ -]?\(?f(ee|oo)t\)?",  # 47
            # I think pattern 26 could be tweaked to cover
            # everything pattern 48 is trying to match.
            r"\d+[ -]?('|\(?ft(\.|\)|\b|$))"  # 48
        ])
        self.feet_patterns.__name__ = "feet"
        
        self.meters_patterns = self._compile_patterns([
            r"\d+(\.\d+)?( ?millimeters?)? ?(-|/|x|~) ?\d+(\.\d+)?( ?millimeters?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?millimeters?\)?",  # 49
            r"\d+(\.\d+)?( ?mm\.?)? ?(-|/|x|~) ?\d+(\.\d+)?( ?mm\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?mm(\.\)?\b|$)",  # 50
            r"(\d+[ -]+)?\d+/\d+( ?millimeters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+( ?millimeters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?millimeters?\)?",  # 51
            r"(\d+[ -]+)?\d+/\d+( ?mm\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+( ?mm\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?mm(\.\)?\b|$)",  # 52
            r"\d+/\d+( ?millimeters?)? ?(-|x|~) ?\d+/\d+( ?millimeters?)? ?(-|x|~) ?\d+/\d+[ -]?\(?millimeters?\)?",  # 52
            r"\d+/\d+( ?mm\.?)? ?(-|x|~) ?\d+/\d+( ?mm\.?)? ?(-|x|~) ?\d+/\d+[ -]?\(?mm(\.\)?\b|$)",  # 53
            r"\d+( ?millimeters?)? ?(-|x|~) ?\d+( ?millimeters?)? ?(-|x|~) ?\d+[ -]?\(?millimeters?\)?",  # 54
            r"\d+( ?mm\.?)? ?(-|x|~) ?\d+( ?mm\.?)? ?(-|x|~) ?\d+[ -]?\(?mm(\.\)?\b|$)",  # 55
            r"(\d+[ -]+)?\d+/\d+( ?millimeters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?millimeters?\)?",  # 56
            r"(\d+[ -]+)?\d+/\d+( ?mm\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?mm(\.\)?\b|$)",  # 57
            r"\d+/\d+( ?millimeters?)? ?(-|x|~) ?\d+/\d+[ -]?\(?millimeters?\)?",  # 58
            r"\d+/\d+( ?mm\.?)? ?(-|x|~) ?\d+/\d+[ -]?\(?mm(\.\)?\b|$)",  # 59
            r"\d+( ?millimeters?)? ?(-|x|~) ?\d+[ -]?\(?millimeters?\)?",  # 60
            r"\d+( ?mm\.?)? ?(-|x|~) ?\d+[ -]?\(?mm(\.\)?\b|$)",  # 61
            r"\d+\.\d+[ -]?\(?millimeters?\)?",  # 62
            r"\d+\.\d+[ -]?\(?mm(\.\)?\b|$)",  # 63
            r"\d+[ -]+\d+/\d+[ -]?\(?millimeters?\)?",  # 64
            r"\d+[ -]+\d+/\d+[ -]?\(?mm(\.\)?\b|$)",  # 65
            r"\d+/\d+[ -]?\(?millimeters?\)?",  # 66
            r"\d+/\d+[ -]?\(?mm(\.\)?\b|$)",  # 67
            r"\d+[ -]?\(?millimeters?\)?",  # 68
            r"\d+[ -]?\(?mm(\.\)?\b|$)",  # 69
            r"\d+(\.\d+)?( ?centimeters?)? ?(-|/|x|~) ?\d+(\.\d+)?( ?centimeters?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?centimeters?\)?",  # 70
            r"\d+(\.\d+)?( ?cm\.?)? ?(-|/|x|~) ?\d+(\.\d+)?( ?cm\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?cm(\.\)?\b|$)",  # 71
            r"(\d+[ -]+)?\d+/\d+( ?centimeters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+( ?centimeters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?centimeters?\)?",  # 72
            r"(\d+[ -]+)?\d+/\d+( ?cm\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+( ?cm\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?cm(\.\)?\b|$)",  # 72
            r"\d+/\d+( ?centimeters?)? ?(-|x|~) ?\d+/\d+( ?centimeters?)? ?(-|x|~) ?\d+/\d+[ -]?\(?centimeters?\)?",  # 73
            r"\d+/\d+( ?cm\.?)? ?(-|x|~) ?\d+/\d+( ?cm\.?)? ?(-|x|~) ?\d+/\d+[ -]?\(?cm(\.\)?\b|$)",  # 74
            r"\d+( ?centimeters?)? ?(-|x|~) ?\d+( ?centimeters?)? ?(-|x|~) ?\d+[ -]?\(?centimeters?\)?",  # 75
            r"\d+( ?cm\.?)? ?(-|x|~) ?\d+( ?cm\.?)? ?(-|x|~) ?\d+[ -]?\(?cm(\.\)?\b|$)",  # 76
            r"\d+(\.\d+)?( ?centimeters?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?centimeters?\)?",  # 77
            r"\d+(\.\d+)?( ?cm\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?cm(\.\)?\b|$)",  # 78
            r"(\d+[ -]+)?\d+/\d+( ?centimeters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?centimeters?\)?",  # 79
            r"(\d+[ -]+)?\d+/\d+( ?cm\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?cm(\.\)?\b|$)",  # 80
            r"\d+/\d+( ?centimeters?)? ?(-|x|~) ?\d+/\d+[ -]?\(?centimeters?\)?",  # 81
            r"\d+/\d+( ?cm\.?)? ?(-|x|~) ?\d+/\d+[ -]?\(?cm(\.\)?\b|$)",  # 82
            r"\d+( ?centimeters?)? ?(-|x|~) ?\d+[ -]?\(?centimeters?\)?",  # 83
            r"\d+( ?cm\.?)? ?(-|x) ?\d+( |-|~)?\(?cm(\.\)?\b|$)",  # 84
            r"\d+\.\d+[ -]?\(?centimeters?\)?",  # 85
            r"\d+\.\d+[ -]?\(?cm(\.?\)?\b|$)",  # 86
            r"\d+[ -]+\d+/\d+[ -]?\(?centimeters?\)?",  # 87
            r"\d+[ -]+\d+/\d+[ -]?\(?cm(\.?\)?\b|$)",  # 88
            r"\d+/\d+[ -]?\(?centimeters?\)?",  # 89
            r"\d+/\d+[ -]?\(?cm(\.?\)?\b|$)",  # 90
            r"\d+[ -]?\(?centimeters?\)?",  # 91
            r"\d+[ -]?\(?cm(\.?\)?\b|$)",  # 92
            r"\d+(\.\d+)?( ?meters?)? ?(-|/|x|~) ?\d+(\.\d+)?( ?meters?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?meters?\)?",  # 93
            r"\d+(\.\d+)?( ?m\.?)? ?(-|/|x|~) ?\d+(\.\d+)?( ?m\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?m(\.\)?\b|$)",  # 94
            r"(\d+[ -]+)?\d+/\d+( ?meters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+( ?meters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?meters?\)?",  # 95
            r"(\d+[ -]+)?\d+/\d+( ?m\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+( ?m\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?m(\.\)?\b|$)",  # 96
            r"\d+/\d+( ?meters?)? ?(-|x|~) ?\d+/\d+( ?meters?)? ?(-|x|~) ?\d+/\d+[ -]?\(?meters?\)?",  # 97
            r"(^|\b\d+/\d+( ?m\.?)? ?(-|x|~) ?)\d+/\d+( ?m\.?)? ?(-|x|~) ?\d+/\d+[ -]?\(?m(\.\)?\b|$)",  # 98
            r"\d+( ?meters?)? ?(-|x|~) ?\d+( ?meters?)? ?(-|x|~) ?\d+[ -]?\(?meters?\)?",  # 99
            r"\d+( ?m\.?)? ?(-|x|~) ?\d+( ?m\.?)? ?(-|x|~) ?\d+[ -]?\(?m(\.\)?\b|$)",  # 100
            r"\d+(\.\d+)?( ?meters?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?meters?\)?",  # 101
            r"\d+(\.\d+)?( ?m\.?)? ?(-|/|x|~) ?\d+(\.\d+)?[ -]?\(?m(\.\)?\b|$)",  # 102
            r"(\d+[ -]+)?\d+/\d+( ?meters?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?meters?\)?",  # 103
            r"(\d+[ -]+)?\d+/\d+( ?m\.?)? ?(-|x|~) ?(\d+[ -]+)?\d+/\d+[ -]?\(?m(\.\)?\b|$)",  # 104
            r"\d+/\d+( ?meters?)? ?(-|x|~) ?\d+/\d+[ -]?\(?meters?\)?",  # 105
            r"\d+/\d+( ?m\.?)? ?(-|x|~) ?\d+/\d+[ -]?\(?m(\.\)?\b|$)",  # 106
            r"\d+( ?meters?)? ?(-|x|~) ?\d+[ -]?\(?meters?\)?",  # 107
            r"\d+( ?m\.?)? ?(-|x|~) ?\d+[ -]?\(?m(\.\)?\b|$)",  # 108
            r"\d+\.\d+[ -]?\(?meters?\)?",  # 109
            r"\d+\.\d+[ -]?\(?m(\.\)?\b|$)",  # 110
            r"\d+[ -]+\d+/\d+[ -]?\(?meters?\)?",  # 111
            r"\d+[ -]+\d+/\d+[ -]?\(?m(\.\)?\b|$)",  # 112
            r"\d+/\d+[ -]?\(?meters?\)?",  # 113
            r"\d+/\d+[ -]?\(?m(\.\)?\b|$)",  # 114
            r"\d+[ -]?\(?meters?\)?",  # 115
            r"\d+[ -]?\(?m(\.\)?\b|$)"  # 116
        ])
        self.meters_patterns.__name__ = "meters"
        
        self.volume_patterns = self._compile_patterns([
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*\b(mls?|l(it(er|re)s?)?)(\.|\b|$)",
            r"\d+(-|/|~)\d+[ -]?\b(mls?|l(it(er|re)s?)?)(\.|\b|$)",
            r"\d+\.\d+[ -]?\b(mls?|l(it(er|re)s?)?)(\.|\b|$)",
            r"\d+[ -]?\b(mls?|l(it(er|re)s?)?)(\.|\b|$)",
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*(gallon|gal)s?(\.|\b|$)",
            r"\d+(-|/|~)\d+[ -]?(gallon|gal)s?(\.|\b|$)",
            r"\d+\.\d+[ -]?(gallon|gal)s?(\.|\b|$)",
            r"\d+[ -]?(gallon|gal)s?(\.|\b|$)",
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*(quart|qt)s?(\.|\b|$)",
            r"\d+(-|/|~)\d+[ -]?(quart|qt)s?(\.|\b|$)",
            r"\d+\.\d+[ -]?(quart|qt)s?(\.|\b|$)",
            r"\d+[ -]?(quart|qt)s?(\.|\b|$)",
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*c(ups?)?(\.|\b|$)",
            r"\d+(-|/|~)\d+[ -]?c(ups?)?(\.|\b|$)",
            r"\d+\.\d+[ -]?c(ups?)?(\.|\b|$)",
            r"\d+[ -]?c(ups?)?(\.|\b|$)",
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*fl\.? *ozs?(\.|\b|$)",
            r"\d+(-|/|~)\d+[ -]?fl\.? *ozs?(\.|\b|$)",
            r"\d+\.\d+[ -]?fl\.? *ozs?(\.|\b|$)",
            r"\d+[ -]?fl\.? *ozs?(\.|\b|$)",
        ])
        self.volume_patterns.__name__ = "volume"
        
        self.thickness_patterns = self._compile_patterns([
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*mils?(\.|\b|$)",
            r"\d+(-|/|~)\d+[ -]?mils?(\.|\b|$)",
            r"\d+\.\d+[ -]?mils?(\.|\b|$)",
            r"\d+[ -]?mils?(\.|\b|$)",
        ])
        self.thickness_patterns.__name__ = "thickness"
        
        self.amps_patterns = self._compile_patterns([
            # Probably requires adding optional spaces
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*(a(\.|$)|amps?(\.|\b|$))",  # 121
            # I think pattern 121 covers everything pattern 122 is trying to match
            r"\d+(-|/|~)\d+[ -]?(a(\.|$)|amps?(\.|\b|$))",  # 122
            # I think pattern 121 can be tweaked to cover everything pattern 123
            # is trying to match
            r"\d+\.\d+[ -]?(a(\.|$)|amps?(\.|\b|$))",  # 123
            # I think pattern 121 can be tweaked to cover everything pattern 124
            # is trying to match
            r"\d+[ -]?(a(\.|$)|amps?(\.|\b|$))",  # 124
            # I think pattern 121 can be tweaked to cover everything pattern 125
            # is trying to match
            r"\d+(\.\d+)?(-|/|~)\d+(\.\d+)?[ -]?mAh(\.|\b|$)",  # 125
            r"(\d+,)*\d+(-|/|~)(\d+,)*\d+[ -]?mAh(\.|\b|$)",  # 126
            # I think pattern 121 can be tweaked to cover everhthing
            # pattern 127 is trying to cover
            r"\d+\.\d+[ -]?mAh(\.|\b|$)",  # 127
            # I think pattern 126 can be tweaked to cover everything
            # pattern 128 is trying to match
            r"(\d+,)*\d+[ -]?mAh(\.|\b|$)",  # 128
        ])
        self.amps_patterns.__name__ = "amps"

        self.voltage_patterns = self._compile_patterns([
            # Needs revisiting. Could use optional spaces. The end is wonky
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*volt(age)?[s\.]?(\.|\b|$)",
            # I think voltage_pattern[0] covers everything voltage_patterns[1]
            # is trying to match
            r"\d+[-/~]\d+[ -]*volt(age)?[s\.]?(\.|\b|$)",
            # I think voltage_pattern[0] covers everthing voltage_patterns[1]
            # is trying to match
            r"\d+\.\d+[ -]*volt(age)?[s\.]?(\.|\b|$)",
            # I think voltage_pattern[0] covers everthing voltage_patterns[1]
            # is trying to match
            r"\d+[ -]*volt(age)?[s\.]?(\.|\b|$)"
        ])
        self.voltage_patterns.__name__ = "voltage"

        self.wattage_patterns = self._compile_patterns([
            # Could use optional spaces. Ending is wonky.
            # Outter parens make no sense
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*(w(\.|$)|watt(age)?s?(\.|\b|$))",  # 129
            # I think pattern #129 covers everything pattern 130 is trying to match
            r"\d+[-/~]\d+[ -]*(w(\.|$)|watt(age)?s?(\.|\b|$))",  # 130
            # I think pattern 129 can be tweaked to cover everything
            # pattern 131 is trying to match
            r"\d+\.\d+[ -]*(w(\.|$)|watt(age)?s?(\.|\b|$))",  # 131
            # I think pattern 129 can be tweaked to cover everything
            # pattern 132 is trying to match
            r"\d+[ -]*(w(\.|$)|watt(age)?s?(\.|\b|$))"  # 132
        ])
        self.wattage_patterns.__name__ = "wattage"

        self.gauge_patterns = self._compile_patterns([
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*awg(\.|\b|$)",  # 133
            # I think pattern 133 covers everything pattern 134
            # is trying to match
            r"\d+[-/~]\d+[ -]*awg(\.|\b|$)",  # 134
            # I think pattern 133 can be tweaked to cover everything
            # pattern 135 is trying to match
            r"\d+\.\d+[ -]*awg(\.|\b|$)",  # 135
            # I think pattern 133 can be tweaked to cover everything
            # pattern 136 is trying to match
            r"\d+[ -]*awg(\.|\b|$)",  # 136
            # I think pattern 133 can be tweaked to cover everything
            # pattern 137 is trying to match
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*gauge(\.|\b|$)",  # 137
            # I think pattern 137 covers everything pattern
            # 138 is trying to match
            r"\d+[-/~]\d+[ -]*gauge(\.|\b|$)",  # 138
            # I think pattern 133 can be tweaked to cover everything
            # pattern 139 is trying to match
            r"\d+\.\d+[ -]*gauge(\.|\b|$)",  # 139
            # I think pattern 133 can be tweaked to cover everything
            # pattern 140 is trying to match
            r"\d+[ -]*gauge(\.|\b|$)"  # 140
        ])
        self.gauge_patterns.__name__ = "gauge"

        self.hertz_patterns = self._compile_patterns([
            r"\d+[ -]*hertz(\.|\b|$)",  # 141
            r"\d+[ -]*[tgmk]?hz(\.|\b|$)"  # 142
        ])
        self.hertz_patterns.__name__ = "hertz"

        self.lumens_patterns = self._compile_patterns([
            r"(\d+,)*\d+(\.\d+)?[ -~]\b(\d+,)*\d+(\.\d+)?[ -]*lumens(\.|\b|$)",  # 143
            r"(\d+,)*\d+(\.\d+)?[ -~]\b(\d+,)*\d+(\.\d+)?[ -]*lms(\.|\b|$)",  # 144
            r"(\d+,)*\d+(\.\d+)?[ -]*lumens?(\.|\b|$)",  # 145
            r"(\d+,)*\d+(\.\d+)?[ -]*lms?(\.|\b|$)"  # 146
        ])
        self.lumens_patterns.__name__ = "lumens"

        self.number_of_outlets_patterns = self._compile_patterns([
            r"\d+[ -]*outlets",  # 147
            r"\d+[ -]*gangs",  # 148
            r"\d+[ -]*ports",  # 149
            r"\d+[ -]*bays"  # 150
        ])
        self.number_of_outlets_patterns.__name__ = "number of outlets"

        self.pack_patterns = self._compile_patterns([
            r"\(?\d+[ -]*pcs\.?/pack\)?",  # 151
            r"\(?\d+[ -]*(pack|pk|piece|pc|can|count|ct)s?\.?\)?",  # 152 & 160 & 161
            r"\(?(two|three|four|six|eight|twelve)[ -]*packs?\)?",  # 153 & 154
            r"\(?\d+[ -]*pieces?( |/)bag\)?",  # 156
            r"\(?\d+[ -]*pcs?\.?( |/)bag\)?",  # 157
            r"\(?\d+[ -]*pieces?( |/)bag\)?",  # 158
            r"\(?\d+[ -]*pcs?\.?( |/)bag\)?",  # 159
            r"(pack|set)s?[ -]of[ -]\d+",
            r"(pack|set)s?[ -]of[ -]({})\b".format('|'.join(a_count_words)),
        ])
        self.pack_patterns.__name__ = "pack"
        
        self.count_patterns = self._compile_patterns([
            r"((\b|\d+ *|({})[ _-]*)pairs?(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)dozens?(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)gross(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)(count|cnt)(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)(quantity|qty)(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)pills?(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)capsules?(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)tablets?(\.|\b|$))".format('|'.join(a_count_words)),
            r"((\b|\d+ *|({})[ _-]*)sheets?(\.|\b|$))".format('|'.join(a_count_words)),
        ])
        self.count_patterns.__name__ = "count"

        self.bytes_patterns = self._compile_patterns([
            r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*|-)[kmgt]?b(yte)?s?"
        ])
        self.bytes_patterns.__name__ = "bytes"

        self.weight_patterns = self._compile_patterns([
            r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*|-)(lb|oz|pound|ounce)s?(\.|\b|$)",
            # This pattern can probably be merged with the 1st one
            r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*|-)[km]*gs?(\.|\b|$)",
            # This pattern can probably be merged with the 1st one
            r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*|-)(kilo|milli)?grams?(\.|\b|$)"
        ])
        self.weight_patterns.__name__ = "weight"

        self.time_patterns = self._compile_patterns([
            r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*| *- *)(second|minute|hour|day|month|year)s?",
            r"((\d+,)*\d+(\.\d+)?[ -])?(\d+,)*\d+(\.\d+)?(\s*| *- *)(sec|min|hr|dy|mon|yr)s?(\.|\b|$)"
        ])
        self.time_patterns.__name__ = "time"
        
        self.carat_patterns = self._compile_patterns([
            r"\d+[ -]\d+[-/~]\d+[ -]*(karat|k|kt|carat|c|ct|cw|tw|ctw|cwt|cttw|dw|dtw|tdw)s?(\.|\b|$)",
            r"\d+(\.\d+)?[-/~]\d+(\.\d+)?[ -]*(karat|k|kt|carat|c|ct|cw|tw|ctw|cwt|cttw|dw|dtw|tdw)s?(\.|\b|$)",
            r"\d*\.\d+[ -]?(karat|k|kt|carat|c|ct|cw|tw|ctw|cwt|cttw|dw|dtw|tdw)s?(\.|\b|$)",
            r"\d+[ -]?(karat|k|kt|carat|c|ct|cw|tw|ctw|cwt|cttw|dw|dtw|tdw)s?(\.|\b|$)",
        ])
        self.carat_patterns.__name__ = "carat"
        
        self.size_patterns = self._compile_patterns([
            r"(^|\b)(size|width|height|length|depth|diameter|thickness)[ -]?\d+[ -]\d+[-/~]\d+",
            r"(^|\b)(size|width|height|length|depth|diameter|thickness)[ -]?\d+(\.\d+)?[-/~]\d+(\.\d+)?",
            r"(^|\b)(size|width|height|length|depth|diameter|thickness)[ -]?\d*\.\d+",
            r"(^|\b)(size|width|height|length|depth|diameter|thickness)[ -]?\d+",
            r"(^|\b)(ring|band|shoe)[ -]?\d+[ -]\d+[-/~]\d+",
            r"(^|\b)(ring|band|shoe)[ -]?\d+(\.\d+)?[-/~]\d+(\.\d+)?",
            r"(^|\b)(ring|band|shoe)[ -]?\d*\.\d+",
            r"(^|\b)(ring|band|shoe)[ -]?\d+",
        ])
        self.size_patterns.__name__ = "size"
        
        self.pattern_groups = [
            self.pack_patterns, self.count_patterns, self.number_of_outlets_patterns, self.lumens_patterns,
            self.hertz_patterns, self.gauge_patterns, self.wattage_patterns, self.voltage_patterns,
            self.amps_patterns, self.meters_patterns, self.feet_patterns, self.inches_patterns,
            self.bytes_patterns, self.weight_patterns, self.volume_patterns, self.thickness_patterns,
            self.time_patterns, self.carat_patterns, self.size_patterns,
            #self.pieces_patterns,
        ]
        
    cpdef _compile_patterns(self, list patterns_list):
        """
        @description: compiles a list of raw string patterns
        @arg: {list} patterns_list: The list of patterns to compile.
        @return: {list} the compiled list of patterns
        """
        cdef str pattern
        return PatternsList([
            re.compile(pattern, re.IGNORECASE) for pattern in patterns_list
        ])

    cpdef tuple tokenize_dimensions(self, str document):
        """
        @description: given a document, turns into a single token all the
        token groups that match the known product dimensions patterns.
        @arg document: {str} the document to look for dimension patterns in.
        @return: {str} The document with dimension patterns tokenized together.
        """
        cdef list numbers
        cdef list dimensions
        cdef str num
        cdef pattern_group

        dimensions = []
        numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"] + a_count_words
        if any([num in document.lower() for num in numbers]):
            for pattern_group in self.pattern_groups:
                document, matches = self._tokenize_patterns(document, pattern_group)
                dimensions.extend(matches)

        return document, dimensions

    cpdef tuple _tokenize_patterns(self, str document, pattern_group):
        """
        @description: given a document and a list of patterns, searches for
        groups of tokens matching the patterns on the list, and concatenates
        them into a single token by replacing spaces with underscores.
        @arg document: {str} the document to tokenize
        @arg pattern_group: {list} The list of patterns to search for in the document.
        @return: {str} the document original document, with all their tokens matching
        the patterns on the list, concatenated via underscores.
        """
        cdef list matches
        cdef list clean_matches
        matches = []

        for pattern in pattern_group:
            match = pattern.search(document)
            if match:
                _match = match.group()
                clean_match = _match.replace(" ", "_")
                document = document.replace(_match, clean_match)
                matches.append(
                    TokenizationResult(
                        _match, pattern_group.__name__,
                        match.start(), match.end()
                    )
                )

        return document, matches


class PatternsList(list):
    """
    @description: A named list. It's meant to hold a list of patterns, and
    possess a name that identifies the type of patterns held.
    """
    __name__ = None


cdef class TokenizationResult(object):
    """
    @description: Contains information about an indentified dimension string.
    @prop string_match: {str} contains the identified dimension string.
    @prop pattern_type: {str} contains the type of pattern that found the dimension.
    @prop position_start: {int} the start position of the match, in the original document.
    @prop position_end: {int} the end position of the match, in the original document.
    """
    cpdef public str string_match
    cpdef public str pattern_type
    cpdef public int position_start
    cpdef public int position_end

    def __init__(self, string_match, pattern_type, position_start, position_end):
        self.string_match = string_match
        self.pattern_type = pattern_type
        self.position_start = position_start
        self.position_end = position_end

    def __repr__(self):
        return "<ProductDimension: {}>".format(self.string_match)

    def __str__(self):
        return self.string_match

    def __richcmp__(self, other, op_code):  # This is cython's __eq__ apparently
        if op_code == 2:  # equivalent to python's __eq__
            return self.string_match == other.string_match
        else:
            raise NotImplementedError("This comparison method is not implemented yet")
            

dimensions_tokenizer = DimensionsTokenizer()
dimensionizer = dimensions_tokenizer.tokenize_dimensions
