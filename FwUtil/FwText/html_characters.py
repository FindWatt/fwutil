import os,re

re_whitespace = re.compile("[ \t\r\n]+", re.I)

def trim(text):
    ''' Remove leading and trailing whitespace characters,
        and replace multiple consecutive ones inside text with single space. '''
    return re_whitespace.sub(' ', text).strip()
    
def get_html_char_codes():
    ''' #dict of html character codes that point to the unicode character they represent '''
    #print(__file__)
    #from .FwText import __file__ as FwTextFile
    e_html_chars = {}
    with open(os.path.join(os.path.dirname(__file__), "fw_html_char_codes.txt"), 'r', encoding='utf-8') as f:
        a_char_codes = [line.split('\t') for line in f]
        for a_line in a_char_codes[1:]:
            if len(a_line) != 4: continue
            s_char,s_uni,s_dec,s_names = a_line
            s_char = trim(s_char)
            e_html_chars[trim(s_uni.lower())] = s_char
            for s_code in s_dec.split(","):
                e_html_chars[trim(s_code.lower())] = s_char
            for s_code in s_names.split(","):
                e_html_chars[trim(s_code.lower())] = s_char
    return e_html_chars
#e_HTML_CHARS = get_html_char_codes() #dict of html character codes that point to the unicode character they represent