import heapq
from operator import itemgetter
import pandas as pd
import numpy as np
from nltk.stem.snowball import SnowballStemmer
SB = SnowballStemmer("english")

__version__ = "0.0.29"
__all__ = ["flatten", "unique", "nth_frequent", "freq",
           "freq_dict_to_lower", "freq_dict_to_percent",
           "add_freq_dicts", "add_freq_percent_dicts", "sort_freqs",
           "freq_combine_stems", "freq_merge_stems",
           "freq_group_dict_keys", "freq_group_dict_key_values",
           "herfindahl", "herfindahl_freqs_dict",
           "jaccard",]


cpdef list flatten(list a_nested, bint filter_blanks=False):
    """ Flatten a nested list.
        Made this 3 to 5x more efficient by using a single list reference
        to add all the new elements to before returning. """
    cdef list a_flat = []
    for elem in a_nested:
        if isinstance(elem, list) and (filter_blanks is False or elem):
            flatten_sub(elem, a_flat, filter_blanks=filter_blanks)
        elif filter_blanks is False or elem:
            a_flat.append(elem)
    return a_flat


cdef flatten_sub(list a_nested, list a_flat, bint filter_blanks=False):
    ''' sub function to add recursive elements to the created list '''
    for elem in a_nested:
        if isinstance(elem, list) and (filter_blanks is False or elem):
            flatten_sub(elem, a_flat, filter_blanks=filter_blanks)
        elif filter_blanks is False or elem:
            a_flat.append(elem)


cpdef list unique(items):
    cdef list a_unique = []
    cdef set c_unique = set()
    for x in items:
        if x not in c_unique:
            a_unique.append(x)
            c_unique.add(x)
    return a_unique


def nth_frequent(n, e_freq):
    ''' return the nlargest frequency from a frequency dict where the
    dict values are the counts/percentages. '''
    a_freqs = [(freq, val) for val, freq in e_freq.items()]
    t_nth = heapq.nlargest(n, a_freqs)[-1]
    return tuple(reversed(t_nth))


cpdef dict freq(a_group, remove_empty=None, flatten_groups=True):
    """Second version of freq, trying to make it faster.
    Return a dict with list of unique values as keys and their
    counts as values."""
    cdef dict e_freq_dict = {}

    if isinstance(a_group, dict):  # Use the dictionary values only
        a_group = a_group.values()
    elif isinstance(a_group, pd.Series):
        a_group = list(a_group.values)
    elif isinstance(a_group, set):
        a_group = list(a_group.values)
    
    if flatten_groups: a_group = flatten(a_group)  # Use a flattened version of the list
    for val in a_group:
        if isinstance(val, float) and np.isnan(val): val = ''
        if val in e_freq_dict:
            e_freq_dict[val] += 1
        else:
            e_freq_dict[val] = 1
            
    if remove_empty == True:
        e_empty = set(['', '0', 0])
        b_remove_empty = True
    elif isinstance(remove_empty, list):
        e_empty = set(remove_empty)
        b_remove_empty = True
    elif isinstance(remove_empty, set):
        e_empty = remove_empty
        b_remove_empty = True
    elif isinstance(remove_empty, dict):
        e_empty = set(list(remove_empty.values()) + list(remove_empty.keys()))
        b_remove_empty = True
    else:
        b_remove_empty = False
        e_empty = {}
    
    if b_remove_empty:
        for v_empty in e_empty:
            try:
                del e_freq_dict[v_empty]
            except:
                pass
                
    return e_freq_dict


cpdef dict freq_dict_to_lower(dict e_freq):
    ''' Convert a frequency dict to lower-case and return it. '''
    cdef dict e_ci_freq = {}
    cdef int i_count
    cdef str s_key
    cdef tuple t_key
    
    for v_key, i_count in e_freq.items():
        if isinstance(v_key, str):
            s_key = v_key.lower()
            if s_key in e_ci_freq:
                e_ci_freq[s_key] += i_count
            else:
                e_ci_freq[s_key] = i_count
        elif isinstance(v_key, tuple):
            t_key = tuple([s_key.lower() for s_key in v_key])
            if t_key in e_ci_freq:
                e_ci_freq[t_key] += i_count
            else:
                e_ci_freq[t_key] = i_count
        else:
            if v_key in e_ci_freq:
                e_ci_freq[v_key] += i_count
            else:
                e_ci_freq[v_key] = i_count
                
    return e_ci_freq
    

cpdef dict freq_dict_to_percent(dict e_freq, int i_total=0):
    ''' Convert a frequency dictionary containing counts into percents.
        If i_total is not passed it, it will be calculated by summing individual counts. '''
    cdef int i_count
    if not i_total:
        i_total = sum(e_freq.values())
        
    if i_total:
        return {v_key: float(i_count) / i_total for v_key, i_count in e_freq.items()}
    else:
        return e_freq
            

def add_freq_dicts(*args):
    ''' Merge multiple freq dicts containing counts into a single new
    freq dict and return it. '''
    cdef dict e_combined = {}
    cdef dict e_freq
    cdef int i_count
    for e_freq in args:
        for v_key, i_count in e_freq.items():
            e_combined[v_key] = e_combined.get(v_key, 0) + i_count
    return e_combined
    

def add_freq_percent_dicts(*args, b_average=True):
    ''' Merge multiple freq dicts containing percentages into a single
    new freq dict and return it. '''
    cdef dict e_combined = {}
    cdef dict e_freq
    cdef double d_count
    cdef int i_dicts = len(args)
    for e_freq in args:
        for v_key, d_count in e_freq.items():
            e_combined[v_key] = e_combined.get(v_key, 0) + d_count
            
    if b_average:
        for v_key in e_combined:
            e_combined[v_key] /= i_dicts
        
    return e_combined
    

cpdef list sort_freqs(dict e_freqs, bint b_descending=True):
    ''' sort a frequency dictionary and return as sorted list of
    tuples of key,value '''
    return sorted(e_freqs.items(), key=itemgetter(1), reverse=b_descending)
    

cpdef dict freq_combine_stems(dict e_freqs, bint b_average=True):
    ''' Take a frequency dictionary of words and combine the stems together.
        If keys are multi-word, only the last word is stemmed. '''
    cdef dict e_stems = {}
    cdef dict e_stem, e_combined
    cdef list a_freq
    cdef str s_stem, s_lower
    cdef tuple t_stem, t_lower
    
    v_total = 0
    for v_key, v_freq in e_freqs.items():
        v_total += v_freq
        if isinstance(v_key, str):
            s_lower = v_key.lower()
            s_stem = SB.stem(s_lower)
            if s_stem not in e_stems:
                e_stems[s_stem] = {s_lower: v_freq}
            else:
                e_stems[s_stem][s_lower] = v_freq + e_stems[s_stem].get(s_lower, 0)
                
        elif isinstance(v_key, tuple):
            t_lower = tuple([s_stem.lower() for s_stem in v_key])
            t_stem = tuple([SB.stem(s_stem) for s_stem in t_lower])
            if t_stem not in e_stems:
                e_stems[t_stem] = {t_lower: v_freq}
            else:
                e_stems[t_stem][t_lower] = v_freq + e_stems[t_stem].get(t_lower, 0)
        else:
            e_stems[v_key] = {v_freq: v_key}

    e_combined = {}
    for e_stem in e_stems.values():
        v_key = sort_freqs(e_stem)[0][0]
        v_freq = sum(e_stem.values())
        if b_average:
            v_freq /= v_total
        e_combined[v_key] = v_freq
    return e_combined


cpdef tuple freq_merge_stems(dict e_freqs, bint b_average=True):
    ''' Take a frequency dictionary of words and combine the stems together.
        If keys are multi-word, only the last word is stemmed.
        Return both the frequencies and the table mapping inputs to their respective stems
    '''
    cdef dict e_stems = {}
    cdef dict e_map = {}
    cdef dict e_stem, e_combined
    cdef list a_freq
    cdef str s_stem
    cdef tuple t_stem
    
    v_total = 0
    for v_key, v_freq in e_freqs.items():
        v_total += v_freq
        if isinstance(v_key, str):
            if v_key in e_map:
                s_stem = e_map[v_key]
            else:
                s_stem = SB.stem(v_key.lower())
                e_map[v_key] = s_stem
                
            if s_stem not in e_stems:
                e_stems[s_stem] = {v_key: v_freq}
            else:
                e_stems[s_stem][v_key] = v_freq + e_stems[s_stem].get(v_key, 0)
                
        elif isinstance(v_key, tuple):
            if v_key in e_map:
                t_stem = e_map[v_key]
            else:
                t_stem = tuple([SB.stem(s_stem.lower()) for s_stem in v_key])
                e_map[v_key] = t_stem
                
            if t_stem not in e_stems:
                e_stems[t_stem] = {v_key: v_freq}
            else:
                e_stems[t_stem][v_key] = v_freq + e_stems[t_stem].get(v_key, 0)
        else:
            e_stems[v_key] = {v_freq: v_key}

    e_combined = {}
    e_map = {}
    for e_stem in e_stems.values():
        v_most_freq, v_sum, v_max = None, 0, -999999
        for v_key, v_freq in e_stem.items():
            v_sum += v_freq
            if v_freq > v_max:
                v_max = v_freq
                v_most_freq = v_key
                
        for v_key in e_stem.keys():
            e_map[v_key] = v_most_freq
        
        if b_average:
            v_sum /= v_total
        e_combined[v_most_freq] = v_sum
    return e_combined, e_map


cpdef dict freq_group_dict_keys(group, remove_empty=None):
    ''' Take a group (list or dict) of dictionaries and calculate
    frequencies of their keys.
    Return a dict with list of unique keyname as keys and their
    counts as values. '''
    cdef dict e_freq_dict = {}
    cdef dict e_dict

    if isinstance(group, dict):
        a_group = group.values()
    elif isinstance(group, list):
        a_group = group
    else:
        return None
    
    for e_dict in a_group:
        for v_key, v_val in e_dict.items():
            if v_key in e_freq_dict:
                e_freq_dict[v_key] += 1
            else:
                e_freq_dict[v_key] = 1

    if remove_empty == True:
        e_empty = set(['', '0', 0, np.nan])
        b_remove_empty = True
    elif isinstance(remove_empty, list):
        e_empty = set(remove_empty)
        b_remove_empty = True
    elif isinstance(remove_empty, dict):
        e_empty = set(list(remove_empty.values()) + list(remove_empty.keys()))
        b_remove_empty = True
    else:
        b_remove_empty = False
        e_empty = {}
    
    if b_remove_empty:
        for v_empty in e_empty:
            try:
                del e_freq_dict[v_empty]
            except:
                pass
        
    return e_freq_dict
    
    
cpdef dict freq_group_dict_key_values(group, remove_empty=False):
    ''' Take a group (list or dict) of dictionaries and calculate their
    keys' number of unique values.
    Return a dict with list of unique keyname as keys and their unique
    value counts as values. '''
    cdef dict e_value_dict = {}
    cdef dict e_dict

    if isinstance(group, dict):
        a_group = group.values()
    elif isinstance(group, list):
        a_group = group
    else:
        return None
    
    for e_dict in a_group:
        for v_key, v_val in e_dict.items():
            if v_key in e_value_dict:
                if v_val in e_value_dict[v_key]:
                    e_value_dict[v_key][v_val] += 1
                else:
                    e_value_dict[v_key][v_val] = 1
            else:
                e_value_dict[v_key] = {v_val: 1}

    if remove_empty == True:
        e_empty = set(['', '0', 0, np.nan])
        b_remove_empty = True
    elif isinstance(remove_empty, list):
        e_empty = set(remove_empty)
        b_remove_empty = True
    elif isinstance(remove_empty, dict):
        e_empty = set(list(remove_empty.values()) + list(remove_empty.keys()))
        b_remove_empty = True
    else:
        b_remove_empty = False
        e_empty = {}
    
    if b_remove_empty:
        for v_empty in e_empty:
            try:
                del e_value_dict[v_empty]
            except:
                pass
            for v_key in e_value_dict:
                try:
                    del e_value_dict[v_key][v_empty]
                except:
                    pass
            
    return e_value_dict

cpdef double herfindahl(a_group, bint b_ignoreblanks=False):
    ''' Calculate the HHI from a list or dictionary values'''
    #cdef dict e_freq
    cdef int i_total
    cdef double x

    if b_ignoreblanks:
        e_freq = freq(a_group, True)  # Faster freq function
    else:
        e_freq = freq(a_group)

    i_total = sum(e_freq.values())
    if i_total != 0:
        return sum([(x / i_total)**2 for x in e_freq.values()])
    return 0


cpdef double herfindahl_freqs_dict(dict e_freq, bint b_ignoreblanks=False):
    cdef double d_total
    cdef double x

    ''' Calculate the HHI from a frequency dictionary'''
    if b_ignoreblanks == True:
        if '' in e_freq: del e_freq['']
        if '0' in e_freq: del e_freq['0']
        if 'nan' in e_freq: del e_freq['nan']
        if 0 in e_freq: del e_freq[0]
        if np.nan in e_freq: del e_freq[np.nan]
        
    d_total = sum(e_freq.values())
    if d_total != 0:
        return sum([(x / d_total)**2 for x in e_freq.values()])
    return 0
    

cpdef float jaccard(x, y):
    """ returns the jaccard similarity between two lists """
    if isinstance(x, dict) and  isinstance(y, dict):
        return jaccard_freq_similarity(x, y)
    
    if isinstance(x, (list, tuple, dict)):
        x = set(x)
    #elif not isinstance(x, set):
    #    raise TypeError('x input is not a set')
    assert isinstance(x, set)
    if isinstance(y, (list, tuple, dict)):
        y = set(y)
    #elif not isinstance(y, set):
    #    raise TypeError('y is input not a set')
    assert isinstance(y, set)
        
    return jaccard_similarity(x, y)

cdef float jaccard_similarity(set x, set y):
    cdef int i_intersect = len(x.intersection(y))
    cdef float d_union = float(len(x.union(y)))
    if not d_union: return 0.0
    return i_intersect / d_union

cdef float jaccard_freq_similarity(dict x, dict y):
    ''' instead of the intersect and union being the number of unique keys,
        use the sum of the frequencies of the sets. '''
    cdef set c_x = set(x.keys())
    cdef set c_y = set(y.keys())
    
    cdef c_union = c_x.union(c_y)
    cdef float d_union = float(
        sum(max(x.get(freq, 0), y.get(freq, 0)) for freq in c_union)
    )
    if not d_union: return 0.0
    
    cdef c_intersect = c_x.intersection(c_y)
    cdef list at_shared = [(x[freq], y[freq]) for freq in c_intersect]
    cdef float d_intersect = float(
        sum(min(a, b) for a, b in at_shared)
    )
    
    return d_intersect / d_union
