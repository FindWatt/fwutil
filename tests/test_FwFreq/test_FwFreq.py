from __future__ import absolute_import
import numpy as np
import pytest
from FwUtil import FwFreq

class TestFlatten():
    def test_flatten(self):
        result = FwFreq.flatten([
            "level1",
            ["level2", ["level3"], ''],
            ["level2"],
            [],
            '',
        ])
        assert result == ["level1", "level2", "level3", '', "level2",'']
        
    def test_flatten_filter(self):
        result = FwFreq.flatten(
        [
            "level1",
            ["level2", ["level3"], ''],
            ["level2"],
            [],
            '',
        ],
        filter_blanks=True)
        assert result == ["level1", "level2", "level3", "level2"]


class TestNthFrequent():
    freq_dict = {
        "maybe": 3,
        "this": 1,
        "is": 1,
        "sparta":2
    }

    def test_nth_frequent(self):
        result = FwFreq.nth_frequent(1, self.freq_dict)
        assert result == ("maybe", 3)

        result = FwFreq.nth_frequent(2, self.freq_dict)
        assert result == ("sparta", 2)

    def test_position_tie(self):
        # Not sure if this is the intended behavior, only 1 key-value
        # is returned, when 2 key-values tie for the spot
        result = FwFreq.nth_frequent(3, self.freq_dict)
        assert result[0] == "this" or result[0] == "is"


class TestFreq():
    def test_freq(self):
        result = FwFreq.freq(
            ["one", "spartan", "two", "spartans", "three", "spartans"]
        )
        assert result["one"] == 1
        assert result["spartan"] == 1
        assert result["two"] == 1
        assert result["spartans"] == 2
        assert result["three"] == 1

    def test_remove_empty_true(self):
        result = FwFreq.freq(["", "0", 0, "sparta"], remove_empty=True)
        assert len(result.keys()) == 1

    def test_remove_empty_list(self):
        result = FwFreq.freq(
            ["one", "spartan", "two", "spartans", "three", "spartans"],
            remove_empty=["spartan", "spartans"]
        )
        assert len(result.keys()) == 3

    def test_remove_empty_dict(self):
        result = FwFreq.freq(
            ["one", "spartan", "two", "spartans", "three", "spartans"],
            remove_empty={"one":"two", "three":"four"}
        )
        assert len(result.keys()) == 2


class TestFreqDictToLower():
    freq_dict = {
        "This":1,
        "and":1,
        "this":1
    }
    def test_freq_dict_to_lower(self):
        result = FwFreq.freq_dict_to_lower(self.freq_dict)
        assert len(result.keys()) == 2
        assert result["this"] == 2


class TestFreqDictToPercent():
    freq_dict = {
        "maybe": 3,
        "this": 1,
        "is": 1,
        "sparta":2
    }
    def test_freq_dict_to_percent(self):
        result = FwFreq.freq_dict_to_percent(self.freq_dict)
        assert np.isclose(result["maybe"], 3/7)
        assert np.isclose(result["this"], 1/7)
        assert np.isclose(result["is"], 1/7)
        assert np.isclose(result["sparta"], 2/7)

    def test_given_total(self):
        result = FwFreq.freq_dict_to_percent(self.freq_dict, i_total=10)
        assert np.isclose(result["maybe"], 3/10)
        assert np.isclose(result["this"], 1/10)
        assert np.isclose(result["is"], 1/10)
        assert np.isclose(result["sparta"], 2/10)


class TestAddFreqDicts():
    freq_dict_1 = {
        "maybe": 3,
        "this": 1,
        "is": 1
    }
    freq_dict_2 = {
        "this": 1,
        "is": 1,
        "sparta": 2
    }
    def test_add_freq_dicts(self):
        result = FwFreq.add_freq_dicts(self.freq_dict_1, self.freq_dict_2)
        assert len(result.keys()) == 4
        assert result["this"] == 2
        assert result["is"] == 2


class TestAddFreqPercentDicts():
    freq_dict_1 = {
        "maybe": 3/5,
        "this": 1/5,
        "is": 1/5
    }
    freq_dict_2 = {
        "this": 1/4,
        "is": 1/4,
        "sparta": 2/4
    }
    def test_add_freq_percent_dicts(self):
        result = FwFreq.add_freq_percent_dicts(
            self.freq_dict_1, self.freq_dict_2
        )
        assert len(result.keys()) == 4
        # Not sure if this is behaving properly.
        # I'd expect the following tests to pass
        # assert np.isclose(result["this"], 2/9)
        # assert np.isclose(result["is"], 2/9)
        # assert np.isclose(result["maybe"], 3/9)
        # assert np.isclose(result["sparta"], 2/9)


class TestSortFreqs():
    def test_sort_freqs(self):
        result = FwFreq.sort_freqs({"this":2, "is":1, "sparta":3})
        assert result[0] == ("sparta", 3)
        assert result[1] == ("this", 2)
        assert result[2] == ("is", 1)

    def test_ascending(self):
        result = FwFreq.sort_freqs(
            {"this":2, "is":1, "sparta":3},
            b_descending=False
        )
        assert result[0] == ("is", 1)
        assert result[1] == ("this", 2)
        assert result[2] == ("sparta", 3)


class TestFreqCombineStems():
    freq_dict_1 = {
        "this": 1,
        "spartan": 1,
        "these": 2,
        "spartans": 2
    }

    def test_freq_combine_stems(self):
        result = FwFreq.freq_combine_stems(self.freq_dict_1)
        assert np.isclose(result["this"], 1/6)
        assert np.isclose(result["these"], 2/6)
        # Not sure if this is the intended behavior
        assert np.isclose(result["spartans"], 3/6)
        # Instead of this
        # assert np.isclose(result["spartan"], 3/6)

    def test_average(self):
        result = FwFreq.freq_combine_stems(self.freq_dict_1, b_average=False)
        assert result["this"] == 1
        assert result["these"] == 2
        # Not sure if this is the intended behavior
        assert result["spartans"] == 3
        # Instead of this
        # assert result["spartan"] == 3



class TestFreqGroupDictKeys():
    freq_dict_1 = {
        "maybe": 3,
        "this": 1,
        "is": 1,
        "0": 2
    }
    freq_dict_2 = {
        "this": 1,
        "is": 1,
        "sparta": 2
    }
    def test_freq_group_dict_keys(self):
        result = FwFreq.freq_group_dict_keys([self.freq_dict_1, self.freq_dict_2])
        assert len(result.keys()) == 5
        assert result["maybe"] == 1
        assert result["this"] == 2
        assert result["is"] == 2
        assert result["sparta"] == 1
        assert result["0"] == 1

    def test_remove_empty_true(self):
        result = FwFreq.freq_group_dict_keys(
            [self.freq_dict_1, self.freq_dict_2],
            remove_empty=True
        )
        assert len(result.keys()) == 4
        assert "0" not in result

    #Probably remove_empty is not the right kwarg name for this feature
    def test_remove_empty_is_list(self):
        result = FwFreq.freq_group_dict_keys(
            [self.freq_dict_1, self.freq_dict_2],
            remove_empty=["sparta", "0"]
        )
        assert len(result.keys()) == 3
        assert "0" not in result
        assert "sparta" not in result

    def test_remove_empty_is_dict(self):
        result = FwFreq.freq_group_dict_keys(
            [self.freq_dict_1, self.freq_dict_2],
            remove_empty={"sparta":"0"}
        )
        assert len(result.keys()) == 3
        assert "0" not in result
        assert "sparta" not in result



class TestFreqGroupDictKeyValues():
    test_group = [
        {1:"this", 2:"is", 3:"0"},
        {1:"this", 2:"is", 3:"sparta"}
    ]
    def test_default_settings(self):
        result = FwFreq.freq_group_dict_key_values(self.test_group)
        assert result[1] == {"this":2}
        assert result[2] == {"is":2}
        assert result[3]["0"] == 1
        assert result[3]["sparta"] == 1

    def test_remove_empty_is_true(self):
        result = FwFreq.freq_group_dict_key_values(
            self.test_group, remove_empty=True
        )
        assert result[1] == {"this":2}
        assert result[2] == {"is":2}
        assert result[3] == {"sparta":1}

    #remove_empty is probably not the right name for this feature
    def test_remove_empty_is_list(self):
        result = FwFreq.freq_group_dict_key_values(
            self.test_group, remove_empty=["0", "is"]
        )
        assert result[1] == {"this":2}
        assert result[2] == dict()
        assert result[3] == {"sparta":1}

    def test_remove_empty_is_dict(self):
        result = FwFreq.freq_group_dict_key_values(
            self.test_group, remove_empty={"0":"is"}
        )
        assert result[1] == {"this":2}
        assert result[2] == dict()
        assert result[3] == {"sparta":1}



class TestHerfindahl():
    def test_herfindhal(self):
        result = FwFreq.herfindahl([1, 1, 1, 2, 2])
        assert np.isclose(result, 0.52)

    def test_ignoreblanks(self):
        result = FwFreq.herfindahl(["this", "is", "", "Sparta"])
        assert np.isclose(result, 0.25)
        result = FwFreq.herfindahl(
            ["this", "is", "", "Sparta"],
            b_ignoreblanks=True
        )
        assert np.isclose(result, 1/3)



class TestHerfindahlFreqsDict():
    test_dict = {
        "this": 2,
        "is": 2,
        "0": 2,
        "sparta": 2
    }
    def test_default_behavior(self):
        result = FwFreq.herfindahl_freqs_dict(self.test_dict)
        assert np.isclose(result, 1/4)

    def test_ignoreblanks(self):
        result = FwFreq.herfindahl_freqs_dict(
            self.test_dict, b_ignoreblanks=True
        )
        assert np.isclose(result, 1/3)


class TestJaccard():
    e_x = {'these', 'are', 'present', 'values'}
    
    def test_blank(self):
        assert FwFreq.jaccard([], self.e_x) == 0.0
        
    def test_non_set(self):
        #bug in cython raised exceptions for unit tests?
        #assertRaises(TypeError, FwFreq.jaccard(0, self.e_x))
        #pytest.assertTrue('TypeError' in str(FwFreq.jaccard(0, self.e_x)))
        #with pytest.raises(Exception):
        #    FwFreq.jaccard(0, self.e_x)
        assert FwFreq.jaccard(0, self.e_x) == 0.0
        
    def test_no_overlap(self):
        a_y = ['no', 'match']
        assert FwFreq.jaccard(a_y, self.e_x) == 0.0
        
    def test_overlap(self):
        e_y = {'present':1, 'values':2}
        assert FwFreq.jaccard(e_y, self.e_x) == 0.5
        
    def test_full_match(self):
        t_y = ('these', 'are', 'present', 'values')
        assert FwFreq.jaccard(t_y, self.e_x) == 1.0
        
    def test_freq_match(self):
        x, y = {'a': 1, 'b': 2, 'c': 3}, {'a': 1, 'b': 2, 'c': 3}
        assert FwFreq.jaccard(x, y) == 1.0
        
    def test_freq_partial_match(self):
        x, y = {'a': 1, 'b': 2, 'c': 2, 'e':1}, {'a': 1, 'b': 2, 'c': 4, 'd':1}
        assert np.isclose(FwFreq.jaccard(x, y), 0.5555555820465088)
        
    def test_freq_no_match(self):
        x, y = {'a': 1, 'b': 2, 'c': 3}, {'e': 1, 'f': 2, 'g': 3}
        assert FwFreq.jaccard(x, y) == 0.0
        
        