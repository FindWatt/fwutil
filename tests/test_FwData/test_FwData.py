from __future__ import absolute_import
import os
import numpy as np
import pytest
from FwUtil.FwData import encodings

s_xlsx = os.path.join(os.path.dirname(__file__), "df.xlsx")
s_csv_iso_8859_2 = os.path.join(os.path.dirname(__file__), "df-iso-8859-2.csv")
s_csv_utf8 = os.path.join(os.path.dirname(__file__), "df.csv")
s_csv_utf8_sig = os.path.join(os.path.dirname(__file__), "df-utf8.csv")
s_csv_utf8_malformed = os.path.join(os.path.dirname(__file__), "df-utf8-malformed.csv")
s_csv_utf16 = os.path.join(os.path.dirname(__file__), "df-utf16.csv")
s_csv_utf32 = os.path.join(os.path.dirname(__file__), "df-utf32.csv")

class TestEncodingTester():
    def test_iso_8859_2(self): #test standard windows text file
        result = encodings.encoding_tester(s_csv_iso_8859_2)
        assert result == "ISO-8859-2"
        
    def test_utf8(self): #test unsigned utf8
        result = encodings.encoding_tester(s_csv_utf8)
        assert result == "utf-8"
        
    def test_utf8_sig(self): #test signed utf8
        result = encodings.encoding_tester(s_csv_utf8_sig)
        assert result == "utf-8-sig"
        
    def test_utf8_malformed(self): #test windows text file improperly signed as utf8
        result = encodings.encoding_tester(s_csv_utf8_malformed)
        assert result == "ISO-8859-2"
        
    def test_utf16(self): #test signed utf16
        result = encodings.encoding_tester(s_csv_utf16)
        assert result == "utf-16"
        
    def test_utf32(self): #test signed utf32
        result = encodings.encoding_tester(s_csv_utf32)
        assert result == "utf-32"
    

class TestDetectBOM():
    def test_iso_8859_2(self): #test standard windows text file
        result = encodings.detect_bom(s_csv_iso_8859_2)
        assert result is None
        
    def test_utf8_sig(self): #test signed utf8
        result = encodings.detect_bom(s_csv_utf8_sig)
        assert result == "utf-8-sig"
        
    def test_utf16(self): #test signed utf16
        result = encodings.detect_bom(s_csv_utf16)
        assert result == "utf-16"
        
    def test_utf32(self): #test signed utf32
        result = encodings.detect_bom(s_csv_utf32)
        assert result == "utf-32"