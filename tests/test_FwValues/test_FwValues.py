import pytest
import math
import numpy as np
from FwUtil import FwValues

class TestListValues():
    def test_is_empty(self):
        assert FwValues.is_empty(np.nan) == True
        assert FwValues.is_empty("") == True
        assert FwValues.is_empty(" ") == True
        assert FwValues.is_empty("0") == False
        assert FwValues.is_empty(0) == False
        assert FwValues.is_empty(0.0) == False
    
    def test_num_to_string(self):
        assert FwValues.num_to_string(np.nan) == ''
        assert FwValues.num_to_string(0) == '0'
        assert FwValues.num_to_string(0.0) == '0'
        assert FwValues.num_to_string(1) == '1'
        assert FwValues.num_to_string(1.1) == '1.1'
    
    def test_list_values(self):
        a_test = [None, np.nan, 0, 0.0, 1, 1.1, '', ' ', 'a']
        assert FwValues.list_values(a_test) == [0, 0.0, 1, 1.1, 'a']
    
    def test_fraction_to_float(self):
        assert FwValues.fraction_to_float('') == 0.0
        assert FwValues.fraction_to_float('0') == 0
        assert FwValues.fraction_to_float('1') == 1.0
        assert FwValues.fraction_to_float('1/2') == 0.5
        assert FwValues.fraction_to_float('1-2') == 1.0
        assert FwValues.fraction_to_float('-1/2') == -0.5
        assert FwValues.fraction_to_float('1-1/2') == 1.5
        assert FwValues.fraction_to_float('-1-1/2') == -1.5
        assert FwValues.fraction_to_float('1 1/2') == 1.5
        assert FwValues.fraction_to_float('-1 1/2') == -1.5
        assert FwValues.fraction_to_float(1.0) == 1.0
        assert FwValues.fraction_to_float(1) == 1.0
        assert np.isnan(FwValues.fraction_to_float([]))
        
    def test_list_numbers(self):
        a_test = [None, np.nan, 0, 0.0, 1, 1.1, '', ' ', 'a']
        assert FwValues.list_values(a_test) == [0, 0.0, 1, 1.1, 'a']
    
    def test_value_list_sum(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert FwValues.value_list_sum(a_test) == 3.5
        
    def test_value_list_avg(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert FwValues.value_list_avg(a_test) == 0.875
        
    def test_value_list_stddev(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert math.isclose(FwValues.value_list_stddev(a_test), 1.0231690964840563)
        
    def test_value_list_similarity(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert np.isnan(FwValues.value_list_similarity([]))
        assert math.isclose(FwValues.value_list_similarity([0,1,2.5]), 0.11936942814728912)
        assert math.isclose(FwValues.value_list_similarity([1,2.5]), .5714285714285714)
        assert math.isclose(FwValues.value_list_similarity([1,1]), 1.0)
        assert math.isclose(FwValues.value_list_similarity([0,1]), 0.0)
        assert math.isclose(FwValues.value_list_similarity([-1,1]), 0.0)
        
    def test_value_list_variation(self):
        assert np.isnan(FwValues.value_list_variation([]))
        assert math.isclose(FwValues.value_list_variation([0,1,2.5]), 1.0)
        assert math.isclose(FwValues.value_list_variation([1,2.5]), 0.6)
        assert math.isclose(FwValues.value_list_variation([-1,2.5]), 1.4)
        assert math.isclose(FwValues.value_list_variation([-1,-2.5]), 0.6)
        assert math.isclose(FwValues.value_list_variation([-1,0]), 1.0)


class TestGroupContiguousIndexes():
    def test_empty(self):
        result = FwValues.group_contiguous_indexes([])
        assert result == []
        
    def test_normal(self):
        a_test = [1, 2, 3, 6, 7, 9, 11]
        result = FwValues.group_contiguous_indexes(a_test)
        assert result == [[1, 2, 3], [6, 7], [9], [11]]
        
    def test_duplicate(self):
        a_test = [1, 2, 3, 3, 6, 7, 9, 11]
        result = FwValues.group_contiguous_indexes(a_test)
        assert result == [[1, 2, 3, 3], [6, 7], [9], [11]]
        
    def test_unordered(self):
        a_test = [11, 3, 6, 1, 2, 7, 9]
        result = FwValues.group_contiguous_indexes(a_test, b_sort=True)
        assert result == [[1, 2, 3], [6, 7], [9], [11]]


#class TestVectors():
#need to write unit tests for vectors


class TestDelimitedStrings():
    def test_delimited_strings_to_dicts(self):
        ae_test = FwValues.delimited_strings_to_dicts(["attributeA : value1, attributeB : value2"], s_pairs_delimiter=",", s_key_val_delimiter=":")
        assert ae_test == [{'attributeA': 'value1', 'attributeB': 'value2'}]
    

