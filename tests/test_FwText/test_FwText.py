'''
Test cases for FwUtil/FwText/FwText.pyx
'''
from __future__ import absolute_import
import string
import re
import pytest
from FwUtil import FwText


class TestTitleTokenize():
    def test_blank(self):
        result = FwText.title_tokenize("")
        assert result == []
        
    def test_default(self):
        result = FwText.title_tokenize("This is a test Title.")
        assert result == ["This", "is", "a", "test", "Title"]
        
    def test_lower(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       return_lower=True)
        assert result == ["this", "is", "a", "test", "title"]
        
    def test_default_stop_words(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       stop_words='english')
        assert result == ["test", "Title"]
        
    def test_custom_stop_words(self):
        result = FwText.title_tokenize("This is A test Title.",
                                       stop_words=('is', 'a'))
        assert result == ["This", "test", "Title"]
        
    def test_phrases_underscore(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       phrases=['test_title'])
        assert result == ["This", "is", "a", "test_Title"]
        
    def test_phrases_underscore_and_tuple(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       phrases=['test_title'],
                                       return_tuples=True)
        assert result == ["This", "is", "a", ("test", "Title")]
        
    def test_phrases_tuple(self):
        result = FwText.title_tokenize("This is a test Title.",
                                       phrases=[('test', 'title')],
                                       use_underscore=False,
                                       return_tuples=True)
        assert result == ["This", "is", "a", ("test", "Title")]


class TestDescriptionTokenize():
    def test_blank(self):
        result = FwText.description_tokenize("")
        assert result == []
        
    def test_default(self):
        result = FwText.description_tokenize("This is a test Description.")
        assert result == [["This", "is", "a", "test", "Description"]]
        
    def test_lower(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       return_lower=True)
        assert result == [["this", "is", "a", "test", "description"]]
        
    def test_default_stop_words(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       stop_words='english')
        assert result == [["test", "Description"]]
        
    def test_custom_stop_words(self):
        result = FwText.description_tokenize("This is A test Description.",
                                       stop_words=('is', 'a'))
        assert result == [["This", "test", "Description"]]
        
    def test_phrases_underscore(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       phrases=['test_description'])
        assert result == [["This", "is", "a", "test_Description"]]
        
    def test_phrases_underscore_and_tuple(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       phrases=['test_description'],
                                       return_tuples=True)
        assert result == [["This", "is", "a", ("test", "Description")]]
        
    def test_phrases_tuple(self):
        result = FwText.description_tokenize("This is a test Description.",
                                       phrases=[('test', 'description')],
                                       use_underscore=False,
                                       return_tuples=True)
        assert result == [["This", "is", "a", ("test", "Description")]]
        
    def test_sentences_default(self):
        result = FwText.description_tokenize("This is a test Description. It has two sentences.",
                                       return_sentences=True)
        assert result == [["This", "is", "a", "test", "Description"],
                          ["It", "has", "two", "sentences"]]
        
    def test_sentences_joined(self):
        result = FwText.description_tokenize("This is a test Description. It has two sentences.",
                                       return_sentences=False)
        assert result == ["This", "is", "a", "test", "Description",
                          "It", "has", "two", "sentences"]


class TestWordTokenize():
    def test_default_remove_english_stopwords(self):
        result = FwText.word_tokenize("This is Sparta")
        assert result == ["Sparta"]

    def test_do_not_remove_stopwords(self):
        result = FwText.word_tokenize("This is Sparta", stop_words=None)
        assert result == ["This", "is", "Sparta"]

    def test_remove_custom_stopwords(self):
        result = FwText.word_tokenize("This is Sparta", stop_words=["sparta"])
        assert result == ["This", "is"]

    def test_default_remove_punctuation(self):
        result = FwText.word_tokenize("Tom | Jerry!")
        assert result == ["Tom", "Jerry"]

    def test_do_not_remove_punctuation(self):
        result = FwText.word_tokenize("Tom & Jerry!", remove_punct=False)
        assert result == ["Tom", "&", "Jerry!"]

    def test_custom_vocab(self):
        result = FwText.word_tokenize("We're only interested in keeping this word",
            vocabulary=["this", "word"], stop_words=None)
        assert result == ["this", "word"]

    def test_ampersand(self):
        result = FwText.word_tokenize("I like A&W Root Beer & French Fries",
            stop_words=None, convert_ampersand=True)
        assert result == ["I", "like", "A&W", "Root", "Beer", "and", "French", "Fries"]


class TestWordIndexes():
    def test_blank_text(self):
        s_text = ""
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text)
        assert result == []
    
    def test_blank_word_list(self):
        s_text = "This is a a 100% test - sentence w / slashes."
        a_text = []
        result = FwText.word_indexes(a_text, s_text)
        assert result == []
    
    def test_normal(self):
        s_text = "This is a a 100% test - sentence w / slashes."
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text)
        assert result == [(0, 4), (5, 7), (8, 9), (12, 15), (17, 21), (24, 32), (33, 34), None, (37, 44)]
    
    def test_no_spaces(self):
        s_text = "Thisisaa100%test-sentencew/slashes."
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text)
        assert result == [(0, 4), (4, 6), (6, 7), (8, 11), (12, 16), (17, 25), (25, 26), None, (27, 34)]
    
    def test_case(self):
        s_text = "this IS a A 100% tEsT - Sentence W / Slashes."
        a_text = ['This', 'is', 'a', '100', 'test', 'sentence', 'w', 'w', 'slashes']
        result = FwText.word_indexes(a_text, s_text, b_case_insensitive=True)
        assert result == [(0, 4), (5, 7), (8, 9), (12, 15), (17, 21), (24, 32), (33, 34), None, (37, 44)]


class TestTrim():
    def test_blank(self):
        result = FwText.trim("")
        assert result == ""
        
    def test_normal(self):
        result = FwText.trim("\t \r\nThis is a   test. \t ")
        assert result == "This is a test."


class TestRemoveHyphens():
    def test_with_spaces(self):
        result = FwText.remove_hyphens("This is a test - a really easy test")
        assert result == "This is a test   a really easy test"

    def test_equation(self):
        result = FwText.remove_hyphens("3-2=1")
        assert result == "3-2=1"

    def test_negative_number(self):
        result = FwText.remove_hyphens("It is -20 with windchill")
        assert result == "It is -20 with windchill"

    def test_keep_compound_word(self):
        result = FwText.remove_hyphens("Z-lon is a brand but slow-cooking is a phrase", b_dehyphenate_phrases=True)
        assert result == "Z-lon is a brand but slow cooking is a phrase"

    def test_keep_phrase(self):
        result = FwText.remove_hyphens("Z-lon is a brand but slow-cooking is a phrase", b_dehyphenate_phrases=False)
        assert result == "Z-lon is a brand but slow-cooking is a phrase"


class TestRemoveApostrophes():
    def test_time(self):
        result = FwText.remove_apostrophes("It's 11 o'clock")
        assert result == "It's 11 o'clock"

    def test_contraction(self):
        result = FwText.remove_apostrophes("This won't work")
        assert result == "This won't work"

    def test_single_quotes(self):
        result = FwText.remove_apostrophes("It is a 'bad' idea")
        assert result == "It is a bad idea"

    def test_single_quotes(self):
        result = FwText.remove_apostrophes("In 'extreme' cases")
        assert result == "In extreme cases"


class TestRemovePossessives():
    def test_singular(self):
        result = FwText.remove_possessives("Virginia's temperatures")
        assert result == "Virginia temperatures"
        
    def test_singular(self):
        result = FwText.remove_possessives("boys' scouting program")
        assert result == "boys scouting program"

    def test_number(self):
        result = FwText.remove_possessives("This is 2017's wettest month")
        assert result == "This is 2017 wettest month"


class TestRemovePeriods():
    def test_keep_decimal_points(self):
        result = FwText.remove_periods("I still owe you $500.00")
        assert result == "I still owe you $500.00"

    def test_remove_leading_trailing_dots(self):
        result = FwText.remove_periods("...Some words...")
        assert result == "Some words"

    def test_custom_replace_value(self):
        result = FwText.remove_periods("The end.", s_replace="<period>")
        assert result == "The end<period>"


class TestFixSentencePeriods():
    def test_blank(self):
        result = FwText.fix_sentence_periods("")
        assert result == ""
        
    def test_single_period(self):
        result = FwText.fix_sentence_periods(".")
        assert result == "."
        
    def test_keep_decimal_points(self):
        result = FwText.fix_sentence_periods("I still owe you $500.00.It was due last week.")
        assert result == "I still owe you $500.00. It was due last week."
        
        result = FwText.fix_sentence_periods("This needs to be done A.S.A.P.but not by you.")
        assert result == "This needs to be done A.S.A.P. but not by you."

    def test_add_space(self):
        result = FwText.fix_sentence_periods("This is a test.These sentences were not split.")
        assert result == "This is a test. These sentences were not split."
    

class TestReplaceAmpersands():
    def test_blank(self):
        result = FwText.replace_ampersands("")
        assert result == ""
        
    def test_keep_abbreviation(self):
        result = FwText.replace_ampersands("A&W root beer")
        assert result == "A&W root beer"
        
    def test_keep_html_characters(self):
        result = FwText.replace_ampersands("McDonalds &trade;")
        assert result == "McDonalds &trade;"
        
    def test_remove_between_words(self):
        result = FwText.replace_ampersands("root beer&french fries & hamburger")
        assert result == "root beer and french fries and hamburger"
        
    def test_remove_leading_trailing(self):
        result = FwText.replace_ampersands("& test&")
        assert result == "test"
    

class TestReplaceWith():
    def test_blank(self):
        result = FwText.replace_with("")
        assert result == ""
        
    def test_normal(self):
        result = FwText.replace_with("Hamburger with french fries")
        assert result == "Hamburger with french fries"
        
    def test_space(self):
        result = FwText.replace_with("Hamburger w french fries")
        assert result == "Hamburger with french fries"
        
    def test_slash_space(self):
        result = FwText.replace_with("Hamburger w/ french fries")
        assert result == "Hamburger with french fries"
        
    def test_slash(self):
        result = FwText.replace_with("Hamburger w/french fries")
        assert result == "Hamburger with french fries"
        
    def test_start(self):
        result = FwText.replace_with("W/ french fries")
        assert result == "with french fries"
        
    def test_white(self):
        result = FwText.replace_with("Phone B/W Cord")
        assert result == "Phone B/W Cord"


class TestRemoveTrademarks():
    def test_blank(self):
        result = FwText.remove_trademarks("")
        assert result == ""
        
    def test_normal(self):
        result = FwText.remove_trademarks("Aragorn® Lord of the Rings© McDonalds™ Cup")
        assert result == "Aragorn Lord of the Rings McDonalds Cup"
        
    def test_suffix(self):
        result = FwText.remove_trademarks("MicrosoftTM Office")
        assert result == "Microsoft Office"
        
    def test_abbreviation(self):
        result = FwText.remove_trademarks("Drive through ATM receipt")
        assert result == "Drive through ATM receipt"
    

class TestRemoveSlashes():
    def test_blank(self):
        result = FwText.remove_slashes("")
        assert result == ""
        
    def test_remove_slashes(self):
        result = FwText.remove_slashes("Black / White Tshirt in S/M/L")
        assert result == "Black  White Tshirt in S M L"
        
    def test_keep_fractions(self):
        result = FwText.remove_slashes("1 / 2 off 1/4 bits")
        assert result == "1 / 2 off 1/4 bits"
    

class TestRemoveHtmlCharacters():
    def test_blank(self):
        result = FwText.remove_html_characters("")
        assert result == ""
        
    def test_false_positive(self):
        result = FwText.remove_html_characters("A&W rootbeer; 12oz.")
        assert result == "A&W rootbeer; 12oz."
        
    def test_remove_html_characters(self):
        result = FwText.remove_html_characters("McDonalds &trade;")
        assert result == "McDonalds "
    

class TestReplaceHtmlCharacters():
    def test_blank(self):
        result = FwText.replace_html_characters("")
        assert result == ""
        
    def test_replace_html_characters(self):
        result = FwText.replace_html_characters(r"A&amp;W&trade; rootbeer; 12oz.&apos;")
        print(result)
        assert result == r"A&W™ rootbeer; 12oz.'"
    

class TestSortTokensByLength():
    def test_blank(self):
        result = FwText.sort_tokens_by_length([], b_descending=False)
        assert result == []
    
    def test_descending_default(self):
        result = FwText.sort_tokens_by_length(set(['test', 'a', 'length', 'check']), b_descending=True)
        assert result == ('length', 'check', 'test', 'a')
    
    def test_ascending(self):
        result = FwText.sort_tokens_by_length(set(['test', 'a', 'length', 'check']), b_descending=False)
        assert result == ('a', 'test', 'check', 'length')
        
    def test_list(self):
        result = FwText.sort_tokens_by_length(['test', 'a', 'length', 'check'], b_descending=False)
        assert result == ['a', 'test', 'check', 'length']
        
    def test_regexes(self):
        s_text = 'test a length check'
        a_patterns = ["a", "length", "check", "to", "test"]
        a_regexes = [re.compile(r"\b{}\b".format(pattern))
                     for pattern in a_patterns]
        a_matches = [o_match
                     for o_re in a_regexes
                     for o_match in o_re.finditer(s_text)]
        print(a_matches)
        a_matches = FwText.sort_tokens_by_length(a_matches)
        result = [s_text[match.start():match.end()] for match in a_matches]
        assert result == ['length', 'check', 'test', 'a']
        
    def test_list_elements(self):
        result = FwText.sort_tokens_by_length(
            [(3, 'test'), (4, 'a'), (1, 'length'), (2, 'check')],
            index=1,
        )
        assert result == [(1, 'length'), (2, 'check'), (3, 'test'), (4, 'a')]


class TestPosContainedInSpans():
    def test_none_pos(self):
        result = FwText.pos_contained_in_spans(
            None,
            [(1, 5), (10, 15)])
        assert result == False
    
    def test_empty_pos(self):
        result = FwText.pos_contained_in_spans(
            [],
            [(1, 5), (10, 15)])
        assert result == False

    def test_empty_spans(self):
        result = FwText.pos_contained_in_spans(
            (5, 10),
            [])
        assert result == False
    
    def test_no_overlap(self):
        result = FwText.pos_contained_in_spans(
            (5, 10),
            [(1, 5), (10, 15)])
        assert result == False
    
    def test_partial_overlaps(self):
        result = FwText.pos_contained_in_spans(
            (3, 12),
            [(1, 5), (10, 15)])
        assert result == False
    
    def test_contained(self):
        result = FwText.pos_contained_in_spans(
            (1, 5),
            [(1, 5), (10, 15)])
        assert result == True


class TestUniqueSubsetStrings():
    def test_blank_list(self):
        result = FwText.unique_subset_strings(
            [],
            FwText.word_tokenize("Web exclusive brandnew red doll Web- red")
        )
        assert result == []

    def test_blank_tokens(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'exclusive'],
            FwText.word_tokenize("")
        )
        assert result == ['Web exclusive', 'exclusive']

    def test_overlapped(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'exclusive'],
            FwText.word_tokenize("Web exclusive brandnew red doll Web- red")
        )
        assert result == ['Web exclusive']

    def test_overlapped_and_separate(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'exclusive'],
            FwText.word_tokenize("Web exclusive brandnew red exclusive doll Web- red")
        )
        assert result == ['Web exclusive', 'exclusive']

    def test_separate(self):
        result = FwText.unique_subset_strings(
            ['Web exclusive', 'brandnew', 'exclusive'],
            FwText.word_tokenize("Web exclusive brandnew red exclusive doll Web- red")
        )
        assert result == ['Web exclusive', 'exclusive', 'brandnew']


class TestUniqueRegexResults():
    regexes = [re.compile('web\s*exclusive', re.I),
               re.compile('brand\s*new', re.I),
               re.compile('exclusive', re.I),]
                 
    def test_blank_list(self):
        result = FwText.unique_regex_results(
            [],
            "Web exclusive brandnew red doll Web- red"
        )
        assert result == []

    def test_blank_tokens(self):
        result = FwText.unique_regex_results(
            self.regexes,
            ""
        )
        assert result == []

    def test_overlapped(self):
        result = FwText.unique_regex_results(
            self.regexes,
            "Web exclusive brandnew red doll Web- red"
        )
        assert result == ['Web exclusive', 'brandnew']

    def test_overlapped_and_separate(self):
        result = FwText.unique_regex_results(
            self.regexes,
            "Web exclusive brandnew red exclusive doll Web- red"
        )
        assert result == ['Web exclusive', 'exclusive', 'brandnew']

    def test_separate(self):
        result = FwText.unique_regex_results(
            self.regexes,
            "Web exclusive brandnew red exclusive doll Web- red"
        )
        assert result == ['Web exclusive', 'exclusive', 'brandnew']


class TestAllNGrams():
    def test_default_settings(self):
        result = FwText.all_ngrams("This is Sparta!")
        assert result == [["Sparta"]]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams("This is Sparta", stop_words=None)
        assert result == [
            ["This is Sparta"],
            ["This is", "is Sparta"],
            ["This", "is", "Sparta"]
        ]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams("This is Sparta", stop_words=None, i_max_size=2)
        assert result == [["This is", "is Sparta"], ["This", "is", "Sparta"]]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams("This is Sparta", stop_words=None, i_min_size=2)
        assert result == [["This is Sparta"], ["This is", "is Sparta"]]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams("This is Sparta!", remove_punct=False)
        assert result == [["Sparta!"]]


class TestAllNGramsLists():
    def test_default_settings(self):
        result = FwText.all_ngrams_lists("This is Sparta!")
        assert result == [[("Sparta",)]]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams_lists("This is Sparta", stop_words=None)
        assert result == [
            [("This", "is", "Sparta")],
            [("This", "is"), ("is", "Sparta")],
            [("This",), ("is",), ("Sparta",)]
        ]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams_lists("This is Sparta", stop_words=None, i_max_size=2)
        assert result == [
            [("This", "is"), ("is", "Sparta")],
            [("This",), ("is",), ("Sparta",)]
        ]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams_lists("This is Sparta", stop_words=None, i_min_size=2)
        assert result == [
            [("This", "is", "Sparta")],
            [("This", "is"), ("is", "Sparta")]
        ]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams_lists("This is Sparta!", remove_punct=False)
        assert result == [[("Sparta!",)]]


class TestAllNGramsFlat():
    def test_default_settings(self):
        result = FwText.all_ngrams_flat("This is Sparta!")
        assert result == ["Sparta"]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams_flat("This is Sparta", stop_words=None)
        assert result == ["This is Sparta", "This is", "is Sparta",
            "This", "is", "Sparta"]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams_flat("This is Sparta", stop_words=None, i_max_size=2)
        assert result == ["This is", "is Sparta", "This", "is", "Sparta"]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams_flat("This is Sparta", stop_words=None, i_min_size=2)
        assert result == ["This is Sparta", "This is", "is Sparta"]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams_flat("This is Sparta!", remove_punct=False)
        assert result == ["Sparta!"]


class TestAllNGramsTuples():
    def test_default_settings(self):
        result = FwText.all_ngrams_tuples("This is Sparta!")
        assert result == [("Sparta",)]

    def test_do_not_remove_stopwords(self):
        result = FwText.all_ngrams_tuples("This is Sparta", stop_words=None)
        assert result == [("This", "is", "Sparta"), ("This", "is"), ("is", "Sparta"),
            ("This",), ("is",), ("Sparta",)]

    def test_limit_max_ngram_length(self):
        result = FwText.all_ngrams_tuples("This is Sparta", stop_words=None, i_max_size=2)
        assert result == [("This", "is"), ("is", "Sparta"), ("This",), ("is",), ("Sparta",)]

    def test_limit_min_ngram_length(self):
        result = FwText.all_ngrams_tuples("This is Sparta", stop_words=None, i_min_size=2)
        assert result == [("This", "is", "Sparta"), ("This", "is"), ("is", "Sparta")]

    def test_do_not_remove_punctuation(self):
        result = FwText.all_ngrams_tuples("This is Sparta!", remove_punct=False)
        assert result == [("Sparta!",)]


class TestEndGrams():
    def test_blank(self):
        result = FwText.end_grams("")
        assert result == []
        
    def test_string(self):
        result = FwText.end_grams("This is a test")
        assert result == ["This is a test", "is a test", "a test", "test"]
        
    def test_list(self):
        result = FwText.end_grams(["This", "is", "a", "test"])
        assert result == [("This", "is", "a", "test"), ("is", "a", "test"), ("a", "test"), ("test", )]
        
    def test_list_to_string(self):
        result = FwText.end_grams(["This", "is", "a", "test"], return_tuples=False)
        assert result == ["This is a test", "is a test", "a test", "test"]
        

class TestSkipBigrams():
    def test_blank(self):
        result = FwText.skip_bigrams("")
        assert result == []
        
    def test_bigrams(self):
        result = FwText.skip_bigrams("This is a test", i_max_dist=4, i_min_dist=1, stop_words=None, remove_punct=None)
        assert result == [('This', 'is'), ('This', 'a'), ('This', 'test'),
                          ('is', 'a'), ('is', 'test'), ('a', 'test')]
        
    def test_skip_only(self):
        result = FwText.skip_bigrams("This is a test", i_max_dist=4, i_min_dist=2, stop_words=None, remove_punct=None)
        assert result == [('This', 'a'), ('This', 'test'), ('is', 'test')]


class TestExtractNLTKStems():
    def test_default_settings(self):
        result = FwText.extract_nltk_stems("Those Spartans are training with weights")
        assert result == "spartan train weight"

    def test_do_not_remove_stopwords(self):
        result = FwText.extract_nltk_stems("Those Spartans are training with weights",
            stop_words=None)
        assert result == "those spartan are train with weight"


class TestIsAlphaNumeric():
    def test_just_numbers(self):
        assert FwText.is_alpha_numeric("300") is False

    def test_just_letters(self):
        assert FwText.is_alpha_numeric("sparta") is False

    def test_letters_and_numbers(self):
        assert FwText.is_alpha_numeric("300spartans") is True

    def test_contains_spaces(self):
        assert FwText.is_alpha_numeric("300 spartans") is True

    def test_it_is_just_spaces(self):
        assert FwText.is_alpha_numeric("     ") is False

    def test_contains_punctuation(self):
        assert FwText.is_alpha_numeric("300 spartans!") is True

    def test_it_is_just_punctuation(self):
        assert FwText.is_alpha_numeric(":)") is False

    def test_non_ascii_are_not_letters(self):
        assert FwText.is_alpha_numeric("300 ñá") is False

    def test_empty_is_not_alphanumeric(self):
        assert FwText.is_alpha_numeric("") is False


class TestHasAlphaNumeri():
    def test_just_numbers(self):
        assert FwText.has_alpha_numeric("300") is True

    def test_just_letters(self):
        assert FwText.has_alpha_numeric("sparta") is True

    def test_letters_and_numbers(self):
        assert FwText.has_alpha_numeric("300spartans") is True

    def test_contains_spaces(self):
        assert FwText.has_alpha_numeric("300 spartans") is True

    def test_it_is_just_spaces(self):
        assert FwText.has_alpha_numeric("      ") is False

    def test_contains_punctuation(self):
        assert FwText.is_alpha_numeric("300 spartans!") is True

    def test_it_is_just_punctuation(self):
        assert FwText.is_alpha_numeric(":)") is False

    def test_non_ascii_are_not_letters(self):
        assert FwText.is_alpha_numeric("ñá") is False

    def test_empty_is_not_alphanumeric(self):
        assert FwText.is_alpha_numeric("") is False


class TestRemoveStopwords():
    def test_default_settings(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"])
        assert result == ["Sparta"]

    def test_do_not_remove_stopwords(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"], stop_words=None)
        assert result == ["This", "is", "Sparta"]

    def test_custom_stopwords(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"],
            stop_words=["Sparta"])
        assert result == ["This", "is"]

    def test_custom_stopwords_case_insensitive(self):
        result = FwText.remove_stopwords(["This", "is", "Sparta"],
            stop_words=["sparta"])
        assert result == ["This", "is"]


class TestStripQuotes():
    def test_blank(self):
        result = FwText.strip_quotes('')
        assert result == ''
    
    def test_single_quote(self):
        result = FwText.strip_quotes('"')
        assert result == '"'
    
    def test_double_quote(self):
        result = FwText.strip_quotes('""')
        assert result == '""'
    
    def test_normal(self):
        result = FwText.strip_quotes('"a"')
        assert result == 'a'
    
    def test_unmatched(self):
        result = FwText.strip_quotes('"test')
        assert result == '"test'
    
    def test_matched(self):
        result = FwText.strip_quotes('"test"', True)
        assert result == 'test'
    
    def test_start(self):
        result = FwText.strip_quotes('"test', True)
        assert result == 'test'
        
    def test_end(self):
        result = FwText.strip_quotes('test"', True)
        assert result == 'test'


class TestSplitStrip():
    def test_blank(self):
        result = FwText.split_strip("")
        assert result == []
        
    def test_default_settings(self):
        result = FwText.split_strip("This  Example")
        assert result == ["This", "Example"]

    def test_multiple_spaces(self):
        result = FwText.split_strip(" This  Example ")
        assert result == ["This", "Example"]

    def test_custom_delimiter(self):
        result = FwText.split_strip("This : Example", s_delimiter=":")
        assert result == ["This", "Example"]
        

class TestWordSplit():
    def test_empty(self):
        a_words = []
        FwText.split_words(a_words, FwText.split_camelcase)
        assert a_words == []
        FwText.split_words(a_words, FwText.split_number_uom)
        assert a_words == []
        FwText.split_words(a_words, FwText.split_edge_punctuation)
        assert a_words == []
        FwText.split_words(a_words, FwText.split_edge_quotes)
        assert a_words == []
    
    def test_camel_case_words(self):
        a_words = ['CamelCase', 'for', 'TestExampleSplit']
        FwText.split_words(a_words, FwText.split_camelcase)
        assert a_words == ['Camel', 'Case', 'for', 'Test', 'Example', 'Split']
    
    def test_number_uom(self):
        a_words = ['Cardboard', 'Box', '10inch', 'x', '10in.', 'x', '12-in']
        FwText.split_words(a_words, FwText.split_number_uom)
        assert a_words == ['Cardboard', 'Box', '10', 'inch', 'x', '10', 'in.', 'x', '12', 'in']
    
    def test_edge_punct(self):
        a_words = ["-test'/", '"this;', "-out."]
        FwText.split_words(a_words, FwText.split_edge_punctuation)
        assert a_words == ['-', "test'", '/', '"this', ';', '-', 'out', '.']
    
    def test_edge_quotes(self):
        a_words = ["'test'", "this'", "'out", '"and"', '"split', 'quotes"', "36'", "'72"]
        FwText.split_words(a_words, FwText.split_edge_quotes)
        assert a_words == ["'", 'test', "'", 'this', "'", "'", 'out', '"',
                           'and', '"', '"', 'split', 'quotes', '"', "36'", "'", '72']
    
class TestSplitCamelCase():
    def test_empty(self):
        result = FwText.split_camelcase('')
        assert result == tuple()
        
    def test_two_words(self):
        result = FwText.split_camelcase('PyCon')
        assert result == ('Py', 'Con')
        
    def test_three_words(self):
        result = FwText.split_camelcase('PyConNews')
        assert result == ('Py', 'Con', 'News')
        
    def test_too_short_first(self):
        result = FwText.split_camelcase('PCon')
        assert result == tuple()
        
    def test_too_short_last(self):
        result = FwText.split_camelcase('PyC')
        assert result == tuple()
        

class TestSplitNumberUOM():
    def test_empty(self):
        result = FwText.split_number_uom('')
        assert result == tuple()
        
    def test_non_matches(self):
        result = FwText.split_number_uom('none')
        assert result == tuple()
        result = FwText.split_number_uom('mile1')
        assert result == tuple()
        
    def test_matches(self):
        result = FwText.split_number_uom('1In')
        assert result == ('1', 'In')
        result = FwText.split_number_uom('1in.')
        assert result == ('1', 'in.')
        result = FwText.split_number_uom('1inch')
        assert result == ('1', 'inch')
        result = FwText.split_number_uom('2inches')
        assert result == ('2', 'inches')
        result = FwText.split_number_uom('1 in')
        assert result == ('1', 'in')
        result = FwText.split_number_uom('1-in.')
        assert result == ('1', 'in.')
        result = FwText.split_number_uom('1__inch')
        assert result == ('1', 'inch')
        result = FwText.split_number_uom('2.inches')
        assert result == ('2', 'inches')
        result = FwText.split_number_uom('1.5_inch')
        assert result == ('1.5', 'inch')
        result = FwText.split_number_uom('1-1/2inch')
        assert result == ('1-1/2', 'inch')
        result = FwText.split_number_uom('2 3/4 inch')
        assert result == ('2 3/4', 'inch')
        result = FwText.split_number_uom('3 - 1/4 inch')
        assert result == ('3 - 1/4', 'inch')
    
class TestSplitEdgePunctuation():
    def test_empty(self):
        assert FwText.split_edge_quotes("") == ""
    
    def test_plain_quotes(self):
        assert FwText.split_edge_quotes("'") == "'"
        assert FwText.split_edge_quotes("''") == "''"
        assert FwText.split_edge_quotes('"') == '"'
        assert FwText.split_edge_quotes('""') == '""'
        
    def test_plain_quotes(self):
        assert FwText.split_edge_quotes("'") == "'"
        assert FwText.split_edge_quotes("''") == "''"
        
    def test_words(self):
        assert FwText.split_edge_quotes('"test') == ['"', 'test']
        assert FwText.split_edge_quotes('test"') == ['test', '"']
        assert FwText.split_edge_quotes('"test"') == ['"', 'test', '"']
        assert FwText.split_edge_quotes("'test") == ["'", 'test']
        assert FwText.split_edge_quotes("test'") == ['test', "'"]
        assert FwText.split_edge_quotes("'test'") == ["'", 'test', "'"]
        
    def test_letters(self):
        assert FwText.split_edge_quotes('"a') == ['"', 'a']
        assert FwText.split_edge_quotes('a"') == ['a', '"']
        assert FwText.split_edge_quotes('"a"') == ['"', 'a', '"']
        assert FwText.split_edge_quotes("'b") == ["'", 'b']
        assert FwText.split_edge_quotes("b'") == ['b', "'"]
        assert FwText.split_edge_quotes("'b'") == ["'", 'b', "'"]
        
    def test_numbers(self):
        assert FwText.split_edge_quotes('"1') == ['"', '1']
        assert FwText.split_edge_quotes('1"') == '1"'
        assert FwText.split_edge_quotes('"1"') == ['"', '1', '"']
        assert FwText.split_edge_quotes("'0") == ["'", '0']
        assert FwText.split_edge_quotes("0'") == "0'"
        assert FwText.split_edge_quotes("'0'") == ["'", '0', "'"]
    
    
class TestJoinOxford():
    def test_blank(self):
        a_test = []
        result = FwText.join_oxford(a_test)
        assert result == ''
    
    def test_one_word(self):
        a_test = ['test']
        result = FwText.join_oxford(a_test)
        assert result == 'test'
    
    def test_two_words(self):
        a_test = ['test', 'this']
        result = FwText.join_oxford(a_test)
        assert result == 'test, and this'
    
    def test_three_words(self):
        a_test = ['test', 'this', 'function']
        result = FwText.join_oxford(a_test)
        assert result == 'test, this, and function'
    
    
class TestJoinSentence():
    def test_blank(self):
        a_test = []
        result = FwText.join_sentence(a_test)
        assert result == ''
    
    def test_one_punct(self):
        a_test = ['-']
        result = FwText.join_sentence(a_test)
        assert result == '-'
    
    def test_sentence(self):
        a_test = ['This', 'is', 'a', '100', '%', 'test', '-', 'sentence', 'w', '/', 'slashes.']
        result = FwText.join_sentence(a_test)
        assert result == 'This is a 100% test-sentence w/slashes.'


class TestStripUnicodeAccents():
    def test_default_settings(self):
        result = FwText.strip_unicode_accents("áàäâ")
        assert result == "aaaa"


class TestCleanTitle():
    def test_blank(self):
        result = FwText.clean_title('')
        assert result == ''

    def test_hyphens(self):
        result = FwText.clean_title('This is a cleaning-test - for titles')
        assert result == 'This is a cleaning-test for titles'

    def test_apostrophes(self):
        result = FwText.clean_title("Tim's test for 'cleaning' titles")
        assert result == "Tim's test for cleaning titles"

    def test_periods(self):
        result = FwText.clean_title("Title Quality Analysis. Version 2.0")
        assert result == "Title Quality Analysis Version 2.0"

    def test_trademarks(self):
        result = FwText.clean_title("BankTM ATM Plaque™")
        assert result == "Bank ATM Plaque"

    def test_ampersands(self):
        result = FwText.clean_title("A&W Burger & Fries")
        assert result == "A&W Burger and Fries"

    def test_with(self):
        result = FwText.clean_title("Burger w / Fries")
        assert result == "Burger with Fries"

    def test_html_characters(self):
        result = FwText.clean_title("&apos;BODY&apos; T Shirt")
        assert result == "'BODY' T Shirt"

    def test_other_punctuation(self):
        result = FwText.clean_title("Pants &amp; T Shirt")
        assert result == "Pants and T Shirt"


class TestCleanDescription():
    def test_blank(self):
        result = FwText.clean_description('')
        assert result == ''

    def test_hyphens(self):
        result = FwText.clean_description('This is a cleaning-test - for titles')
        assert result == 'This is a cleaning-test for titles'

    def test_apostrophes(self):
        result = FwText.clean_description("Tim's test for 'cleaning' titles")
        assert result == "Tim's test for cleaning titles"

    def test_periods(self):
        result = FwText.clean_description("Title Quality Analysis. Version 2.0")
        assert result == "Title Quality Analysis Version 2.0"

    def test_trademarks(self):
        result = FwText.clean_description("BankTM ATM Plaque™")
        assert result == "Bank ATM Plaque"

    def test_ampersands(self):
        result = FwText.clean_description("A&W Burger & Fries")
        assert result == "A&W Burger and Fries"

    def test_with(self):
        result = FwText.clean_description("Burger w / Fries")
        assert result == "Burger with Fries"

    def test_html_characters(self):
        result = FwText.clean_description("&apos;BODY&apos; T Shirt")
        assert result == "'BODY' T Shirt"

    def test_other_punctuation(self):
        result = FwText.clean_description("Pants &amp; T Shirt")
        assert result == "Pants and T Shirt"


class TestCleanText():
    def test_default_settings(self):
        result = FwText.clean_text("This is Spârtâ!")
        assert result == "This is Sparta!"

    def test_do_no_remove_accents(self):
        result = FwText.clean_text("This is Spârtâ!", b_accents=False)
        assert result == "This is Spârtâ!"

    def test_lowercase(self):
        result = FwText.clean_text("This is Sparta!", b_lowercase=True)
        assert result == "this is sparta!"

    def test_punctuation(self):
        # Missing test case for the punctuation defined by its bytes representation
        result = FwText.clean_text("remove these +(),; only", b_punctuation=True)
        assert result == "remove these only"

    def test_normalize_digits(self):
        result = FwText.clean_text(
            "They were 300 Spartans",
            b_normalize_digits=True
        )
        assert result == "They were 000 Spartans"

    def test_product_numbers(self):
        # This feature will be tested in depth in TestMPNPatterns
        result = FwText.clean_text("Product number 0.0.0", b_product_numbers=True)
        assert result == "Product number"

    def test_product_dimensions(self):
        # This feature will be tested in depth when we test
        # dimensions_tokenizers.tokenize_dimensions
        # For now this test will just make sure it works
        result = FwText.clean_text(
            "6 pack of Spartan drinks",
            b_product_dimensions=True
        )
        assert result == "6_pack of Spartan drinks"


class TestCleanGroupOfDicts():
    def test_default_settings(self):
        test_dict = {"outter_key":{"Sôme-key1": "sóme$Value"}}
        result = FwText.clean_group_of_dicts(test_dict)
        assert result == {"outter_key":{"Some-key1":"some$Value"}}

    def test_lowercase(self):
        test_dict = {"outter_key":{"SOME_KEY":"SOME_VALUE"}}
        result = FwText.clean_group_of_dicts(test_dict, b_lowercase=True)
        assert result == {"outter_key":{"some_key":"some_value"}}

    def test_punctuation(self):
        # This feature should be fully tested on TestCleanText.test_punctuation
        test_dict = {
            "outter_key":{
                "some;key":"some+value",
                "another(key":"another)value"
            }
        }
        result = FwText.clean_group_of_dicts(test_dict, b_punctuation=True)
        assert result == {
            "outter_key":{
                "some key":"some value",
                "another key":"another value"
                }
            }

    def test_normalize_digits(self):
        test_dict = {
            "outter_key":{
                "key_one":"1",
                "key_twenty":"20"
            }
        }
        result = FwText.clean_group_of_dicts(test_dict, b_normalize_digits=True)
        assert result == {
            "outter_key":{
                "key_one":"0",
                "key_twenty":"00"
            }
        }

    def test_product_numbers(self):
        # This feature is tested in depth in TestMPNPatterns
        pass

    def test_list_of_dicts(self):
        test_list = [{"some;key":"some+value", "another(key":"another)value"},
            {"some;key":"some+value", "another(key":"another)value"}]
        result = FwText.clean_group_of_dicts(test_list, b_punctuation=True)
        assert result == [{"some key":"some value", "another key":"another value"},
            {"some key":"some value", "another key":"another value"}]

    def test_do_not_support_single_dictionary(self):
        test_dict = {"some;key":"some+value", "another(key":"another)value"}
        with pytest.raises(TypeError) as error_info:
            result = FwText.clean_group_of_dicts(test_dict)
        assert error_info.value.__str__() == "Expected dict, got str"


class TestCleanList():
    def test_default_settings(self):
        result = FwText.clean_list(["This is Spârtá!", "Thât is àlso Sparta."])
        assert result == ["This is Sparta!", "That is also Sparta."]

    def test_do_not_remove_accents(self):
        result = FwText.clean_list(["This is Spàrta!"], b_accents=False)
        assert result == ["This is Spàrta!"]

    def test_lowercase(self):
        result = FwText.clean_list(["This is Sparta!", "That is Sparta"], b_lowercase=True)
        assert result == ["this is sparta!", "that is sparta"]

    def test_punctuation(self):
        # This feature should be fully tested in TestCleanText.test_punctuation
        result = FwText.clean_list(["This+is+Sparta!"], b_punctuation=True)
        assert result == ["This is Sparta!"]

    def test_normalize_digits(self):
        result = FwText.clean_list(["They were 300 Spartans"], b_normalize_digits=True)
        assert result == ["They were 000 Spartans"]

    def test_product_numbers(self):
        # This feature should be tested in depth in TestMPNPatterns
        pass


class TestMPNPatterns():
    def test_pattern_mpn_1(self):
        pattern = FwText.FwText.a_mpn_patterns[0]
        assert re.search(pattern, "ab0a0")
        assert re.search(pattern, "a0a0") is None
        assert re.search(pattern, "abc0abc0")
        assert re.search(pattern, "ab-0-ab0")
        assert re.search(pattern, "ab-000-ab0")
        assert re.search(pattern, "ab-0-ab0-ab0")
        assert re.search(pattern, "ab-0-ab0ab0")
        assert re.search(pattern, "ab00ab00")
        assert re.search(pattern, "#ab0ab0")

    def test_pattern_mpn_2(self):
        pattern = FwText.FwText.a_mpn_patterns[1]
        assert re.search(pattern, "ab0a")
        assert re.search(pattern, "#ab0a")
        assert re.search(pattern, "abc0a")
        assert re.search(pattern, "a0a") is None
        assert re.search(pattern, "aba") is None
        assert re.search(pattern, "ab-0a")
        assert re.search(pattern, "ab-0-a")
        assert re.search(pattern, "ab00a")
        assert re.search(pattern, "ab0-a")
        assert re.search(pattern, "ab0ab")
        assert re.search(pattern, "ab0") is None
        assert re.search(pattern, "ab0a0a")

    def test_pattern_mpn_3(self):
        pattern = FwText.FwText.a_mpn_patterns[2]
        assert re.search(pattern, "ab0a")
        assert re.search(pattern, "#ab0a")
        assert re.search(pattern, "a0a") is None
        assert re.search(pattern, "abc0a")
        assert re.search(pattern, "ab-0a")
        assert re.search(pattern, "ab00a")
        assert re.search(pattern, "aba") is None
        assert re.search(pattern, "ab0") is None
        assert re.search(pattern, "ab0aa")
        assert re.search(pattern, "ab-0a-0a")

    def test_pattern_mpn_4(self):
        pattern = FwText.FwText.a_mpn_patterns[3]
        assert re.search(pattern, "ab0")
        assert re.search(pattern, "#ab0")
        assert re.search(pattern, "abc0")
        assert re.search(pattern, "a0") is None
        assert re.search(pattern, "ab-0")
        assert re.search(pattern, "ab") is None
        assert re.search(pattern, "ab00")

    def test_pattern_mpn_5(self):
        pattern = FwText.FwText.a_mpn_patterns[4]
        assert re.search(pattern, "0000")
        assert re.search(pattern, "#0000")
        assert re.search(pattern, "000") is None
        assert re.search(pattern, "00000")

    def test_pattern_mpn_6(self):
        pattern = FwText.FwText.a_mpn_patterns[5]
        assert re.search(pattern, "00abc0a0")
        assert re.search(pattern, "#00abc0a0")
        assert re.search(pattern, "0abc0a0") is None
        assert re.search(pattern, "00-abc0a0")
        assert re.search(pattern, "00-abc-0-0")
        assert re.search(pattern, "00abcd0a0")
        assert re.search(pattern, "00ab0a0") #is None
        assert re.search(pattern, "00in-0in") is None
        assert re.search(pattern, "00abc-0a0")
        assert re.search(pattern, "00abc00a0")
        assert re.search(pattern, "00abca0") #is None
        assert re.search(pattern, "00abc0-a0")
        assert re.search(pattern, "00abc0aa0")
        assert re.search(pattern, "00abc00") #is None
        assert re.search(pattern, "00abc0a-0")
        assert re.search(pattern, "00abc0a") #is None
        assert re.search(pattern, "00abc0a00")

    def test_pattern_mpn_7(self):
        pattern = FwText.FwText.a_mpn_patterns[6]
        assert re.search(pattern, "00abc")
        assert re.search(pattern, "#00abc")
        assert re.search(pattern, "0abc") is None
        assert re.search(pattern, "000abc")
        assert re.search(pattern, "00-abc")
        assert re.search(pattern, "00abcd")
        assert re.search(pattern, "00ab") is None

    def test_pattern_mpn_8(self):
        pattern = FwText.FwText.a_mpn_patterns[7]
        assert re.search(pattern, "a0a0")
        assert re.search(pattern, "aa0a0")
        assert re.search(pattern, "0a0") is None
        assert re.search(pattern, "a00a0")
        assert re.search(pattern, "aa0") is None
        assert re.search(pattern, "a0aa0")
        assert re.search(pattern, "a00") is None
        assert re.search(pattern, "a0a00")
        assert re.search(pattern, "a0a") is None
        assert re.search(pattern, "a0a0a0")

    def test_pattern_mpn_9(self):
        pattern = FwText.FwText.a_mpn_patterns[8]
        assert re.search(pattern, "0.0.0")
        assert re.search(pattern, "00.0.0")
        assert re.search(pattern, ".0.0") is None
        assert re.search(pattern, "0.00.0")
        assert re.search(pattern, "0..0") is None
        assert re.search(pattern, "0.0.00")
        assert re.search(pattern, "0.0.") is None
        assert re.search(pattern, "0.0.0.0")
        

class TestRemoveAllPunctuation():
    def test_default_settings(self):
        result = FwText.remove_all_punctuation("This. Is. Sparta{}".format(
            string.punctuation))
        assert result == "This Is Sparta"

    def test_do_not_keep_spaces(self):
        result = FwText.remove_all_punctuation("This. Is. Sparta{}".format(
            string.punctuation), b_keepspaces=False)
        assert result == "ThisIsSparta"


class TestAreTwoSubstringsConsecutive():
    def test_ordered_case_sensitive_match(self):
        result = FwText.are_two_substrings_consecutive(
            "This is Sparta",
            ["This is", "Sparta"]
        )
        assert result is True

    def test_fail_unordered_match(self):
        result = FwText.are_two_substrings_consecutive(
            "This is Sparta",
            ["Sparta", "This is"]
        )
        assert result is False

    def test_case_insensitive_match(self):
        result = FwText.are_two_substrings_consecutive(
            "This is Sparta",
            ["this is", "sparta"]
        )
        assert result is True

    def test_case_sensitive_match(self):
        result = FwText.are_two_substrings_consecutive(
            "this is sparta", ["THIS IS", "SPARTA"],
            case_sensitive = True
        )

    def test_separated_multiple_spaces(self):
        result = FwText.are_two_substrings_consecutive(
            "This is                 Sparta",
            ["This is", "Sparta"]
        )
        assert result is True

    def test_separated_by_punctuation(self):
        result = FwText.are_two_substrings_consecutive(
            "This is + Sparta",
            ["This is", "Sparta"]
        )
        assert result is True

    def test_separated_by_word(self):
        result = FwText.are_two_substrings_consecutive(
            "This is not Sparta",
            ["This is", "Sparta"]
        )
        assert result is False

    def test_more_than_two_substrings(self):
        with pytest.raises(Exception) as error_info:
            result = FwText.are_two_substrings_consecutive(
                "This is not Sparta",
                ["This", "is not", "Sparta"]
            )
        assert "This function only takes 2 substrings" in error_info.__str__()


class TestAreConsecutiveOrdered():
    def test_default_settings(self):
        result = FwText.are_consecutive_ordered(
            "This is Sparta", ["This", "is", "Sparta"])
        assert result is True

    def test_case_sensitive(self):
        result = FwText.are_consecutive_ordered(
            "THIS IS SPARTA", ["this", "is", "sparta"],
            case_sensitive=True
        )
        assert result is False

    def test_unordered(self):
        result = FwText.are_consecutive_ordered(
            "This is Sparta", ["Sparta", "is", "this"]
        )
        assert result is False


class TestAreSubstringsConsecutive():
    def test_default_settings(self):
        result = FwText.are_substrings_consecutive(
            "This is Sparta", ["this", "is", "sparta"])
        assert result is True

    def test_unordered_substrings(self):
        result = FwText.are_substrings_consecutive(
            "This is Sparta", ["Sparta", "is", "this"])
        assert result is True

    def test_case_sensitive(self):
        result = FwText.are_substrings_consecutive(
            "THIS IS SPARTA", ["this", "is", "sparta"],
            case_sensitive = True)
        assert result is False

    def test_separated_multiple_spaces(self):
        result = FwText.are_substrings_consecutive(
            "This is        Sparta", ["This", "is", "Sparta"]
        )
        assert result is True

    def test_separated_by_punctuation(self):
        result = FwText.are_substrings_consecutive(
            "This+is+sparta", ["This", "is", "Sparta"]
        )
        assert result is True


class TestEditDistance():
    def test_case_sensitive(self):
        result = FwText.edit_distance("Sparta", "sparta")
        assert result == 1

    def test_case_insensitive(self):
        result = FwText.edit_distance("Sparta", "sparta", b_case_insensitive=True)
        assert result == 0


class TestDifferences():
    def test_case_difference(self):
        result = FwText.text_differences("This is Sparta", "this is sparta")
        assert result == {"case": 2}

    def test_punctuation_difference(self):
        result = FwText.text_differences("This is Sparta", "This is Sparta!")
        assert result == {"punctuation": 1}

    def test_spaces_are_punctuation(self): # Is this intended?
        result = FwText.text_differences("This is Sparta", "This is  Sparta")
        assert result == {"punctuation": 1}

    def test_letters_differences(self):
        result = FwText.text_differences("This is not Sparta", "This is Sparta")
        assert result["letters"] == 3 and result["punctuation"] == 1
    

class TestTextListToNGramList():
    def test_default_settings(self):
        result = FwText.text_list_to_ngram_list([
            "Sparta is full of Spartans",
            "This is Sparta"
        ])
        assert result == [
            ["sparta full spartans", "sparta full", "full spartans",
            "sparta", "full", "spartans"],
            ["sparta"]
        ]

    def test_max_size(self):
        result = FwText.text_list_to_ngram_list(
            ["Sparta is full of Spartans", "This is Sparta!"],
            i_max_size=2)
        assert result == [
            ["sparta full", "full spartans", "sparta", "full", "spartans"],
            ["sparta"]
        ]

    def test_min_size(self):
        result = FwText.text_list_to_ngram_list(
            ["Sparta is full of Spartans", "This is Sparta!"],
            i_min_size=2)
        assert result == [
            ["sparta full spartans", "sparta full", "full spartans"],
            []
        ]

    def test_do_not_remove_stopwords(self):
        result = FwText.text_list_to_ngram_list(
            ["Sparta is full of Spartans", "This is Sparta!"],
            stop_words=None)
        assert result == [
            ["sparta is full of spartans", "sparta is full of",
            "is full of spartans", "sparta is full", "is full of",
            "full of spartans", "sparta is", "is full", "full of",
            "of spartans", "sparta", "is", "full", "of", "spartans"],
            ["this is sparta", "this is", "is sparta", "this", "is",
            "sparta"]
        ]

    def test_remove_punctuation(self):
        result = FwText.text_list_to_ngram_list(
            ["This is Sparta!"], remove_punctuation=False)
        assert result == [["sparta!"]]

    def test_case_sensitive(self):
        result = FwText.text_list_to_ngram_list(
            ["This is Sparta"], b_case_insensitive=False)
        assert result == [["Sparta"]]

    def test_return_tuples(self):
        result = FwText.text_list_to_ngram_list(
            ["This is Sparta"], b_return_tuples=True)
        assert result == [[("sparta",)]]



class TestNGramFreqCoverageInList():
    def test_default_behaviour(self):
        freq, coverage, max_count = FwText.ngram_freq_coverage_in_list([
            ["this", "is", "Sparta"],
            ["that", "is", "Sparta"],
            ["this", "is", "this", "good"]
        ])
        # verify frequency results
        assert freq["this"] == 3
        assert freq["is"] == 3
        assert freq["Sparta"] == 2
        assert freq["that"] == 1
        assert freq["good"] == 1
        # verify coverage results
        assert coverage["this"] == 2
        assert coverage["is"] == 3
        assert coverage["Sparta"] == 2
        assert coverage["that"] == 1
        assert coverage["good"] == 1
        # verify max_count result
        assert max_count["this"] == 2
        assert max_count["is"] == 1
        assert max_count["Sparta"] == 1
        assert max_count["that"] == 1
        assert max_count["good"] == 1

    def test_single_string_not_wrapped_as_list(self):
        with pytest.raises(TypeError) as error_info:
            result = FwText.ngram_freq_coverage_in_list(["This", "is","Sparta"])
            assert error_info.__str__() == "Expected list, got str"

    def test_single_string_wrapped_as_list(self):
        freq, coverage, max_count = FwText.ngram_freq_coverage_in_list([
            ["This", "is", "Sparta"]
        ])
        assert sum(freq.values()) == 3
        assert sum(coverage.values()) == 3
        assert sum(max_count.values()) == 3

    def test_case_sensitivity(self):
        freq, coverage, max_count = FwText.ngram_freq_coverage_in_list([
            ["This", "and", "this"]
        ])
        assert len(freq.keys()) == len(coverage.keys()) == len(max_count.keys()) == 3

class TestNGramFreqInList():
    def test_default_behaviour(self):
        result = FwText.ngram_freq_in_list([
            ["this", "is", "sparta"],
            ["this", "is", "not", "sparta"]
        ])
        assert result["this"] == 2
        assert result["is"] == 2
        assert result["not"] == 1
        assert result["sparta"] == 2


class TestTupleNGramFreqInList():
    def test_default_behaviour(self):
        result = FwText.tuple_ngram_freq_in_list([
            [("this",), ("is",), ("sparta",)]
        ])
        assert result[("this",)] == 1
        assert result[("is",)] == 1
        assert result[("sparta",)] == 1

    def test_fail_if_not_tuples(self):
        with pytest.raises(TypeError) as error_info:
            result = FwText.tuple_ngram_freq_in_list([
                ["This", "is", "Sparta"]
            ])
            expected_error = "Expected tuple, got str"
            assert error_info.__str__() == expected_error


class TestNgramCoverageInList():
    def test_default_behavior(self):
        result = FwText.ngram_coverage_in_list([
            ["this", "is", "Sparta"],
            ["this", "is", "also", "Sparta"]
        ])
        assert result["this"] == 2
        assert result["is"] == 2
        assert result["also"] == 1
        assert result["Sparta"] == 2

    def test_case_sensitivity(self):
        result = FwText.ngram_coverage_in_list([
            ["This", "and", "this"]
        ])
        assert len(result.keys()) == 3


class TestTupleNGramCoverageInList():
    def test_default_behavior(self):
        result = FwText.tuple_ngram_coverage_in_list([
            [("this",), ("is",), ("sparta",)],
            [("that",), ("is",), ("sparta",)]
        ])
        assert result[("this",)] == 1
        assert result[("is",)] == 2
        assert result[("sparta",)] == 2
        assert result[("that",)] == 1

    def test_case_sensitivity(self):
        result = FwText.tuple_ngram_coverage_in_list([
            [("This",), ("and",), ("this",)]
        ])
        assert len(result.keys()) == 3

    def test_fail_if_not_tuples(self):
        with pytest.raises(TypeError) as error_info:
            result = FwText.tuple_ngram_coverage_in_list([
                ["This", "is", "Sparta"]
            ])
            expected_error = "Expected tuple, got str"
            assert error_info.__str__() == expected_error

    
class TestNgramTable():
    def test_default_settings(self):
        result = FwText.ngram_table(["This is Sparta", "That is Sparta"])
        assert result == [(0, ("Sparta",), 2, 2, 0, 1)]

    def test_do_not_remove_stopwords(self):
        result = FwText.ngram_table(
            ["This is Sparta", "That is Sparta"],
            stop_words = None
        )
        assert len(result) == 9

    def test_max_ngram_size(self):
        result = FwText.ngram_table(
            ["This is Sparta", "That is Sparta"],
            stop_words = None, i_max_size=2
        )
        assert len(result) == 7

    def test_min_ngram_size(self):
        result = FwText.ngram_table(
            ["This is Sparta", "That is Sparta"],
            stop_words = None, i_min_size=2
        )
        assert len(result) == 5

    def test_do_not_remove_punctuation(self):
        result = FwText.ngram_table(["This is Sparta!"],
            remove_punctuation=False)
        assert result == [(0, ("Sparta!",), 1, 1, 0, 1)]

    def test_case_sensitive(self):
        result = FwText.ngram_table(["Sparta sparta"],
            b_case_insensitive=False)
        assert len(result) == 3 # "Sparta", "sparta", "Sparta sparta"


class TestNGramCount():
    ngrams = {
        ("test", ): 1, ("tests", ): 1,
        ("ngramtest", ): 1, ("ngramtests", ): 1,
        ("ngram-test", ): 1, ("ngram-tests", ): 1,
        ("ngram_test", ): 1, ("ngram_tests", ): 1,
        ("ngram", "test"): 1, ("ngram", "tests"): 1,
    }

    def test_blank(self):
        result = FwText.get_ngram_count(tuple([]), self.ngrams)
        assert result == 0

    def test_singular(self):
        result = FwText.get_ngram_count(('test', ), self.ngrams)
        assert result == 2

    def test_plural(self):
        result = FwText.get_ngram_count(('tests', ), self.ngrams)
        assert result == 2

    def test_compound(self):
        result = FwText.get_ngram_count(('ngramtest', ), self.ngrams)
        assert result == 8

    def test_bigram(self):
        result = FwText.get_ngram_count(('ngram', 'test'), self.ngrams)
        assert result == 8


class TestCombinePluralSingularStems():
    ngrams = {
        ("test", ): 1, ("tests", ): 1,
        ("ngramtest", ): 1, ("ngramtests", ): 1,
        ("ngram-test", ): 1, ("ngram-tests", ): 1,
        ("ngram_test", ): 1, ("ngram_tests", ): 1,
        ("ngram", "test"): 1, ("ngram", "tests"): 1,
    }
    
    def test_blank(self):
        result = FwText.combine_plural_singular_stems({})
        assert result == {}

    def test_combine(self):
        result = FwText.combine_plural_singular_stems(self.ngrams)
        print(result)
        assert result[('test', )] == 2
        assert result[('tests', )] == 2
        assert result[('ngramtest', )] == 2
        assert result[('ngramtests', )] == 2
        assert result[('ngram-test', )] == 2
        assert result[('ngram-tests', )] == 2
        assert result[('ngram_test', )] == 2
        assert result[('ngram_tests', )] == 2
        assert result[('ngram', 'test')] == 2
        assert result[('ngram', 'tests')] == 2


class TestWordInText():
    def test_default_settings(self):
        result = FwText.word_in_text("This is Sparta", "Sparta")
        assert result == (7, 14)

    def test_plural_and_singular(self):
        result = FwText.word_in_text("These are Spartans", "Spartan")
        assert result == (9, 18)
        result = FwText.word_in_text("This is a Spartan", "Spartans")
        assert result == (9, 17)

    def test_position_function(self):
        # Testing each position function independently
        # in their respective classes.
        pass


class TestWordInStr():
    def test_word_in_str(self):
        result = FwText.word_instr("This is Sparta", "Sparta")
        assert result == (7, 14)

    def test_case_sensitivity(self):
        result = FwText.word_instr("This is Sparta", "sparta")
        assert result is None

    def test_word_boundaries_importance(self):
        result = FwText.word_instr("This is a Spartan", "Sparta")
        assert result is None


class TestFindCharSequence():
    def test_default_settings(self):
        result = FwText.find_char_sequence("This is Sparta", "Sparta")
        assert result == (8, 14)

    def test_word_boundaries_not_important(self):
        result = FwText.find_char_sequence("These are Spartans", "Sparta")
        assert result == (10, 16)

    def test_ignore_some_punctuation(self):
        result = FwText.find_char_sequence("This is Sparta", "Spa__r__ta")
        assert result == (8, 14)
        result = FwText.find_char_sequence("This is Spa__r__ta", "Sparta")
        assert result == (8, 18)


class TestFindWordSequence():
    def test_default_settings(self):
        result = FwText.find_word_sequence("This is Sparta", "Sparta")
        assert result == (8, 14)

    def test_ignore_some_punctuation(self):
        result = FwText.find_word_sequence("This is-Sparta", "is Sparta")
        assert result == (5, 14)
        result = FwText.find_word_sequence("This is Sparta", "is-Sparta")
        assert result == (5, 14)
        result = FwText.find_word_sequence("This is'Sparta", "is Sparta")
        assert result == (5, 14)
        result = FwText.find_word_sequence("This is Sparta", "is'Sparta")
        assert result == (5, 14)

    def test_try_plural_singular(self):
        # Not sure if this feature if working
        pass


class TestPluralize():
    # The capitalization of the results is inconsisten with singularize
    def test_blank(self):
        assert FwText.pluralize("") is None
        
    def test_this_is_sparta(self):
        assert FwText.pluralize("this") == "these"
        assert FwText.pluralize("is") == "are"
        assert FwText.pluralize("Sparta") == "Spartas"

    def test_already_plural(self):
        assert FwText.pluralize("pieces") == "pieces"
        assert FwText.pluralize("spartans") == "spartans"


class TestSingularize():
    # The capitalization of the results is inconsistent with pluralize
    def test_blank(self):
        assert FwText.singularize("") is None
        
    def test_these_are_spartas(self):
        assert FwText.singularize("these") == "this"
        assert FwText.singularize("are") == "is"
        assert FwText.singularize("spartas") == "sparta"

    def test_already_singular(self):
        assert FwText.singularize("piece") == "piece"
        assert FwText.singularize("sparta") == "sparta"
        
        # already singularized
        # assert FwText.singularize("this") == "this"
        # assert FwText.singularize("is") == "is"


class TestPluralizeTuple():
    def test_blank(self):
        assert FwText.pluralize_tuple(tuple([])) == tuple([])

    def test_last_word_default(self):
        result = FwText.pluralize_tuple(("this", "test"))
        assert result == ("this", "tests")

    def test_all_words(self):
        result = FwText.pluralize_tuple(("this", "test"), b_last_word_only=False)
        assert result == ("these", "tests")


class TestSingularizeTuple():
    def test_blank(self):
        assert FwText.singularize_tuple(tuple([])) == tuple([])

    def test_last_word_default(self):
        result = FwText.singularize_tuple(("these", "tests"))
        assert result == ("these", "test")

    def test_all_words(self):
        result = FwText.singularize_tuple(("these", "tests"), b_last_word_only=False)
        assert result == ("this", "test")


class TestGetPhrasePosition():
    def test_default_settings(self):
        result = FwText.get_phrase_position("This is Sparta", "is Sparta")
        assert result == (1, 3)

    def test_case_insensitivity(self):
        result = FwText.get_phrase_position(
            "This is Sparta", "is Sparta",
            b_case_insensitive=True
        )
        assert result == (1, 3)

    def test_punctuation_insensiive(self):
        result = FwText.get_phrase_position(
            "This is Sparta!", "Sparta",
            b_punctuation_insensitive=True
        )
        assert result == (2, 3)

    def test_from_beginning(self):
        # Not sure if this feature is working properly
        pass


class TestSplitOnEdgePunctuation():
    def test_default_settings(self):
        result = FwText.split_on_edge_punctuation([
            "test/", ",this-", "out"
        ])
        assert result == ["test", "/", ",", "this", "-","out"]

    def test_do_not_split_if_not_edge(self):
        result = FwText.split_on_edge_punctuation([
            "back-end", "front-end"
        ])
        assert result == ["back-end", "front-end"]


class TestGetQuotedPhrases():
    def test_blank_tokenized(self):
        result = FwText.get_quoted_phrases([])
        assert result == []
    
    def test_blank_string(self):
        result = FwText.get_quoted_phrases('')
        assert result == []
    
    def test_tokenized(self):
        text = ['This', 'is', 'a', '"EMPIRE"', 'T-shirt', 'with', '8"', '"DARTH', 'VADER', '"']
        result = FwText.get_quoted_phrases(text)
        assert result == ['EMPIRE', 'DARTH VADER']
    
    def test_string(self):
        text = 'This is a "STAR WARS" T-shirt with 8" "DARTH VADER "'
        result = FwText.get_quoted_phrases(text)
        assert result == ['STAR WARS', 'DARTH VADER']


class TestGetQuotedPhrases():
    def test_blank_tokenized(self):
        result = FwText.get_quoted_phrase_words([])
        assert result == set()

    def test_blank_string(self):
        result = FwText.get_quoted_phrase_words('')
        assert result == set()
        
    def test_tokenized(self):
        text = ['This', 'is', 'a', '"STAR', 'WARS"', 'T-shirt', 'with', '8"', '"DARTH', 'VADER', '"']
        result = FwText.get_quoted_phrase_words(text)
        assert result == {'DARTH', 'STAR', 'VADER', 'WARS'}

    def test_string(self):
        text = 'This is a "STAR WARS" T-shirt with 8" "DARTH VADER "'
        result = FwText.get_quoted_phrase_words(text)
        assert result == {'DARTH', 'STAR', 'VADER', 'WARS'}


class TestIsCompoundWord():
    ngrams = {
        ("singular", ): 1,
        ("compoundword", ): 1,
        ("compound", "word"): 1,
        ("compound", ): 1,
        ("word"): 1,
        ("asits"): 1,
        ("as"): 1,
        ("its"): 1,
    }
    
    def test_blank(self):
        assert FwText.is_compound_word('', self.ngrams) == False

    def test_plain(self):
        result = FwText.is_compound_word('singular', self.ngrams)
        assert result == False

    def test_compound(self):
        result = FwText.is_compound_word('compoundword', self.ngrams)
        assert result == True

    def test_short(self):
        result = FwText.is_compound_word('asits', self.ngrams, i_start=3)
        assert result == False

    def test_short_override(self):
        result = FwText.is_compound_word('asits', self.ngrams, i_start=1)
        assert result == True


class TestGetCompounds():
    ngrams = {
        ("singular", ): 1,
        ("compoundword", ): 1,
        ("compound", "word"): 1,
        ("compound", ): 1,
        ("word"): 1,
        ("asits"): 1,
        ("as"): 1,
        ("its"): 1,
    }

    def test_blank(self):
        assert FwText.get_compounds({}, e_ngrams=self.ngrams) == {}

    def test_compounds(self):
        result = FwText.get_compounds(self.ngrams)
        assert result == {
            ("compoundword", ): ("compound", "word"),
        }
        

class TestFreqDictToLower():
    def test_default_settings(self):
        # Use FwText.Fwtext because this function is not exposed
        # in the external FwText
        result = FwText.FwText.freq_dict_to_lower({"This":1, "is":1, "SPARTA":1})
        # Not sure if this is the intended behavior
        assert result["this"] == 1
        assert result["This"] == 1
        assert result["is"] == 2
        assert result["sparta"] == 1
        assert result["SPARTA"] == 1 