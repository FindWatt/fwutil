﻿import pytest
from FwUtil.FwText import dimension_patterns

dim_tok = dimension_patterns.DimensionsTokenizer()

class TestTokenizeDimensions():
    # Including regex level tests would be the optimal way to go
    # http://stackoverflow.com/questions/488601/how-do-you-unit-test-regular-expressions
    # For now I'll include tests to check that the module is not broken
    def test_default_behavior(self):
        result = dim_tok.tokenize_dimensions(
            "jars of pickles, packs of 6 jars. Only high quality 3 cm pickles."
        )
        exp_result = "jars of pickles, packs_of_6 jars. Only high quality 3_cm pickles."
        assert result[0] == exp_result
        assert isinstance(result[1], list)

        assert result[1][0].pattern_type == "pack"
        assert result[1][0].string_match == "packs of 6"
        assert result[1][0].position_start == 17
        assert result[1][0].position_end == 27

        assert result[1][1].pattern_type == "meters"
        assert result[1][1].string_match == "3 cm"
        assert result[1][1].position_start == 52
        assert result[1][1].position_end == 56


class TestCompilePatterns():
    def test_compiles_elements_in_list(self):
        compiled = dim_tok._compile_patterns(
            ["pattern1", "pattern2", "pattern3"]
        )
        assert len(compiled) == 3

    def test_returns_patternslist_object(self):
        compiled = dim_tok._compile_patterns(
            ["pattern1", "pattern2", "pattern3"]
        )
        assert isinstance(compiled, dimension_patterns.PatternsList)

    def test_ignores_case(self):
        compiled = dim_tok._compile_patterns(["sparta"])
        assert compiled[0].search("Sparta")


class TestTokenizePatterns():
    def test_finds_patterns(self):
        doc, matches = dim_tok._tokenize_patterns(
            "this is Sparta", dim_tok._compile_patterns(["this is"])
        )
        assert len(matches) == 1
        assert matches[0].position_start == 0
        assert matches[0].position_end == 7

    def test_returns_tokenizationresult_object(self):
        doc, matches = dim_tok._tokenize_patterns(
            "this is Sparta", dim_tok._compile_patterns(["this is"])
        )
        assert all(
            isinstance(match, dimension_patterns.TokenizationResult)
            for match in matches
        )

    def test_returns_modified_document(self):
        doc, matches = dim_tok._tokenize_patterns(
            "this is Sparta", dim_tok._compile_patterns(["this is"])
        )
        assert doc == "this_is Sparta"



class TestInchesPatterns():
    def test_inches_patterns_1(self):
        pattern = dim_tok.inches_patterns[0]
        assert pattern.search("10-12-90 inch")
        assert pattern.search("10/12/90 inch")
        assert pattern.search("10X12X90 inch")
        assert pattern.search("10x12x90 inch")
        assert pattern.search("10~12~90 inch")
        assert pattern.search("100-120-900 inch")
        assert pattern.search("1.10-1.12-9.90 inch")
        assert pattern.search("10-12-90 inches")
        assert pattern.search("10-12-90inch")
        assert pattern.search("10inch-12inch-90inch")
        assert pattern.search("10inches-12inches-90inches")
        assert pattern.search("10 - 12 - 90 inch")
        assert pattern.search("10 - 12 - 90 (inch)")
        assert pattern.search("10 - 12 - 90 (inches)")
        
        
    def test_inches_patterns_2(self):
        pattern = dim_tok.inches_patterns[1]
        assert pattern.search("10-12-90in")
        assert pattern.search("10/12/90in")
        assert pattern.search("10X12X90in")
        assert pattern.search("10x12x90in")
        assert pattern.search("10~12~90in")
        assert pattern.search("100-120-900in")
        assert pattern.search("1.10-1.12-9.90in")
        assert pattern.search("10-12-90 in")
        assert pattern.search("10-12-90in.")
        assert pattern.search('10-12-90"')
        assert pattern.search('10"-12"-90"')
        assert pattern.search("10in.-12in.-90in")
        assert pattern.search("10 - 12 - 90in")
        assert pattern.search("10in. - 12in. - 90in.")
        assert pattern.search("10-12-90(in)")

    def test_inches_patterns_3(self):
        pattern = dim_tok.inches_patterns[2]
        assert pattern.search("1/2-1/4-1/6inch")
        assert pattern.search("1/2X1/4X1/6inch")
        assert pattern.search("1/2x1/4x1/6inch")
        assert pattern.search("1/2~1/4~1/6inch")
        assert pattern.search("1/2 - 1/4 - 1/6inch")
        assert pattern.search("1 1/2 - 1 1/4 - 1 1/6inch")
        assert pattern.search("1-1/2 - 1-1/4 - 1-1/6inch")
        assert pattern.search("11 1/2 x 11 1/4 x 12 1/6 inch")
        assert pattern.search("1/2inches-1/4inches-1/6inches")
        assert pattern.search("1/2-1/4-1/6(inch)")
        assert pattern.search("1/2-1/4-1/6(inches)")

    def test_inches_patterns_4(self):
        pattern = dim_tok.inches_patterns[3]
        assert pattern.search('1/2-1/4-1/6in')
        assert pattern.search('1/2-1/4-1/6in.')
        assert pattern.search('1/2-1/4-1/6"')
        assert pattern.search('1/2x1/4x1/6in')
        assert pattern.search('1/2X1/4X1/6in')
        assert pattern.search('1/2~1/4~1/6in')
        assert pattern.search('1/2 - 1/4 - 1/6in')
        assert pattern.search('2 1/2 - 2 1/4 - 3 1/6in')
        assert pattern.search('2-1/2 - 2-1/4 - 3-1/6in')
        assert pattern.search('2 1/2 x 3 1/4 x 2 1/6 in')
        assert pattern.search('1/2in. - 1/4in. - 1/6in.')
        assert pattern.search('1/2-1/4-1/6(in)')

    def test_inches_patterns_5(self):
        # I think pattern #3 covers everything that
        # pattern #5 would match
        pass

    def test_inches_patterns_6(self):
        # I think pattern #4 covers everything that
        # pattern #6 would match
        pass

    def test_inches_patterns_7(self):
        # I think pattern #1 covers everything that
        # pattern #7 would match
        pass

    def test_inches_patterns_8(self):
        # I think pattern #2 covers everything that
        # pattern #8 would match
        pass

    def test_inches_patterns_9(self):
        # I think pattern #1 can be tweaked to
        # cover everything pattern #9 tries to match
        pattern = dim_tok.inches_patterns[8]
        assert pattern.search("10-12 inch")
        assert pattern.search("10/12 inch")
        assert pattern.search("10X12 inch")
        assert pattern.search("10x12 inch")
        assert pattern.search("10~12 inch")
        assert pattern.search("100-120 inch")
        assert pattern.search("1.10-1.12 inch")
        assert pattern.search("10-12 inches")
        assert pattern.search("10-12-inch")
        assert pattern.search("10inch-12inch")
        assert pattern.search("10inches-12inches")
        assert pattern.search("10 - 12 inch")
        assert pattern.search("10 - 12 (inch)")
        assert pattern.search("10 - 12 (inches)")

    def test_inches_patterns_10(self):
        # I think pattern #2 can be tweaked to cover
        # everything pattern #10 tries to match
        pattern = dim_tok.inches_patterns[9]
        assert pattern.search("10-12in")
        assert pattern.search("10/12in")
        assert pattern.search("10X12in")
        assert pattern.search("10x12in")
        assert pattern.search("10~12in")
        assert pattern.search("100-120in")
        assert pattern.search("1.10-1.12in")
        assert pattern.search("10-12 in")
        assert pattern.search("10-12in.")
        assert pattern.search('10-12"')
        assert pattern.search('10"-12"')
        assert pattern.search("10in.-12in.")
        assert pattern.search("10 - 12in")
        assert pattern.search("10in. - 12in.")
        assert pattern.search("10-12(in)")

    def test_inches_patterns_11(self):
        # I think pattern #3 can be tweaked to cover
        # everything pattern #11 tries to match
        pattern = dim_tok.inches_patterns[10]
        assert pattern.search("1/2-1/4inch")
        assert pattern.search("1/2X1/4inch")
        assert pattern.search("1/2x1/4inch")
        assert pattern.search("1/2~1/4inch")
        assert pattern.search("1/2 - 1/4inch")
        assert pattern.search("1 1/2 - 1 1/4inch")
        assert pattern.search("1-1/2 - 1-1/4inch")
        assert pattern.search("11 1/2 x 11 1/4inch")
        assert pattern.search("1/2inches-1/4inches")
        assert pattern.search("1/2-1/4(inch)")
        assert pattern.search("1/2-1/4(inches)")

    def test_inches_patterns_12(self):
        # I think pattern #4 can be tweaked to cover
        # everything pattern #12 tries to match
        pattern = dim_tok.inches_patterns[11]
        assert pattern.search('1/2-1/4in')
        assert pattern.search('1/2-1/4in.')
        assert pattern.search('1/2-1/4"')
        assert pattern.search('1/2x1/4in')
        assert pattern.search('1/2X1/4in')
        assert pattern.search('1/2~1/4in')
        assert pattern.search('1/2 - 1/4in')
        assert pattern.search('2 1/2 - 2 1/4in')
        assert pattern.search('2-1/2 - 2-1/4in')
        assert pattern.search('2 1/2 x 3 1/4in')
        assert pattern.search('1/2in. - 1/4in.')
        assert pattern.search('1/2-1/4(in)')

    def test_inches_patterns_13(self):
        # I think pattern #11 covers everything that
        # pattern #13 tries to match
        pass

    def test_inches_patterns_14(self):
        # I think pattern #12 covers everything that
        # pattern #14 tries to match
        pass

    def test_inches_patterns_15(self):
        # I think pattern #9 cover everything that
        #pattern #15 tries to match
        pass

    def test_inches_patterns_16(self):
        # I think pattern #10 cover everything that
        #pattern #16 tries to match
        pass
    
    def test_inches_pattern_17(self):
        # I think pattern #1 can be tweaked to
        # cover everything pattern #17 tries to match
        pattern = dim_tok.inches_patterns[16]
        assert pattern.search("1.12 inch")
        assert pattern.search("1.12inch")
        assert pattern.search("1.12-inch")
        assert pattern.search("12.2 inches")
        assert pattern.search("12.2-inch")
        assert pattern.search("12.2inch")
        assert pattern.search("12.2(inch)")
        assert pattern.search("12.2 (inches)")

    def test_inches_pattern_18(self):
        # I think pattern #2 can be tweaked to
        # cover everything pattern #18 tries to match
        pattern = dim_tok.inches_patterns[17]
        assert not pattern.search("1.0 in configuration")
        assert pattern.search("1.10in")
        assert pattern.search("1.10 in")
        assert pattern.search("1.10-in")
        assert pattern.search("1.10in.")
        assert pattern.search('10.5"')
        assert pattern.search("10.4(in)")

    def test_inches_pattern_19(self):
        # I think pattern #3 can be tweaked to cover
        # everything pattern #19 tries to match
        pattern = dim_tok.inches_patterns[18]
        assert pattern.search("1 1/2inch")
        assert pattern.search("1-1/2 inch")
        assert pattern.search("11 1/2-inch")
        assert pattern.search("1 1/2inches")
        assert pattern.search("1-1/2 inches")
        assert pattern.search("11 1/2-inches")
        assert pattern.search("4 1/2(inch)")
        assert pattern.search("2 1/2(inches)")

    def test_inches_pattern_20(self):
        # I think pattern #4 can be tweaked to cover
        # everything pattern #20 tries to match
        pattern = dim_tok.inches_patterns[19]
        assert not pattern.search("1 1/2 in example")
        assert pattern.search("1 1/2in")
        assert pattern.search("1-1/2 in")
        assert pattern.search("11 1/2-in")
        assert pattern.search("1 1/2in.")
        assert pattern.search("1-1/2 in.")
        assert pattern.search("11 1/2-in.")
        assert pattern.search("4 1/2(in)")
    
    def test_inches_pattern_21(self):
        # I think pattern #1 can be tweaked to cover
        # everything pattern #21 tries to match
        pattern = dim_tok.inches_patterns[20]
        assert pattern.search("1/2inch")
        assert pattern.search("1/2 inch")
        assert pattern.search("1/2-inch")
        assert pattern.search("1/2inches")
        assert pattern.search("1/2 inches")
        assert pattern.search("1/2-inches")
        assert pattern.search("1/2(inch)")
        assert pattern.search("1/2(inches)")

    def test_inches_pattern_22(self):
        # I think pattern #2 can be tweaked to cover
        # everything pattern #22 tries to match
        pattern = dim_tok.inches_patterns[21]
        assert not pattern.search("1/2 in profile")
        assert pattern.search("1/2in")
        assert pattern.search("1/2 in")
        assert pattern.search("1/2-in")
        assert pattern.search("1/2in.")
        assert pattern.search("1/2 in.")
        assert pattern.search("1/2-in.")
        assert pattern.search("1/2(in)")

    def test_inches_pattern_23(self):
        # I think pattern #1 can be tweaked to cover
        # everything pattern #23 tries to match
        pattern = dim_tok.inches_patterns[22]
        assert pattern.search("1inch")
        assert pattern.search("1 inch")
        assert pattern.search("1-inch")
        assert pattern.search("1inches")
        assert pattern.search("1 inches")
        assert pattern.search("1-inches")
        assert pattern.search("1(inch)")
        assert pattern.search("1(inches)")

    def test_inches_pattern_24(self):
        # I think pattern #2 can be tweaked to cover
        # everything pattern #24 tries to match
        pattern = dim_tok.inches_patterns[23]
        assert not pattern.search("1 in a million")
        assert pattern.search("1in")
        assert pattern.search("1 in")
        assert pattern.search("1-in")
        assert pattern.search("1in.")
        assert pattern.search("1 in.")
        assert pattern.search("1-in.")
        assert pattern.search("1(in)")


class TestFeetPatterns():
    def test_feet_patterns_25(self):
        pattern = dim_tok.feet_patterns[0]
        assert pattern.search("1-2-3feet")
        assert pattern.search("10-12-90feet")
        assert pattern.search("10.12-12.12-90.12feet")
        assert pattern.search("2feet-3feet-4feet")
        assert pattern.search("2feet - 3feet - 4feet")
        assert pattern.search("2feet/3feet/4feet")
        assert pattern.search("2feetx3feetx4feet")
        assert pattern.search("2feetX3feetX4feet")
        assert pattern.search("2feet~3feet~4feet")
        assert pattern.search("2FEET-3FEET-4FEET")
        assert pattern.search("1-1.5-1.8foot")
        assert pattern.search("1x2x3 feet")
        assert pattern.search("1-2-3(feet)")


    def test_feet_patterns_26(self):
        # feet pattern 25 could be tweaked to cover everything
        # feet pattern 26 is trying to match
        pattern = dim_tok.feet_patterns[1]
        assert pattern.search("1-2-3ft")
        assert pattern.search("10-12-90ft")
        assert pattern.search("10.12-12.12-90.12ft")
        assert pattern.search("2ft-3ft-4ft")
        assert pattern.search("2ft - 3ft - 4ft")
        assert pattern.search("2ft/3ft/4ft")
        assert pattern.search("2ftx3ftx4ft")
        assert pattern.search("2ftX3ftX4ft")
        assert pattern.search("2ft~3ft~4ft")
        assert pattern.search("2ft.~3ft.~4ft.")
        assert pattern.search("2FT-3FT-4FT")
        assert pattern.search("2'-3'-4'")
        assert pattern.search("1-1.5-1.8'")
        assert pattern.search("1x2x3 ft")
        assert pattern.search("1-2-3(ft)")


    def test_feet_patterns_27(self):
        pattern = dim_tok.feet_patterns[2]
        assert pattern.search("1/2-2/3-3/4feet")
        assert pattern.search("10/12-20/12-13/12feet")
        assert pattern.search("2 10/12 - 2 20/12 - 2 13/12feet")
        assert pattern.search("10/12feet-20/12feet-13/12feet")
        assert pattern.search("10/12 - 20/12 - 13/12feet")
        assert pattern.search("10/12x20/12x13/12feet")
        assert pattern.search("10/12X20/12X13/12feet")
        assert pattern.search("10/12~20/12~13/12feet")
        assert pattern.search("10/12~20/12~13/12FEET")
        assert pattern.search("10/12~20/12~13/12foot")
        assert pattern.search("10/12~20/12~13/12 feet")
        assert pattern.search("10/12~20/12~13/12(feet)")


    def test_feet_pattern_28(self):
        pattern = dim_tok.feet_patterns[3]
        assert pattern.search("1/2-2/3-3/4ft")
        assert pattern.search("10/12-20/12-13/12ft")
        assert pattern.search("2 10/12 - 2 20/12 - 2 13/12ft")
        assert pattern.search("10/12ft-20/12ft-13/12ft")
        assert pattern.search("10/12 - 20/12 - 13/12ft")
        assert pattern.search("10/12x20/12x13/12ft")
        assert pattern.search("10/12X20/12X13/12ft")
        assert pattern.search("10/12~20/12~13/12ft")
        assert pattern.search("10/12~20/12~13/12FT")
        assert pattern.search("10/12ft.~20/12ft.~13/12ft.")
        assert pattern.search("10/12'~20/12'~13/12'")
        assert pattern.search("10/12~20/12~13/12 ft")
        assert pattern.search("10/12~20/12~13/12(ft)")


    def test_feet_pattern_29(self):
        # I think pattern 27 covers everything this
        # pattern is trying to match
        pass


    def test_feet_pattern_30(self):
        # I think pattern 28 covers everything this
        # pattern is trying to match
        pass


    def test_feet_pattern_31(self):
        # I think pattern 25 could be tweaked to cover
        # to cover everything this pattern tries to match.
        pattern = dim_tok.feet_patterns[6]
        assert pattern.search("1-2-3feet")
        assert pattern.search("10-12-90feet")
        assert pattern.search("2feet-3feet-4feet")
        assert pattern.search("2feet - 3feet - 4feet")
        assert pattern.search("2feetx3feetx4feet")
        assert pattern.search("2feetX3feetX4feet")
        assert pattern.search("2feet~3feet~4feet")
        assert pattern.search("2FEET-3FEET-4FEET")
        assert pattern.search("1-2-3foot")
        assert pattern.search("1x2x3 feet")
        assert pattern.search("1-2-3(feet)")

    def test_feet_pattern_32(self):
        # I think pattern 26 coulod be tweaked to cover
        # everything this pattern is trying to match
        pattern = dim_tok.feet_patterns[7]
        assert pattern.search("1-2-3ft")
        assert pattern.search("10-12-90ft")
        assert pattern.search("10-12-90ft")
        assert pattern.search("2ft-3ft-4ft")
        assert pattern.search("2ft - 3ft - 4ft")
        assert pattern.search("2ftx3ftx4ft")
        assert pattern.search("2ftX3ftX4ft")
        assert pattern.search("2ft~3ft~4ft")
        assert pattern.search("2ft.~3ft.~4ft.")
        assert pattern.search("2FT-3FT-4FT")
        assert pattern.search("2'-3'-4'")
        assert pattern.search("1-2-3'")
        assert pattern.search("1x2x3 ft")
        assert pattern.search("1-2-3(ft)")

    def test_feet_pattern_33(self):
        # I think pattern 25 could be tweaked to cover
        # everythin this pattern is trying to match.
        pattern = dim_tok.feet_patterns[8]
        assert pattern.search("1-2feet")
        assert pattern.search("10-12feet")
        assert pattern.search("10.12-12.12feet")
        assert pattern.search("2feet-3feet")
        assert pattern.search("2feet - 3feet")
        assert pattern.search("2feet/3feet")
        assert pattern.search("2feetx3feet")
        assert pattern.search("2feetX3feet")
        assert pattern.search("2feet~3feet")
        assert pattern.search("2FEET-3FEET")
        assert pattern.search("1-1.5foot")
        assert pattern.search("1x2 feet")
        assert pattern.search("1-2(feet)")


    def test_feet_pattern_34(self):
        # I think pattern 26 could be tweaked to cover
        # everthing this pattern is trying to match
        pattern = dim_tok.feet_patterns[9]
        assert pattern.search("1-2ft")
        assert pattern.search("10-12ft")
        assert pattern.search("10.12-12.12ft")
        assert pattern.search("2ft-3ft")
        assert pattern.search("2ft - 3ft")
        assert pattern.search("2ft/3ft")
        assert pattern.search("2ftx3ft")
        assert pattern.search("2ftX3ft")
        assert pattern.search("2ft~3ft")
        assert pattern.search("2ft.~3ft.")
        assert pattern.search("2FT-3FT")
        assert pattern.search("2'-3'-4'")
        assert pattern.search("1-1.5'")
        assert pattern.search("1x2 ft")
        assert pattern.search("1-2(ft)")


    def test_feet_pattern_35(self):
        # I think pattern 27 can be tweaked to cover
        # everything this pattern is trying to match.
        pattern = dim_tok.feet_patterns[10]
        assert pattern.search("2/3-3/4feet")
        assert pattern.search("20/12-13/12feet")
        assert pattern.search("2 20/12 - 2 13/12feet")
        assert pattern.search("20/12feet-13/12feet")
        assert pattern.search("20/12 - 13/12feet")
        assert pattern.search("20/12x13/12feet")
        assert pattern.search("20/12X13/12feet")
        assert pattern.search("20/12~13/12feet")
        assert pattern.search("20/12~13/12FEET")
        assert pattern.search("20/12~13/12foot")
        assert pattern.search("20/12~13/12 feet")
        assert pattern.search("20/12~13/12(feet)")


    def test_feet_pattern_36(self):
        # I think pattern 28 can be tweaked to cover
        # everything this pattern is trying to match.
        pattern = dim_tok.feet_patterns[11]
        assert pattern.search("2/3-3/4ft")
        assert pattern.search("20/12-13/12ft")
        assert pattern.search("2 20/12 - 2 13/12ft")
        assert pattern.search("20/12ft-13/12ft")
        assert pattern.search("20/12 - 13/12ft")
        assert pattern.search("20/12x13/12ft")
        assert pattern.search("20/12X13/12ft")
        assert pattern.search("20/12~13/12ft")
        assert pattern.search("20/12~13/12FT")
        assert pattern.search("20/12ft.~13/12ft.")
        assert pattern.search("20/12'~13/12'")
        assert pattern.search("20/12~13/12 ft")
        assert pattern.search("20/12~13/12(ft)")


    def test_feet_pattern_37(self):
        # I think pattern 35 covers everything
        # pattern 37 is trying to match.
        pass


    def test_feet_pattern_38(self):
        # I think pattern 36 covers everything
        # pattern 38 is trying to match.
        pass


    def test_feet_pattern_39(feet):
        # I think pattern 25 could be tweaked to
        # cover everything pattern 39 tries to match.
        pattern = dim_tok.feet_patterns[14]
        assert pattern.search("1-2feet")
        assert pattern.search("10-12feet")
        assert pattern.search("2feet-3feet")
        assert pattern.search("2feet - 3feet")
        assert pattern.search("2feetx3feet")
        assert pattern.search("2feetX3feet")
        assert pattern.search("2feet~3feet")
        assert pattern.search("2FEET-3FEET")
        assert pattern.search("1-1foot")
        assert pattern.search("1x2 feet")
        assert pattern.search("1-2(feet)")


    def test_feet_pattern_40(self):
        # I think pattern 26 could be tweaked to
        # cover everything pattern 40 tries to match.
        pattern = dim_tok.feet_patterns[15]
        assert pattern.search("1-2ft")
        assert pattern.search("10-12ft")
        assert pattern.search("2ft.-3ft.")
        assert pattern.search("2ft - 3ft")
        assert pattern.search("2ftx3ft")
        assert pattern.search("2ftX3ft")
        assert pattern.search("2ft~3ft")
        assert pattern.search("2FT-3FT")
        assert pattern.search("1-1ft.")
        assert pattern.search("1x2 ft")
        assert pattern.search("1-2(ft)")


    def test_feet_pattern_41(self):
        # I think pattern 25 can be tweaked to cover
        # everything pattern 41 tries to match
        pattern = dim_tok.feet_patterns[16]
        assert pattern.search("2.1feet")
        assert pattern.search("12.12feet")
        assert pattern.search("3.1feet")
        assert pattern.search("3.3FEET")
        assert pattern.search("1.9-foot")
        assert pattern.search("1.4-2.2(feet)")


    def test_feet_pattern_42(self):
        # I think patter 26 could be tweaked to cover
        # everything pattern 42 is trying to match.
        pattern = dim_tok.feet_patterns[17]
        assert pattern.search("2.1ft")
        assert pattern.search("12.12ft")
        assert pattern.search("3.1ft.")
        assert pattern.search("3.3FT")
        assert pattern.search("2.2 ft")
        assert pattern.search("2.2-ft")
        assert pattern.search("2.2(ft)")


    def test_feet_pattern_43(self):
        # I think pattern 25 could be tweaked to cover
        # everything pattern 43 is trying to match.
        pattern = dim_tok.feet_patterns[18]
        assert pattern.search("1 2/3feet")
        assert pattern.search("1-1/2feet")
        assert pattern.search("1  2/3feet")
        assert pattern.search("3 1/3FEET")
        assert pattern.search("1 4/9-foot")
        assert pattern.search("1 1/2(feet)")


    def test_feet_pattern_44(self):
        # I think pattern 26 could be tweaked to cover
        # everything pattern 44 is trying to match
        pattern = dim_tok.feet_patterns[19]
        assert pattern.search("1 2/3ft")
        assert pattern.search("1-1/2ft")
        assert pattern.search("1  2/3ft")
        assert pattern.search("3 1/3FT")
        assert pattern.search("1 4/9ft.")
        assert pattern.search("1 1/2(ft)")


    def test_Feet_pattern_45(self):
        # I think pattern 25 could be tweaked to cover
        # everything pattern 45 is trying to match.
        pattern = dim_tok.feet_patterns[20]
        assert pattern.search("2/3feet")
        assert pattern.search("2/3 feet")
        assert pattern.search("2/3-feet")
        assert pattern.search("1/3FEET")
        assert pattern.search("4/9foot")
        assert pattern.search("1/2(feet)")


    def test_Feet_pattern_46(self):
        # I think pattern 26 could be tweaked to cover
        # everything pattern 46 is trying to match.
        pattern = dim_tok.feet_patterns[21]
        assert pattern.search("2/3ft")
        assert pattern.search("2/3 ft")
        assert pattern.search("2/3-ft")
        assert pattern.search("1/3FT")
        assert pattern.search("4/9ft.")
        assert pattern.search("1/2(ft)")


    def test_Feet_pattern_47(self):
        # I think pattern 25 could be tweaked to cover
        # everything pattern 47 is trying to match.
        pattern = dim_tok.feet_patterns[22]
        assert pattern.search("3feet")
        assert pattern.search("3 feet")
        assert pattern.search("3-feet")
        assert pattern.search("3FEET")
        assert pattern.search("1foot")
        assert pattern.search("2(feet)")


    def test_Feet_pattern_48(self):
        # I think pattern 26 could be tweaked to cover
        # everything pattern 48 is trying to match.
        pattern = dim_tok.feet_patterns[23]
        assert pattern.search("3ft")
        assert pattern.search("3 ft")
        assert pattern.search("3-ft")
        assert pattern.search("3FT")
        assert pattern.search("1ft.")
        assert pattern.search("2(ft)")


class TestAmpsPatterns():
    def test_amps_pattern_121(self):
        # Probably requires adding optional spaces
        pattern = dim_tok.amps_patterns[0]
        assert pattern.search("2-2a")
        assert pattern.search("10-12a")
        assert pattern.search("10.5-12.5a")
        assert pattern.search("10/12a")
        assert pattern.search("10~12a")
        assert pattern.search("10~12 a")
        assert pattern.search("10~12-a")
        assert pattern.search("10~12-amps")
        assert pattern.search("10~12-a.")

    def test_amps_pattern_122(self):
        # I think pattern 121 covers everything patter 122
        # is trying to match
        pass

    def test_amps_pattern_123(self):
        # I think pattern 121 can be tweaked to cover everything
        # pattern 123 is trying to match
        pattern = dim_tok.amps_patterns[2]
        assert pattern.search("12.5a")
        assert pattern.search("12.55a")
        assert pattern.search("12.5 a")
        assert pattern.search("12.5-a")
        assert pattern.search("12.5-amps")
        assert pattern.search("12.5-a.")

    def test_amps_pattern_124(self):
        # I think pattern 121 can be tweaked to cover everything
        # pattern 124 is trying to match
        pattern = dim_tok.amps_patterns[3]
        assert pattern.search("12a")
        assert pattern.search("12 a")
        assert pattern.search("12-a")
        assert pattern.search("12-amps")
        assert pattern.search("12-a.")

    def test_amps_pattern_125(self):
        # I think pattern 121 can be tweaked to cover everything
        # pattern 125 is trying to match
        pattern = dim_tok.amps_patterns[4]
        assert pattern.search("2-2mAh")
        assert pattern.search("10-12mAh")
        assert pattern.search("10.5-12.5mAh")
        assert pattern.search("10/12mAh")
        assert pattern.search("10~12mAh")
        assert pattern.search("10~12 mAh")
        assert pattern.search("10~12-mAh")
        assert pattern.search("10~12-mAh.")
        assert pattern.search("10~12-MAH")
    
    def test_amps_pattern_126(self):
        pattern = dim_tok.amps_patterns[5]
        assert pattern.search("2,2-2,2mAh")
        assert pattern.search("2,200-2,200mAh")
        assert pattern.search("20,200,200-20,200,200mAh")
        assert pattern.search("2,200/2,200mAh")
        assert pattern.search("2,200~2,200mAh")
        assert pattern.search("2,200-2,200 mAh")
        assert pattern.search("2,200~2,200-mAh")
        assert pattern.search("2,200-2,200mAh.")
        assert pattern.search("2,200-2,200MAH")

    def test_amps_pattern_127(self):
        # I think pattern 121 can be tweaked to cover everything
        # pattern 121 is trying to cover
        pattern = dim_tok.amps_patterns[6]
        assert pattern.search("2.5mAh")
        assert pattern.search("12.5mAh")
        assert pattern.search("12.52mAh")
        assert pattern.search("12.5 mAh")
        assert pattern.search("12.5-mAh")
        assert pattern.search("12.5-mAh.")
        assert pattern.search("12.5-MAH")

    def test_amps_pattern_128(self):
        # I think pattern 126 can be tweaked to cover everything
        # pattern 128 is trying to match
        pattern = dim_tok.amps_patterns[7]
        assert pattern.search("2,2mAh")
        assert pattern.search("2,200mAh")
        assert pattern.search("20,200,200mAh")
        assert pattern.search("2,200 mAh")
        assert pattern.search("2,200-mAh")
        assert pattern.search("2,200mAh.")
        assert pattern.search("2,200MAH")



class TestVoltagePatterns():
    def test_voltage_pattern_0(self):
        # Could use optional spaces. The end is wonky
        pattern = dim_tok.voltage_patterns[0]
        assert pattern.search("2-2volt")
        assert pattern.search("12.10-12.10volt")
        assert pattern.search("2~2volt")
        assert pattern.search("2/2volt")
        assert pattern.search("2-2 volt")
        assert pattern.search("2~2-volt")
        assert pattern.search("2-2 voltage")
        assert pattern.search("2-2 volts")
        assert pattern.search("2-2 volt.")

    def test_voltage_pattern_1(self):
        # I think voltage_patterns[0] covers everything
        # this pattern is trying to match
        pass

    def test_voltage_pattern_2(self):
        # I think voltage_patterns[0] can be tweaked to
        # cover everything this pattern is trying to match
        pattern = dim_tok.voltage_patterns[2]
        assert pattern.search("1.1volt")
        assert pattern.search("12.10volt")
        assert pattern.search("10.12 volt")
        assert pattern.search("10.12-volt")
        assert pattern.search("10.12 voltage")
        assert pattern.search("10.12 volts")
        assert pattern.search("10.12 volt.")

    def test_voltage_pattern_3(self):
        # I think voltage_patterns[0] can be tweaked to
        # cover everything this pattern is trying to match
        pattern = dim_tok.voltage_patterns[3]
        assert pattern.search("1volt")
        assert pattern.search("10volt")
        assert pattern.search("12 volt")
        assert pattern.search("12-volt")
        assert pattern.search("12 voltage")
        assert pattern.search("12 volts")
        assert pattern.search("12 volt.")



class TestWattagePatters():
    def test_wattage_pattern_129(self):
        # Could use optional spaces. Ending is wonky
        pattern = dim_tok.wattage_patterns[0]
        assert pattern.search("2-2w")
        assert pattern.search("10-12w")
        assert pattern.search("10.12-10.12w")
        assert pattern.search("10/12w")
        assert pattern.search("10~12w")
        assert pattern.search("10-12 w")
        assert pattern.search("10~12-w")
        assert pattern.search("10-12watt")
        assert pattern.search("10-12wattage")
        assert pattern.search("10-12watts")
        # this one doesn't look right
        assert pattern.search("10-12wattages")

    def test_wattage_pattern_130(self):
        # I think pattern #129 covers everything pattern 130 is
        # trying to match
        pass

    def test_wattage_pattern_131(self):
        # I think pattern 129 can be tweaked to cover everything
        # pattern 131 is trying to match
        pattern = dim_tok.wattage_patterns[2]
        assert pattern.search("2.1w")
        assert pattern.search("10.12w")
        assert pattern.search("10.12 w")
        assert pattern.search("10.12-w")
        assert pattern.search("10.12watt")
        assert pattern.search("10.12wattage")
        assert pattern.search("10.12watts")
        # this one doesn't look right
        assert pattern.search("10.12wattages")

    def test_wattage_pattern_132(self):
        # I think pattern 129 can be tweaked to cover everything
        # pattern 132 is trying to match
        pattern = dim_tok.wattage_patterns[3]
        assert pattern.search("2w")
        assert pattern.search("12w")
        assert pattern.search("12 w")
        assert pattern.search("12-w")
        assert pattern.search("12watt")
        assert pattern.search("12wattage")
        assert pattern.search("12watts")
        # this one doesn't look right
        assert pattern.search("12wattages")



class TestGaugePatterns():
    def test_gauge_pattern_133(self):
        # Could use optional spaces.
        pattern = dim_tok.gauge_patterns[0]
        assert pattern.search("2-2awg")
        assert pattern.search("10-12awg")
        assert pattern.search("10.12-10.12awg")
        assert pattern.search("10/12awg")
        assert pattern.search("10~12awg")
        assert pattern.search("10-12 awg")
        assert pattern.search("10~12-awg")

    def test_gauge_pattern_134(self):
        # I think pattern 133 covers everything pattern 134
        # is trying to match
        pass

    def test_gauge_pattern_135(self):
        # I think pattern 133 can be tweaked to cover everything
        # pattern 135 is trying to match
        pattern = dim_tok.gauge_patterns[2]
        assert pattern.search("2.2awg")
        assert pattern.search("1.12awg")
        assert pattern.search("10.12awg")
        assert pattern.search("10.12 awg")
        assert pattern.search("10.12-awg")

    def test_gauge_pattern_136(self):
        # I think pattern 133 can be tweaked to cover everything
        # pattern 136 is trying to match
        pattern = dim_tok.gauge_patterns[3]
        assert pattern.search("2awg")
        assert pattern.search("12awg")
        assert pattern.search("12 awg")
        assert pattern.search("12-awg")

    def test_gauge_pattern_137(self):
        # Could use optional spaces.
        pattern = dim_tok.gauge_patterns[4]
        assert pattern.search("2-2gauge")
        assert pattern.search("10-12gauge")
        assert pattern.search("10.12-10.12gauge")
        assert pattern.search("10/12gauge")
        assert pattern.search("10~12gauge")
        assert pattern.search("10-12 gauge")
        assert pattern.search("10~12-gauge")

    def test_gauge_pattern_138(self):
        # I think pattern 137 covers everything pattern
        # 138 is trying to match
        pass

    def test_gauge_pattern_139(self):
        # I think pattern 133 can be tweaked to cover everything
        # pattern 139 is trying to match
        pattern = dim_tok.gauge_patterns[6]
        assert pattern.search("2.2gauge")
        assert pattern.search("1.12gauge")
        assert pattern.search("10.12gauge")
        assert pattern.search("10.12 gauge")
        assert pattern.search("10.12-gauge")

    def test_gauge_pattern_140(self):
        # I think pattern 133 can be tweaked to cover everything
        # pattern 140 is trying to match
        pattern = dim_tok.gauge_patterns[7]
        assert pattern.search("2gauge")
        assert pattern.search("12gauge")
        assert pattern.search("12 gauge")
        assert pattern.search("12-gauge")
        assert pattern.search("2GAUGE")


    
class TestHertzPatterns():
    def test_hertz_patterns_141(self):
        pattern = dim_tok.hertz_patterns[0]
        assert pattern.search("8hertz")
        assert pattern.search("80hertz")
        assert pattern.search("8 hertz")
        assert pattern.search("8-hertz")
        assert pattern.search("8HERTZ")

    def test_hertz_patterns_142(self):
        pattern = dim_tok.hertz_patterns[1]
        assert pattern.search("8hz")
        assert pattern.search("80hz")
        assert pattern.search("8 hz")
        assert pattern.search("8-hz")
        assert pattern.search("8thz")
        assert pattern.search("8ghz")
        assert pattern.search("8mhz")
        assert pattern.search("8khz")
        assert pattern.search("8HZ")


class TestLumensPatterns():
    def test_lumens_patterns_143(self):
        # Optional spaces could be included
        pattern = dim_tok.lumens_patterns[0]
        assert pattern.search("1-2lumens")
        assert pattern.search("1.1-2.2lumens")
        assert pattern.search("1,000.20-2,000.20lumens")
        assert pattern.search("1~2lumens")
        assert pattern.search("1-2 lumens")
        assert pattern.search("1~2-lumens")

    def test_lumens_patterns_144(self):
        # Pattern 143 could be tweaked to cover everythign that
        # pattern 144 is trying to match.
        pattern = dim_tok.lumens_patterns[1]
        assert pattern.search("1-2lms")
        assert pattern.search("1.1-2.2lms")
        assert pattern.search("1,000.20-2,000.20lms")
        assert pattern.search("1~2lms")
        assert pattern.search("1-2 lms")
        assert pattern.search("1~2-lms")

    def test_lumens_patterns_145(self):
        # Pattern 143 could be tweaked to cover everything
        # that pattern 145 is trying to match
        pattern = dim_tok.lumens_patterns[2]
        assert pattern.search("2lumens")
        assert pattern.search("2.2lumens")
        assert pattern.search("2,000.20lumens")
        assert pattern.search("1-2 lumens")
        assert pattern.search("1~2-lumens")

    def test_lumens_patterns_146(self):
        # Pattern 143 could be tweaked to cover everything
        # that pattern 146 is trying to match
        pattern = dim_tok.lumens_patterns[3]
        assert pattern.search("2lms")
        assert pattern.search("2.2lms")
        assert pattern.search("2,000.20lms")
        assert pattern.search("1-2 lms")
        assert pattern.search("1~2-lms")


class TestNumberOfOutletsPatterns():
    # Trailing "s" could be made optional in these patterns
    # And they could all be merged into a single pattern
    def test_number_of_outlets_patterns_147(self):
        pattern = dim_tok.number_of_outlets_patterns[0]
        assert pattern.search("2outlets")
        assert pattern.search("20outlets")
        assert pattern.search("2 outlets")
        assert pattern.search("2-outlets")
        assert pattern.search("2OUTLETS")

    def test_number_of_outlets_patterns_148(self):
        pattern = dim_tok.number_of_outlets_patterns[1]
        assert pattern.search("2gangs")
        assert pattern.search("20gangs")
        assert pattern.search("2 gangs")
        assert pattern.search("2-gangs")
        assert pattern.search("2GANGS")

    def test_number_of_outlets_patterns_149(self):
        pattern = dim_tok.number_of_outlets_patterns[2]
        assert pattern.search("2ports")
        assert pattern.search("20ports")
        assert pattern.search("2 ports")
        assert pattern.search("2-ports")
        assert pattern.search("2PORTS")

    def test_number_of_outlets_patterns_150(self):
        pattern = dim_tok.number_of_outlets_patterns[3]
        assert pattern.search("2bays")
        assert pattern.search("20bays")
        assert pattern.search("2 bays")
        assert pattern.search("2-bays")
        assert pattern.search("2BAYS")



class TestPackPatterns():
    def test_pack_pattern_151(self):
        pattern = dim_tok.pack_patterns[0]
        assert pattern.search("2pcs/pack")
        assert pattern.search("20pcs/pack")
        assert pattern.search("2 pcs/pack")
        assert pattern.search("2-pcs/pack")
        assert pattern.search("2pcs./pack")
        assert pattern.search("2  pcs/pack")
        assert pattern.search("(2pcs/pack)")

    def test_pack_pattern_152_160_161(self):
        pattern = dim_tok.pack_patterns[1]
        assert pattern.search("2pc")
        assert pattern.search("20pc")
        assert pattern.search("2 pc")
        assert pattern.search("2-pc")
        assert pattern.search("2  pc")
        assert pattern.search("(2pc)")
        assert pattern.search("2pcs")
        assert pattern.search("2pc.")
        assert pattern.search("2pcs.")
        assert pattern.search("2pack")
        assert pattern.search("2pk")
        assert pattern.search("2piece")
        assert pattern.search("2can")
        assert pattern.search("2count")
        assert pattern.search("2ct")

    def test_pack_pattern_153_154(self):
        pattern = dim_tok.pack_patterns[2]
        assert pattern.search("twopack")
        assert pattern.search("two pack")
        assert pattern.search("two  pack")
        assert pattern.search("two-pack")
        assert pattern.search("twopacks")
        assert pattern.search("(twopack)")
        assert pattern.search("threepack")
        assert pattern.search("fourpack")
        assert pattern.search("sixpack")
        assert pattern.search("eightpack")
        assert pattern.search("twelvepack")

    def test_pack_pattern_156(self):
        pattern = dim_tok.pack_patterns[3]
        assert pattern.search("2piece bag")
        assert pattern.search("20pieces bag")
        assert pattern.search("2 piece bag")
        assert pattern.search("2-piece bag")
        assert pattern.search("2piece/bag")
        assert pattern.search("2pieces bag")
        assert pattern.search("(2piece bag)")

    def test_pack_pattern_157(self):
        pattern = dim_tok.pack_patterns[4]
        assert pattern.search("2pc bag")
        assert pattern.search("20pc bag")
        assert pattern.search("2 pc bag")
        assert pattern.search("2-pc bag")
        assert pattern.search("2pc/bag")
        assert pattern.search("2pcs bag")
        assert pattern.search("2pcs. bag")
        assert pattern.search("(2pc bag)")

    def test_pack_pattern_158(self):
        # Duplicate of 156
        pass

    def test_pack_pattern_159(self):
        # Duplicate of 157
        pass

    def test_pack_patterns_8th(self):
        pattern = dim_tok.pack_patterns[7]
        assert pattern.search("pack of 2")
        assert pattern.search("pack of 20")
        assert pattern.search("packs of 2")
        assert pattern.search("set of 2")
        assert pattern.search("sets of 2")
        assert pattern.search("pack-of-2")
        assert pattern.search("PACK OF 2")

    def test_pack_patterns_9th(self):
        # Can probably be merged with pack_patterns[7]
        pattern = dim_tok.pack_patterns[8]
        assert pattern.search("pack of two")
        assert pattern.search("packs of two")
        assert pattern.search("set of two")
        assert pattern.search("sets of two")
        assert pattern.search("pack-of-two")
        assert pattern.search("pack of three")
        assert pattern.search("pack of four")
        assert pattern.search("pack of six")
        assert pattern.search("pack of eight")
        assert pattern.search("pack of twelve")
        assert pattern.search("PACK OF TWO")
        

class TestBytesPatterns():
    def test_bytes_patterns_1st(self):
        pattern = dim_tok.bytes_patterns[0]
        assert pattern.search("2b")
        assert pattern.search("200b")
        assert pattern.search("2.2b")
        assert pattern.search("2,000.05b")
        assert pattern.search("2-b")
        assert pattern.search("2 b")
        assert pattern.search("2-2b")
        assert pattern.search("2,000-4,000b")
        assert pattern.search("2.2-4.4b")
        assert pattern.search("1,024 2,048b")
        assert pattern.search("2byte")
        assert pattern.search("2bytes")
        assert pattern.search("2kb")
        assert pattern.search("2mb")
        assert pattern.search("2gb")
        assert pattern.search("2tb")
        assert pattern.search("2tbs")
        assert pattern.search("2TBS")



class TestWeightPatterns():
    def test_weight_patterns_1st(self):
        pattern = dim_tok.weight_patterns[0]
        assert pattern.search("2lb")
        assert pattern.search("20lb")
        assert pattern.search("2.2lb")
        assert pattern.search("2,000.15lb")
        assert pattern.search("2-lb")
        assert pattern.search("2 lb")
        assert pattern.search("2-4lb")
        assert pattern.search("2,000-4,000lb")
        assert pattern.search("2,000.15-4,000.25lb")
        assert pattern.search("4oz")
        assert pattern.search("4pound")
        assert pattern.search("4ounce")
        assert pattern.search("4lbs")
        assert pattern.search("4pounds")
        assert pattern.search("4ounces")

    def test_weight_patterns_2nd(self):
        pattern = dim_tok.weight_patterns[1]
        assert pattern.search("2g")
        assert pattern.search("20g")
        assert pattern.search("2.2g")
        assert pattern.search("2,000.15g")
        assert pattern.search("2-g")
        assert pattern.search("2 g")
        assert pattern.search("2-4g")
        assert pattern.search("2,000-4,000g")
        assert pattern.search("2,000.15-4,000.25g")
        assert pattern.search("4kg")
        assert pattern.search("4mg")
        assert pattern.search("4gs")
        assert pattern.search("4kgs")

    def test_weight_patterns_3rd(self):
        pattern = dim_tok.weight_patterns[2]
        assert pattern.search("2gram")
        assert pattern.search("20gram")
        assert pattern.search("2.2gram")
        assert pattern.search("2,000.15gram")
        assert pattern.search("2-gram")
        assert pattern.search("2 gram")
        assert pattern.search("2-4gram")
        assert pattern.search("2,000-4,000gram")
        assert pattern.search("2,000.15-4,000.25gram")
        assert pattern.search("4kilogram")
        assert pattern.search("4milligram")
        assert pattern.search("4grams")
        assert pattern.search("4kilograms")
        assert pattern.search("4milligrams")

class TestCaratPatterns():
    def test_carat_patterns_1st(self):
        pattern = dim_tok.carat_patterns[0]
        assert pattern.search("1 1/2carat")
        assert pattern.search("1-1/2carats")
        
        
    def test_carat_patterns_2nd(self):
        pattern = dim_tok.carat_patterns[1]
        assert pattern.search("1/2carat")
        assert pattern.search("1-2 carats")
        assert pattern.search("1.5~2 c.")
        assert pattern.search("1.5~2-ct")
        assert pattern.search("1.5~2.5-ctw")
        
    def test_carat_patterns_3rd(self):
        pattern = dim_tok.carat_patterns[2]
        assert pattern.search("1.5 cwt")
        assert pattern.search(".5-tw")
        assert pattern.search("0.5 cttw.")
        assert pattern.search("0.5 dtw.")
        assert pattern.search("0.5 dw.")
        assert pattern.search("0.5 tdw")
        
    def test_carat_patterns_4th(self):
        pattern = dim_tok.carat_patterns[3]
        assert pattern.search("12 karat")
        assert pattern.search("18-karat")
        assert pattern.search("24karat")
        assert pattern.search("12 karats")
        assert pattern.search("18-karats")
        assert pattern.search("24karats")