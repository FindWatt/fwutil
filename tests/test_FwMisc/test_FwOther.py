import pytest
import math, re
import numpy as np
from collections import OrderedDict
import FwUtil

class TestVersion():
    def test_version(self):
        print(FwUtil.python_version(b_full_version=False))
        print(FwUtil.python_version(b_full_version=True))


class TestCoresToUse():
    def test_count(self):
        i_mp_cores = FwUtil.cores_to_use()
        assert isinstance(i_mp_cores, int)
        assert i_mp_cores > 0


class TestSecondsToStr():
    def test_seconds_to_string(self):
        assert FwUtil.secondsToStr(5) == "0:00:05.000"


class TestMultiIndex():
    def test_blank_item(self):
        assert FwUtil.multiindex(None, ["item"]) == []
        
    def test_blank_list(self):
        assert FwUtil.multiindex("item", []) == []
        
    def test_number(self):
        assert FwUtil.multiindex(2, [1, 2, 3, 4, 3, 2, 1]) == [1, 5]
        
    def test_strings(self):
        assert FwUtil.multiindex("test", ["test", "this", "function", "test", "out"]) == [0, 3]
        
    def test_non_match(self):
        assert FwUtil.multiindex("no-match", ["test", "this", "function", "test", "out"]) == []


class TestTake():
    def test_blank(self):
        assert FwUtil.take(5,[]) == []
    def test_list(self):
        assert FwUtil.take(2,["test","this","function","out"]) == ["test","this"]
    def test_dict(self):
        e_test = OrderedDict([("test",0), ("this",1), ("function",2), ("out",3)])
        assert FwUtil.take(2,e_test) == ['test', 'this']
        assert FwUtil.take(2,e_test.items()) == [("test",0), ("this",1)]


class TestNthLargest():
    def test_blank(self):
        assert FwUtil.nth_largest(1,[]) == None
    def test_list(self):
        a_test = [5,1,4,10,2,6,8,3,9,7]
        assert FwUtil.nth_largest(1,a_test) == 10
        assert FwUtil.nth_largest(2,a_test) == 9
        assert FwUtil.nth_largest(3,a_test) == 8
        assert FwUtil.nth_largest(4,a_test) == 7
        assert FwUtil.nth_largest(5,a_test) == 6
        assert FwUtil.nth_largest(6,a_test) == 5
        assert FwUtil.nth_largest(7,a_test) == 4
        assert FwUtil.nth_largest(8,a_test) == 3
        assert FwUtil.nth_largest(9,a_test) == 2
        assert FwUtil.nth_largest(10,a_test) == 1
    def test_dict(self):
        at_test = [("test",0), ("this",1), ("function",2), ("out",3)]
        a_keys,a_values = zip(*at_test)
        assert FwUtil.nth_largest(1,a_values) == 3


class TestNthLargestValueIndex():
    def test_blank(self):
        assert FwUtil.nth_largest_value_index(1,[]) == None
    def test_list(self):
        a_test = [5,1,4,10,2,6,8,3,9,7]
        assert FwUtil.nth_largest_value_index(1,a_test) == (10, 3)
    def test_dict(self):
        at_test = [("test",0), ("this",3), ("function",1), ("out",2)]
        a_keys,a_values = zip(*at_test)
        assert FwUtil.nth_largest_value_index(1,a_values) == (3, 1)


class TestGetPublicIP():
    def test_ip(self):
        s_ip = FwUtil.get_public_ip()
        assert re.search(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", s_ip)


class TestRoundTo():
    def test_zero(self):
        assert FwUtil.round_to(0,1) == 0
        assert FwUtil.round_to(0,0.1) == 0
    def test_int(self):
        assert FwUtil.round_to(1,1) == 1
        assert FwUtil.round_to(1,0.1) == 1
    def test_float(self):
        assert math.isclose(FwUtil.round_to(1.23456,1), 1)
        assert math.isclose(FwUtil.round_to(1.23456,0.1), 1.2)
        assert math.isclose(FwUtil.round_to(1.23456,0.01), 1.23)
        assert math.isclose(FwUtil.round_to(1.23456,0.001), 1.235)
        assert math.isclose(FwUtil.round_to(1.23456,0.0001), 1.2346)
        assert math.isclose(FwUtil.round_to(1.23456,0.00001), 1.23456)


class TestDictFunctions():
    testlist = ['object']
    testdict = {"test":0, "this":"1", "function":testlist, "out":None}
    
    def test_check_key(self):
        assert FwUtil.check_key(self.testdict, "this") == "1"
        assert FwUtil.check_key(self.testdict, "TEST") is None
    
    def test_check_is_key_value(self):
        assert FwUtil.check_is_key_value(self.testdict, "this") == "1"
        assert FwUtil.check_is_key_value(self.testdict, "out") is None
        assert FwUtil.check_is_key_value(self.testdict, "TEST") == False
    
    def test_check_key_is_value(self):
        assert FwUtil.check_key_is_value(self.testdict, "test", 0) == True
        assert FwUtil.check_key_is_value(self.testdict, "this", 2) == False
        assert FwUtil.check_key_is_value(self.testdict, "TEST", 1) == False
    
    def test_check_key_is_object(self):
        assert FwUtil.check_key_is_object(self.testdict, "function", self.testlist) == True
        assert FwUtil.check_key_is_object(self.testdict, "function", ['object']) == False
    
    def test_check_key_is_type(self):
        assert FwUtil.check_key_is_type(self.testdict, "test", int) == True
        assert FwUtil.check_key_is_type(self.testdict, "this", str) == True
        assert FwUtil.check_key_is_type(self.testdict, "function", list) == True
    
    def test_check_key_is_in_list(self):
        a_test = [0,1]
        assert FwUtil.check_key_is_in_list(self.testdict, "test", a_test) == True
        assert FwUtil.check_key_is_in_list(self.testdict, "this", a_test) == False


class TestFlatten():
    def test_blank(self):
        assert FwUtil.flatten([]) == []
        assert FwUtil.flatten([[],[],[]]) == []
    def test_lists(self):
        testlist = [[1,2,3],['a','b','c']]
        assert FwUtil.flatten(testlist) == [1,2,3,'a','b','c']


class TestGetDictValues():
    def test_blank(self):
        assert FwUtil.get_dict_values({}) == []
        assert FwUtil.get_dict_values(set([])) == []
        assert FwUtil.get_dict_values({'a':{},'b':set([])}) == []
    def test_dict(self):
        assert set(FwUtil.get_dict_values({1,2,3,'a','b','c'})) == set([1,2,3,'a','b','c'])
        assert set(FwUtil.get_dict_values({1:{1:1,2:2,3:3},2:{1:'a',2:'b',3:'c'},3:set('d')})) == set([1,2,3,'a','b','c','d'])
