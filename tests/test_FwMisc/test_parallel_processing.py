import pytest
import math, random
import FwUtil

class TestParallelProcessing():
    mp_cores = FwUtil.cores_to_use()
    small_random = [random.random() for i in range(10)]
    medium_random = [random.random() for i in range(500)]
    large_random = [random.random() for i in range(5000)]
    kwargs = {'precision':0.001}
        
    def test_multi_core(self):
        small_results_single = FwUtil.run_parallel(self.small_random, FwUtil.round_to, self.kwargs, n_cores = 1 )
        medium_results_single = FwUtil.run_parallel(self.medium_random, FwUtil.round_to, self.kwargs, n_cores = 1 )
        FwUtil.run_parallel(self.large_random, FwUtil.round_to, self.kwargs, n_cores = 1 )
        assert len(small_results_single) == 10
        assert len(medium_results_single) == 500
        
        if self.mp_cores == 1: return None
        small_results_multi = FwUtil.run_parallel(self.small_random, FwUtil.round_to, self.kwargs, n_cores=self.mp_cores )
        medium_results_multi = FwUtil.run_parallel(self.medium_random, FwUtil.round_to, self.kwargs, n_cores=self.mp_cores )
        FwUtil.run_parallel(self.large_random, FwUtil.round_to, self.kwargs, n_cores=self.mp_cores )
        assert small_results_single == small_results_multi
        assert medium_results_single == medium_results_multi